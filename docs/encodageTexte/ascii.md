# ASCII



## Qu'est ce que ASCII?

ASCII (American Standard Code for Information Interchange) 
est une norme informatique de codage de caractères apparue dans les années 1960.

Cette norme ne définissait que 128 codes  (sur une longueur 7 bits, codes de 0000 0000 à 0111 1111).

Parmi les 128 caractères codés, les caractères de numéros 0 à 31 et le 127 
ne sont pas affichables (ils correspondent à des commandes de contrôle: retour à la ligne, bip sonore, etc...). 

95 caractères sont imprimables : 

+ les chiffres  de 0 à 9,
+ les lettres minuscules de a à z  et les majuscules de A à Z, 
+ et des symboles mathématiques et de ponctuation.


!!! note
    Avant cette norme ASCII, dans les années 1950, les ordinateurs, les imprimantes...   utilisaient
    de nombreux encodages différents. Tout passage d'une machine à une autre demandait donc l'utilisation
    d'un logiciel convertissant les encodages.  


## Exercice

Pour obtenir le code ASCII d'une lettre minuscule ou majuscule, on peut utiliser la fonction `ord`
en python.

```
>>> ord('a')
97
>>> ord('A')
65
```

&Eacute;crire un script python qui affiche les lettres de l'alphabet en minuscules 
puis en majuscules avec leur code ASCII.

??? solution "Un code"

    le code 
    
    ```python
    for lettre in "abcdefghijklmnopqrstuvwxyz":
        print(f"Code de la lettre {lettre}: {ord(lettre)} en décimal et {hex(ord(lettre))} en hexadécimal.")
    ```
    
    ou (versions python < 3.6):
    
    ```python
    for lettre in "abcdefghijklmnopqrstuvwxyz":
        print("Code de la lettre {}: {} en décimal et {} en hexadécimal.".format(lettre, ord(lettre), hex(ord(lettre))))
    ```
    
    donne 
    
    ```
    Code de la lettre a: 97 en décimal et 0x61 en hexadécimal.
    Code de la lettre b: 98 en décimal et 0x62 en hexadécimal.
    Code de la lettre c: 99 en décimal et 0x63 en hexadécimal.
    Code de la lettre d: 100 en décimal et 0x64 en hexadécimal.
    Code de la lettre e: 101 en décimal et 0x65 en hexadécimal.
    Code de la lettre f: 102 en décimal et 0x66 en hexadécimal.
    Code de la lettre g: 103 en décimal et 0x67 en hexadécimal.
    Code de la lettre h: 104 en décimal et 0x68 en hexadécimal.
    Code de la lettre i: 105 en décimal et 0x69 en hexadécimal.
    Code de la lettre j: 106 en décimal et 0x6a en hexadécimal.
    Code de la lettre k: 107 en décimal et 0x6b en hexadécimal.
    Code de la lettre l: 108 en décimal et 0x6c en hexadécimal.
    Code de la lettre m: 109 en décimal et 0x6d en hexadécimal.
    Code de la lettre n: 110 en décimal et 0x6e en hexadécimal.
    Code de la lettre o: 111 en décimal et 0x6f en hexadécimal.
    Code de la lettre p: 112 en décimal et 0x70 en hexadécimal.
    Code de la lettre q: 113 en décimal et 0x71 en hexadécimal.
    Code de la lettre r: 114 en décimal et 0x72 en hexadécimal.
    Code de la lettre s: 115 en décimal et 0x73 en hexadécimal.
    Code de la lettre t: 116 en décimal et 0x74 en hexadécimal.
    Code de la lettre u: 117 en décimal et 0x75 en hexadécimal.
    Code de la lettre v: 118 en décimal et 0x76 en hexadécimal.
    Code de la lettre w: 119 en décimal et 0x77 en hexadécimal.
    Code de la lettre x: 120 en décimal et 0x78 en hexadécimal.
    Code de la lettre y: 121 en décimal et 0x79 en hexadécimal.
    Code de la lettre z: 122 en décimal et 0x7a en hexadécimal.
    ```
    
    
    Vous définirez facilement un code analogue pour les majuscules.
    
    
Vous pouvez consulter la table ASCII complète [sur cette page](https://fr.wikibooks.org/wiki/Les_ASCII_de_0_%C3%A0_127/La_table_ASCII).
    
    
## Les limites de ASCII

128 caractères suffisaient pour coder les textes de la langue anglaise. Mais ne suffisent évidemment pas pour coder
tous les caractères de toutes les langues.

Par exemple, dans le code ASCII initial, les lettres é, è, ê ne sont pas codés.

Pour pallier à ce problème, on a utilisé des extensions de l'ASCII en utilisant le 8 ème bit des octets du code ASCII.
Ces extensions sont encore utilisées. 

Le problème est que sur 8 bits, on ne code que 2<sup>8</sup> = 256 caractères, ce qui est encore insuffisant pour
coder tous les caractères de toutes les langues. Il a donc fallu définir plusieurs extensions
de la norme ASCII, incompatibles entre elles.

!!! example "Exemple de norme"
    La norme   ISO 8859-1    souvent appelée Latin-1.
    
    Dans cette norme la lettre é est prévue: elle correspond au code e9 (écriture hexadécimale), c'est 
    à dire 233 en décimal (donc codé 11101001 en machine).
    
    
    
L'incompatibilité de ces normes entre elles posent de nombreux problèmes. Avant l'unicode, aucun jeu de 
caractères ne fournissait toutes les lettres, symboles techniques... Produire un logiciel  obligeait alors
à écrire plusieurs versions du logiciel (pour chaque jeu de caractères)  ou produire un code beaucoup plus complexe
pour traiter tous les jeux de caractères.
 

 
