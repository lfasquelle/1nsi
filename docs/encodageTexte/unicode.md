# Unicode


## ISO-10646


La [norme ISO-10646](https://fr.wikipedia.org/wiki/ISO/CEI_10646) définit un "jeu universel de caractères" 
(UCS:  Universal Character Set).

Cette norme associe à chaque caractère un nom et un numéro (appelé point de code). 

Par exemple U+2735  est le point de code du caractère <span style="font-size:2rem;">✵</span>. Le nom de ce caractère
est "Eight Pointed Pinwheel Star". [Consultez cette page](https://unicode-table.com/en/search/?q=%E2%9C%B5).


Avec python3:
 
```
>>> '\u2735'
'✵'
>>> '\N{Eight Pointed Pinwheel Star}'
'✵'
``` 



## Unicode



Le standard Unicode est un mécanisme universel de codage de caractères. Il facilite le codage 
de textes multilingues, l'échange de données textuelles. Cette norme a défini plusieurs techniques d'encodage UTF 
(universal transformation format) des points de code de l'ISO-10646:  UTF-8, UTF-16, UTF-32.



L'encodage le plus usité pour Unicode est utf-8. Cet encodage code les caractères sur 1 à 4 octets.


Aujourd'hui, il n'y a plus aucune raison d'utiliser des normes telles que ISO 8859-1, même si, vous l'avez vu, 
elles sont encore présentes dans les logiciels.


!!! important 
    Vous veillerez à ce que tous les textes, tous les codes que vous rendez pendant l'année soient encodés en UTF8. 
    
    
## En python

Nous avons utilisé la fonction `ord` de python pour obtenir les codes ASCII des lettres de l'alphabet.   
En fait, la fonction `ord` de python3 renvoie le   code unicode (c'est à dire l'entier associé
au caractère par  la norme ISO-10646). 
Les caractères ASCII ont un code ASCII préservé en unicode.     
Les codes obtenus pour les lettres étaient donc à la fois les codes ASCII et les codes unicode.


## Exercice

La fonction `chr` de python est la fonction "inverse" de `ord`. Elle prend un entier en paramètre et renvoie
le caractère ayant cet entier comme code utf8.

```
>>> chr(128690)
'🚲'
``` 

(le caractère affiché est <span style="font-size:2rem;">🚲</span>).


La liste suivante 

```
[233, 112, 97, 116, 97, 110, 116, 32, 33]
```

correspond à la liste des codes unicodes d'une chaîne. Laquelle?

Vous écrirez une fonction python de "décodage".

??? solution "Une réponse"

    Un script python:
    
    ```python
    def lireCode(liste):
        """
        liste -- liste de code unicode
        
        renvoie la chaîne de caractères correspondante
        """
        chaine = ""
        for nombre in liste:
            chaine += chr(nombre)
        return chaine
    ```


    Essai:
    
    ```
    >>> lireCode([233, 112, 97, 116, 97, 110, 116, 32, 33])
    'épatant !'
    ```


