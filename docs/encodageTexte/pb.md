# Ã©lÃ¨ve


Nous donnons ici un exemple de problème provoqué par le fait que plusieurs normes d'encodage des caractères
sont utilisées.

Vous avez certainement déjà ouvert un courriel ou une page web et observer des caractères étranges 
comme dans le titre de cette page: c'est un problème d'encodage et vous  allez reproduire cela ici. 


## Une page html


Nous allons écrire une page html.
Pour cela:

+ Ouvrir le logiciel Geany et nommer essai.html le fichier.
+ Dans le menu de Geany,  vérifier que l'encodage est utf-8 (Document/définir l'encodage/Unicode/UTF-8).
+ Entrer ensuite le code html suivant dans le fichier:

```html
<!DOCTYPE html>
 

<html lang="fr">
<head>
	<meta charset="utf-8">
	<title> accents  </title>
</head>
 
<body>
bébé, élève, où, château.
</body>
</html>
```

+ Ouvrir ensuite le fichier avec un navigateur. Vous ne devriez pas avoir de problème de lecture.
+ Modifier maintenant la ligne `<meta charset="utf-8">` en la ligne `<meta charset="iso-8859-1">` 
puis ouvrir le fichier à l'aide d'un navigateur.

Qu'observez-vous lors de cette dernière étape? Comment l'expliquer?

??? solution "solution"

    On observe des caractères "bizarres" à la place de nos lettres accentuées.
    
    Pourquoi?
    
    Notre fichier a été encodé lors de **l'écriture** en utf-8. 
    
    Avec la ligne `<meta charset="iso-8859-1">`, on dit, par contre, au navigateur de **lire**   le fichier 
    en lui annonçant que ce fichier est encodé suivant la norme  iso-8859-1.
    
    Lorsque le navigateur lit le code de la lettre é (codé en utf-8), il cherche la correspondance 
    de ce code, mais il ne cherche pas cette correspondance   dans la table de l'utf-8, 
    il la cherche dans la table de iso-8859-1.  
    Or ce code ne correspond pas en iso-8859-1 à la lettre é. On obtient   le caractère (ou les caractères)
    correspondant au code lu dans la table de l'iso-8859-1.
    
    

## Remarque

Vous pouvez bien sûr illustrer le problème dans l'autre sens. C'est à dire choisir
d'écrire un fichier html en iso-8859-1 
(en sélectionnant, dans geany, à l'ouverture du fichier 
"Document/Définir l'encodage/Européen de l'ouest/Occidental(iso-8859-1)")
puis tenter d'ouvrir ce fichier avec un navigateur avec le charset défini par `<meta charset="utf-8">`.
Vous obtiendrez de nouveau des caractères bizarres (avec le même texte, vous  obtiendrez des
caractères bizarres différents de ceux de l'exercice précédent puisque l'interprétation se fait maintenant avec un autre code 
de départ et un autre décodage à l'arrivée).


## Quel encodage choisir?

Aujourd'hui, il n'y a plus de questions à se poser: **choisissez systématiquement l'encodage utf-8**.

!!! important 
    Vous veillerez à ce que tous les textes, tous les codes que vous rendez pendant l'année soient encodés en UTF8. 
    
    
UTF-8 est en effet un codage de caractères  conçu pour coder l’ensemble des caractères   Unicode, 
en restant compatible avec la norme ASCII.
