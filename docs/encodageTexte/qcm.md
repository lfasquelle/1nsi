# QCM  


!!! note  
    Les QCM sont là pour vous aider à contrôler ce que vous avez retenu.
    Si vous ne répondez pas à toutes les questions sans hésitation, c'est sans doute
    qu'il faut refaire des lectures des pages précédentes.

Trouvez la ou les bonnes réponses.
 
 

## QCM 1

Les minuscules sont codées en ASCII par des entiers consécutifs.

Il en est de même pour les majuscules.

Le code de la lettre `a` est 97.

Le code de la lettre `A` est 65.

L'affirmation suivante:

"Pour passer du code binaire ASCII d'une majuscule au code binaire ASCII de la minuscule correspondante,
il suffit de changer le 6ème bit (en partant de la droite) de 0 en 1."

est-elle:

- [ ] vraie
- [ ] fausse

??? solution "Réponse"

    - [X] vraie
    - [ ] fausse


    Observez le résultat du code suivant (on y utilise la méthode upper()
    pour passer une lettre en majuscule):
    
    ```python
    for lettre in "abcdefghijklmnopqrstuvwxyz":
        print(f"Code de la lettre {lettre}: {ord(lettre)} en décimal et {bin(ord(lettre))} en binaire.")
        print(f"Code de la lettre {lettre.upper()}: {ord(lettre.upper())} en décimal et {bin(ord(lettre.upper()))} en binaire.")
    ```
    
    Résultat:
    
    ```
    Code de la lettre a: 97 en décimal et 0b1100001 en binaire.
    Code de la lettre A: 65 en décimal et 0b1000001 en binaire.
    Code de la lettre b: 98 en décimal et 0b1100010 en binaire.
    Code de la lettre B: 66 en décimal et 0b1000010 en binaire.
    Code de la lettre c: 99 en décimal et 0b1100011 en binaire.
    Code de la lettre C: 67 en décimal et 0b1000011 en binaire.
    Code de la lettre d: 100 en décimal et 0b1100100 en binaire.
    Code de la lettre D: 68 en décimal et 0b1000100 en binaire.
    Code de la lettre e: 101 en décimal et 0b1100101 en binaire.
    Code de la lettre E: 69 en décimal et 0b1000101 en binaire.
    Code de la lettre f: 102 en décimal et 0b1100110 en binaire.
    Code de la lettre F: 70 en décimal et 0b1000110 en binaire.
    Code de la lettre g: 103 en décimal et 0b1100111 en binaire.
    Code de la lettre G: 71 en décimal et 0b1000111 en binaire.
    Code de la lettre h: 104 en décimal et 0b1101000 en binaire.
    Code de la lettre H: 72 en décimal et 0b1001000 en binaire.
    Code de la lettre i: 105 en décimal et 0b1101001 en binaire.
    Code de la lettre I: 73 en décimal et 0b1001001 en binaire.
    Code de la lettre j: 106 en décimal et 0b1101010 en binaire.
    Code de la lettre J: 74 en décimal et 0b1001010 en binaire.
    Code de la lettre k: 107 en décimal et 0b1101011 en binaire.
    Code de la lettre K: 75 en décimal et 0b1001011 en binaire.
    Code de la lettre l: 108 en décimal et 0b1101100 en binaire.
    Code de la lettre L: 76 en décimal et 0b1001100 en binaire.
    Code de la lettre m: 109 en décimal et 0b1101101 en binaire.
    Code de la lettre M: 77 en décimal et 0b1001101 en binaire.
    Code de la lettre n: 110 en décimal et 0b1101110 en binaire.
    Code de la lettre N: 78 en décimal et 0b1001110 en binaire.
    Code de la lettre o: 111 en décimal et 0b1101111 en binaire.
    Code de la lettre O: 79 en décimal et 0b1001111 en binaire.
    Code de la lettre p: 112 en décimal et 0b1110000 en binaire.
    Code de la lettre P: 80 en décimal et 0b1010000 en binaire.
    Code de la lettre q: 113 en décimal et 0b1110001 en binaire.
    Code de la lettre Q: 81 en décimal et 0b1010001 en binaire.
    Code de la lettre r: 114 en décimal et 0b1110010 en binaire.
    Code de la lettre R: 82 en décimal et 0b1010010 en binaire.
    Code de la lettre s: 115 en décimal et 0b1110011 en binaire.
    Code de la lettre S: 83 en décimal et 0b1010011 en binaire.
    Code de la lettre t: 116 en décimal et 0b1110100 en binaire.
    Code de la lettre T: 84 en décimal et 0b1010100 en binaire.
    Code de la lettre u: 117 en décimal et 0b1110101 en binaire.
    Code de la lettre U: 85 en décimal et 0b1010101 en binaire.
    Code de la lettre v: 118 en décimal et 0b1110110 en binaire.
    Code de la lettre V: 86 en décimal et 0b1010110 en binaire.
    Code de la lettre w: 119 en décimal et 0b1110111 en binaire.
    Code de la lettre W: 87 en décimal et 0b1010111 en binaire.
    Code de la lettre x: 120 en décimal et 0b1111000 en binaire.
    Code de la lettre X: 88 en décimal et 0b1011000 en binaire.
    Code de la lettre y: 121 en décimal et 0b1111001 en binaire.
    Code de la lettre Y: 89 en décimal et 0b1011001 en binaire.
    Code de la lettre z: 122 en décimal et 0b1111010 en binaire.
    Code de la lettre Z: 90 en décimal et 0b1011010 en binaire.
    ```
    
    
    La différence entre le code de `a` et celui de `A` est égale à 32 et 32 = 10 0000<sub>deux</sub>.
    
    
    
## QCM 2

Le codage utf-8 est 

- [ ] sur 1 à 4 octets.
- [ ] sur 8 bits.
- [ ] sur 8 octets.
- [ ] sur 7 bits.


??? solution "Réponse"


    - [X] sur 1 à 4 octets.
    - [ ] sur 8 bits.
    - [ ] sur 8 octets.
    - [ ] sur 7 bits.
    
    

   
## QCM 3

ASCII signifie

- [ ] Alphabet System Caps International Internet.
- [ ] A System Computer International Interchange.
- [ ] Alphabet System Code for Intelligence Interchange.
- [ ] American Standard Code for Information Interchange.


??? solution "Réponse"


    - [ ] Alphabet System Caps International Internet.
    - [ ] A System Computer International Interchange.
    - [ ] Alphabet System Code for Intelligence Interchange.
    - [X] American Standard Code for Information Interchange.
