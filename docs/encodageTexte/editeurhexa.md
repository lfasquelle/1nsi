# Lecture de quelques caractères sous forme d'octets



## Qu'est ce qu'un éditeur hexadécimal?

Un éditeur hexadécimal est un logiciel permettant de visualiser et éditer le contenu de  fichiers, 
en visualisant leurs données en octets (présentés sous forme de caractères hexadécimaux). 


Nous utilisons ci-dessous un éditeur hexadécimal sur un simple fichier texte (.txt): on observera 
donc directement les octets associés au caractère au lieu du caractère. Tandis qu'avec un éditeur de texte, comme
geany, c'est bien le texte qui sera lu. 

En bref, on peut considérer que l'éditeur de texte (ou un lecteur d'image s'il s'agit 
d'un fichier image, ou un lecteur de musique, etc...) tient compte des métadonnées 
pour savoir comment interpréter le code, tandis que l'éditeur hexadécimal ne tiendra pas compte des métadonnées: il ne
cherche pas à interpréter le code, il l'affiche.



## Exemple 1


Nous allons utiliser un éditeur hexadécimal en ligne (vous en trouverez aussi facilement des gratuits que vous
pourrez installer sur vos machines personnelles).


+ Créer un fichier texte .txt dans lequel vous entrez simplement la lettre a.
+ Se rendre ensuite avec votre navigateur à la page [https://hex-works.com/eng](https://hex-works.com/eng).
+ Choisir le menu Open et naviguer dans l'arborescence de votre machine pour sélectionner le fichier .txt que 
vous venez de créer.

Qu'observez-vous? Comment interprétez-vous le code affiché?


??? solution "Réponse"

    ![](fichiers/codeLettrea.png)
    
    Le code 61 est le code hexadécimal de la lettre a (97 en décimal).
    
    Vérifions cela avec python:
    
    ```
    >>> ord('a')
    97
    >>> hex(97)
    '0x61'
    ```
    
    On lit ensuite le code hexadécimal 0A.
    
    Demandons à python3 de quoi il s'agit:
    
    ```
    >>> chr(0x0A)
    '\n'
    ```
    
    Il s'agit du [caractère de fin de ligne](https://fr.wikipedia.org/wiki/Fin_de_ligne).
    
    
## Exemple 2


Recommencer le travail mais avec la lettre é.

+ Avec un premier fichier encodé en utf8.
+ Avec un second fichier encodé en iso-8859-1.


??? solution "Résultat"

    Avec un fichier encodé en utf8, on obtient:
    
    ```
    C3 A9 0A 
    ```
    
    0A est le code de fin de ligne.
    On voit donc que la lettre `é` est encodée sur deux octets.
    
    Avec python:
    
    ```
    >>> ord('é')
    233
    >>> hex(233)
    '0xe9'
    ```
    
    Cela ne semple plus correspondre. C'est normal! Il ne faut pas confondre unicode qui identifie
    des caractères par des nombres 
    et l'encodage utf8 qui va associer à (la plupart) des caractères unicodes une suite d'octets. 
    D'autres encodages des caractères unicode comme utf16 par exemple donneront en général une autre suite d'octets.
    Par contre, il y a bien coïncidence pour les caractères de la table ASCII (utf-8 étant défini
    pour préserver cela).
    
    
    
    L'instruction à utiliser avec python est la suivante:
    
    ```
    >>> 'é'.encode('utf8')
    b'\xc3\xa9'
    ```
    
    C3 A9 correspond bien à la lettre `é` en encodage utf8.
    
    
    !!! important
        Pour faire le point entre unicode et utf8:
        
        + [lien 1](https://fr.esdifferent.com/difference-between-unicode-and-utf-8)
        + [descriptif historique](https://www.figer.com/Publications/utf8.htm)
    
    Avec le fichier encodé en ISO-8859-1, on obtient E9 0A.
    
    Avec python:
    
    ```
    >>> 'é'.encode('iso8859_1')
    b'\xe9'
    ```
    
    
