# Encodage du texte
 
 
 

 
En machine, tout n'est que 0 et 1.

Pour coder autre chose qu'un entier naturel (image, son, texte...), il faut donc nécessairement
décider d'une convention associant un nombre et l'objet à représenter.

Bien entendu, un même nombre pourra représenter dans un contexte donné un entier naturel, 
dans un autre contexte une lettre, dans un autre contexte un son... etc

Dans cette capsule, nous allons voir quels sont les nombres associés usuellement aux lettres (et autres symboles du
texte).
    
