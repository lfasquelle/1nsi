# Compréhension "multiple"




## Un exemple

 
On veut définir la liste des couples (1, 1), (1, 2), (1, 3), (2, 1), (2, 2), (2, 3), (3, 1), (3, 2), (3, 3)  
ou plus généralement la liste des couples (i,j) pour i entre 1 et n et j entre 1 et n (où n est un entier).

On peut procéder ainsi (avec n = 3):
 
```python
listeCouples = []
for i in range(1, 4):
    for j in range(1, 4):
        listeCouples.append(i,j)
```

Ou par compréhension:

```python
listeCouples = [(i,j) for i in range(1,4) for j in range(1,4)]
```



## Exercice

Comment pourriez-vous définir par compréhension la liste `A` suivante:

```python
[  ('a', 0), ('a', 1), ('a', 2), 
   ('b', 0), ('b', 1), ('b', 2), 
   ('c', 0), ('c', 1), ('c', 2)
   ]
```

??? solution

    ```python
    A = [(c,i) for c in 'abc' for i in range(3)]
    ```










## Exercice

Comment pourriez-vous définir par compréhension la liste `A` suivante:

```python
[  (1, 2), (1, 3), (1, 4), (1, 5), (1, 6), 
   (2, 3), (2, 4), (2, 5), (2, 6), 
   (3, 4), (3, 5), (3, 6), 
   (4, 5), (4, 6), 
   (5, 6)
]
```

??? solution

    ```python
    A = [(i,j) for i in range(1,7) for j in range(i+1,7)]
    ```
    On pourrait aussi écrire:
    
    ```python
    A = [(i,j) for i in range(1,7) for j in range(1,7) if j > i]
    ```
    
