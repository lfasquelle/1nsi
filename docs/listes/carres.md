# Somme des carrés des chiffres


## Liste des chiffres

Ecrire un corps pour la fonction suivante:

```python
def listeChiffres(n):
    """
    n -- entier naturel
    renvoie la liste des chiffres de l'écriture décimale de n
    
    >>> listeChiffres(10)
    [0, 1]
    >>> listeChiffres(234)
    [4, 3, 2]
    """
```

??? solution
    On utilise évidemment la méthode des divisions en cascade:
    
    ```python
    def listeChiffres(n):
        """
        n -- entier naturel
        renvoie la liste des chiffres de l'écriture décimale de n
        
        >>> listeChiffres(10)
        [0, 1]
        >>> listeChiffres(234)
        [4, 3, 2]
        """
        if n == 0: return [0]
        resultat = []
        while n!=0:
            resultat.append(n%10)
            n = n//10
        return resultat
    ```



## Somme des carrés des chiffres

Ecrire un corps pour la fonction suivante:

```python
def sommeCarreChiffre(n):
    """
    n -- entier naturel
    renvoie la somme des carrés des chiffres de l'écriture décimale de n
    >>> sommeCarreChiffre(12)
    5
    """
```


??? solution

    On fait appel à la fonction précédente:
    
    ```python
    def sommeCarreChiffre(n):
        """
        n -- entier naturel
        renvoie la somme des carrés des chiffres de l'écriture décimale de n
        >>> sommeCarreChiffre(12)
        5
        """
        s = 0
        for c in listeChiffres(n):
            s += c*c
        return s
    ```


## Liste des sommes des carrés des chiffres

On part d'un entier $n$, on calcule la somme $s_1$ des carrés des chiffres de $n$ (écriture décimale),
puis on calcule $s_2$ la somme des carrés des chiffres de $s_1$, 
puis on calcule $s_3$ la somme des carrés des chiffres de $s_2$, ... et ainsi de suite.
On stocke toutes ces valeurs successives dans une liste. On stoppe le calcul de la liste soit lorsqu'elle a atteint une longueur 
donnée à l'avance soit lorsque la somme des carrés obtenue a déjà été obtenue auparavant.


```python
def listeSommeCarres(n, stop):
    """
    n -- entier naturel
    stop -- entier naturel (longueur maximale de la liste donnée en sortie)
    renvoie la liste définie ci-dessus.
    >>> listeSommeCarres(12, 100)
    [5, 25, 29, 85, 89, 145, 42, 20, 4, 16, 37, 58]
    """
```

Pour l'exemple donné, le calcul de la liste est stoppé bien avant la longueur 100 car la prochaine somme est 
$5^2+8^2 = 89$ et ce résultat 89 est déjà présent  dans la liste.

??? solution


    ```python
    def listeSommeCarres(n, stop):
        """
        n -- entier naturel
        stop -- entier naturel (longueur maximale de la liste donnée en sortie)
        renvoie la liste définie ci-dessus.
        >>> listeSommeCarres(12, 100)
        [5, 25, 29, 85, 89, 145, 42, 20, 4, 16, 37, 58]
        """
        resultat = []
        s = sommeCarreChiffre(n)
        while s not in resultat and len(resultat) < stop:
            resultat.append(s)
            s = sommeCarreChiffre(s)
        return resultat
    ```
    

