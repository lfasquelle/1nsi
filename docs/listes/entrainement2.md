# Le scrabble




!!! important
    On rappelle que l'entraînement à la programmation est indispensable pour apprendre à programmer...




   
## Exercice 1



On dispose de la  liste suivante:

```python
scrabble = ['A',1,'B',3,'C',3,'D',2,'E',1,'F',4,'G',2,'H',4,
            'I',1,'J',8,'K',10,'L',1,'M',2,'N',1, 'O',1,
            'P',3,'Q',8,'R',1,'S',1,'T',1,'U',1,'V',4,'W',10,
            'X',10,'Y',10,'Z',10]
```

Chaque lettre de l'alphabet est suivie du nombre de points qui lui est attribué au scrabble.


&Eacute;crire une fonction python spécifiée comme suit:

```python
def valeur(lettre):
    """
    lettre -- un caractère pris dans les lettres majuscules.
    
    renvoie le nombre de points associé (c'est à dire la valeur inscrite dans 
    la liste scrabble juste après la lettre)
    
    >>> valeur("A")
    1
    >>> valeur("M")
    2
    """
```

??? solution "Une réponse"

    Une réponse possible dans [ce fichier jupyter notebook](fichiers/scrabble.ipynb).
    
    [Version html du fichier](fichiers/scrabble.html):
    
    <iframe src="fichiers/scrabble.html" width="100%" height="400px"></iframe> 

 
 
 
 
 
 
 
 
## Exercice 2
 
On poursuit l'exercice précédent. 

&Eacute;crire maintenant une fonction python spécifiée comme suit:

```python
def maxi(liste):
    """
    liste -- liste de lettres majuscules.
    
    renvoie la lettre qui rapporte le plus grand nombre de points.
    Lorsque plusieurs lettres correspondent à la valeur maximale, 
    la fonction renvoie celle qui a le plus grand indice dans liste.
    
    >>> maxi(['B', 'A', 'K', 'D'])
    'K'
    >>> maxi(['A', 'E', 'I'])
    'I'
    """
```


??? solution "Une réponse"

    Une réponse possible dans [ce fichier jupyter notebook](fichiers/maxiscrabble.ipynb).
    
    Et [sa version html](fichiers/maxiscrabble.html):
 
    <iframe src="fichiers/maxiscrabble.html" width="100%" height="400px"></iframe> 





 
## Exercice 3
 
On poursuit les exercices précédents. 

&Eacute;crire maintenant une fonction python spécifiée comme suit:

```python
def valeur_mot(mot):
    """
    mot -- chaîne de caractères constituée de lettres majuscules.
    
    renvoie le nombre total de points du mot  (en additionnant les valeurs 
    attribuées à chaque lettre du mot)
    
    >>> valeur_mot("PYTHON")
    20
    >>> valeur_mot("JAVASCRIPT")
    24
    """
```



??? solution "Une réponse"

    Une réponse possible dans [ce fichier jupyter notebook](fichiers/scrabblemot.ipynb).
    
    Et [sa version html](fichiers/scrabblemot.html):
 
    <iframe src="fichiers/scrabblemot.html" width="100%" height="400px"></iframe> 





## Exercice 4
 
 

&Eacute;crire  une fonction python spécifiée comme suit:

```python
def compte_carac(mot, lettre):
    """
    mot -- liste de caractères ou chaîne de caractères
    lettre -- caractère
    
    renvoie le nombre d'occurrences de lettre dans mot.
    
    >>> compte_carac("ARBRE", 'R')
    2
    >>> compte_carac(['N','U','M','E','R','I','Q','U','E'], 'I')
    1
    """
```



??? solution "Une réponse"

    Un code possible:
    
    ```python
    def compte_carac(mot, lettre):
        """
        mot -- liste de caractères ou chaîne de caractères
        lettre -- caractère
        
        renvoie le nombre d'occurrences de lettre dans mot.
        
        >>> compte_carac("ARBRE", 'R')
        2
        >>> compte_carac(['N','U','M','E','R','I','Q','U','E'], 'I')
        1
        """
        effectif = 0
        for x in mot:
            if x == lettre:
                effectif += 1
        return effectif
    ```
    
    
    

## Exercice 5
 
 

&Eacute;crire  une fonction python spécifiée comme suit:

```python
def verif(jeu, mot, lettre):
    """
    jeu -- liste de lettres majuscules (les lettres dont dispose le joueur)
    mot -- chaîne de caractères (constituée de lettres majuscules, mot que voudrait écrire le joueur) 
    lettre -- une lettre majuscule (lettre disponible sur le plateau de jeu)
    
    renvoie True si on peut écrire mot avec les lettres de jeu et le caractère lettre, 
    renvoie False sinon.
    
    >>> verif(['A','R','B','E'], "ARBRE", 'R')
    True
    >>> verif(['A','R','B','E'], "ARBRE", 'E')
    False
    """
```



??? solution "Une réponse"



    Un code possible ci-dessous. On utilise la fonction `compte_carac()` de l'exercice précédent.
    
    ```python
    def verif(jeu, mot, lettre):
        """
        jeu -- liste de lettres majuscules (les lettres dont dispose le joueur)
        mot -- chaîne de caractères (constituée de lettres majuscules, mot que voudrait écrire le joueur) 
        lettre -- une lettre majuscule (lettre disponible sur le plateau de jeu)
        
        renvoie True si on peut écrire mot avec les lettres de jeu et le caractère lettre, 
        renvoie False sinon.
        
        >>> verif(['A','R','B','E'], "ARBRE", 'R')
        True
        >>> verif(['A','R','B','E'], "ARBRE", 'E')
        False
        """
        for x in mot:
            if x == lettre:
                if compte_carac(mot, x) != compte_carac(jeu, x) + 1:
                    return False
            else:
                if compte_carac(mot, x) != compte_carac(jeu, x):
                    return False
        return True
    ```

