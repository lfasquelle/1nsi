#  QCM

!!! note  
    Les QCM sont là pour vous aider à contrôler ce que vous avez retenu.
    Si vous ne répondez pas à toutes les questions sans hésitation, c'est sans doute
    qu'il faut refaire des lectures des pages précédentes.

Trouvez la ou les bonnes réponses.

 
    
    
 


## QCM 


Quelle est la réponse de l'interpréteur python:

```
>>> len([])

```

- [ ] une erreur
- [ ] 0
- [ ] undefined
- [ ] 1

??? solution "Réponse"

    - [ ] une erreur
    - [X] 0
    - [ ] undefined
    - [ ] 1
    
    La longueur d'une liste est son nombre d'éléments.
    Une liste vide a 0 élément, donc une longueur égale à 0.
    
    
    
## QCM  


Avec la fonction suivante:

```python
def f(liste):
    c = 0
    for x in liste:
        c = c + 1
    return c
```

quelle est la valeur de `f([1, 2, 3])`:


- [ ] 6
- [ ] 0
- [ ] 3
- [ ] 4



??? solution "Réponse"


    - [ ] 6
    - [ ] 0
    - [X] 3
    - [ ] 4
    
    La fonction renvoie le nombre d'éléments de la liste donnée en argument.


## QCM  

Pour calculer le produit des éléments de liste, par quoi doit-on remplacer
les pointillés:


```python
def produit(liste):
    """
    liste -- liste d'entiers
    
    renvoie le produit des éléments de liste
    """
    p = 1
    for i, x  in enumerate(liste):
        ...........
    return p
```

- [ ] p = p * x
- [ ] p = p * i
- [ ] p = p + i*x
- [ ] p = i*x


??? solution "réponse"

    - [X] p = p * x
    - [ ] p = p * i
    - [ ] p = p + i*x
    - [ ] p = i*x


 

    
    
## QCM 

Pour accèder à l'élément d'indice 2 d'une liste A, on entre:

```python
>>> A = [3, 7, 9, 10]
>>> A(2)
```

On obtient:

- [ ] 7
- [ ] 9
- [ ] l'erreur `list index out of range`
- [ ] l'erreur ` 'list' object is not callable`



??? solution "Réponse"

    - [ ] 7
    - [ ] 9
    - [ ] l'erreur `list index out of range`
    - [X] l'erreur ` 'list' object is not callable`
    
    La notation A(2) correspondrait à l'appel d'une fonction A (à évaluer avec le paramètre 2).
    Mais on ne peut appeler (not callable) une liste.
