# Des requêtes simples



 

Dans les exercices qui suivent, on dispose d'une liste 
constituée de tuples (nom, prénom, année de naissance):.





```python
personnes = [ ('Labrosse', 'Adam', 2000), 
('Gemlamorte', 'Adèle', 1985), 
('Auboisdormant', 'Abel', 2001),
('Etpan', 'Ahmed', 1975), 
('Térieur', 'Alain', 1999), 
('Térieur', 'Alex', 1976), 
('Tanrien', 'Jean', 2010),
('Ouzi', 'Jacques', 1950),
('Deuf', 'John', 2006),
('Provist', 'Alain', 2011)
]
```


On suppose que ceci n'est qu'une petite partie de la liste...il n'est 
évidemment pas question dans la suite de lire les réponses sur la liste de départ, il faut
créer un code qui construira seul la réponse.


!!! note

	Le travail effectué ici est proche de ce que vous ferez en terminale avec le 
	langage SQL. Ce travail sera prolongé cette année lorsque nous parlerons
	du format csv.

## Question 

Créer la liste des noms des personnes.

??? solution 

	Un code possible:

	```python
	noms = [nom for (nom, _, _) in personnes]
	```
	
??? solution "Remarque"

	Pour supprimer les doublons, il est possible d'utiliser l'astuce suivante:
	
	- on transforme la liste en ensemble avec `set` (ce qui supprime 
	automatiquement les doublons).
	- on transforme ensuite le résultat en liste avec `list`.
	
	```
	noms = list(set([nom for (nom, _, _) in personnes]))
	```

## Question 


Créer la liste des couples (nom, prénom) des personnes.

??? solution

	```
	[(nom, prenom) for (nom, prenom, _) in personnes]
	```

## Question 

Créer la liste des couples (nom, prénom) 
des personnes dont le nom commence par la lettre T. 

??? solution

	```
	[(nom, prenom) for (nom, prenom, _) in personnes if nom[0] == 'T']
	```


## Question 

Créer la liste des tuples (nom, prénom, année de naissance)
 des personnes  qui sont nées avant l'année 1999.
 
 
??? solution 

	```
	[(nom, prenom, annee) for (nom, prenom, annee) in personnes if annee < 1999]
	```


## Question

Créer la liste des tuples (nom, prénom, année de naissance) des personnes 
qui sont nées entre 1975 et 1985 (extrêmes compris). 


??? solution

	```
	[(nom, prenom, annee) for (nom, prenom, annee) in personnes if 1975 <= annee <=  1985]
	```


## Question

Créer la liste des tuples (nom, prénom, année de naissance) des personnes 
 dont le prénom est Ahmed ou le prénom est Alain. 
 
??? solution

	```
	[(nom, prenom, annee) for (nom, prenom, annee) in personnes if prenom in ('Alain', 'Ahmed')]
	```
	
	On peut aussi écrire:
	
	```
	[(nom, prenom, annee) for (nom, prenom, annee) in personnes if prenom == 'Alain' or prenom == 'Ahmed']
	```
	
## Question

Créer la liste des tuples (nom, prénom) des personnes  dont le prénom n'est ni Ahmed, ni Alain. 


??? solution


	```
	[(nom, prenom) for (nom, prenom, _) in personnes if prenom not in ('Alain', 'Ahmed')]
	```
	
	ou 
	
	```
	[(nom, prenom) for (nom, prenom, _) in personnes if prenom != 'Alain' and prenom != 'Ahmed']
	```
	
	
## Question

Créer la liste contenant l'année de naissance de Labrosse. 


??? solution

	```
	[annee for (nom, _, annee) in personnes if nom == 'Labrosse']
	```

## Question

Créer la liste contenant  les années de naissance des personnes de prénom Alain. 


??? solution

	```
	[annee for (_, prenom, annee) in personnes if prenom == "Alain"]
	```
	



## Question 

Créer la liste des couples (nom, prénom) des personnes plus âgées que Adèle Gemlamorte.

??? solution

	```python
	[(nom, prenom) 
	for (nom, prenom, annee) in personnes 
	if annee < [an for (n,p,an) in personnes if (n,p)==('Gemlamorte', 'Adèle')][0]]
	```
