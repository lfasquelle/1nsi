# Les python-listes




## Liste python
 
Une  liste python  est une séquence d'éléments séparés par des virgules et "encapsulés" par des crochets.


```
>>> a = [3, 4, 6, 7]
>>> a[0]
3
>>> a[len(a)-1]
7
```


## Longueur d'une liste python

On obtient la longueur d'une liste python avec la fonction `len` (abréviation de length).

```
>>> tab = [3, 4, 88, 42, 666]
>>> len(tab)
5
```


## Numérotation des éléments

Les éléments d'une liste sont numérotés de 0 à longueur(liste)-1.

```
>>> tab = [666, 42, 1789, 1968]
>>> tab[0]
666
>>> tab[1]
42
>>> tab[2]
1789
>>> tab[3]
1968
>>> tab[4]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
IndexError: list index out of range
```

!!! important "out of range"
    Rappelons que vous **devez** savoir interpréter les messages d'erreur tels que le précédent 
    `list index out of range`  (indice en dehors de la plage d'indices existante: vous cherchez
    à accèder à un élément de la liste via un indice n'existant pas).

Python autorise également de numéroter -1 le dernier élément, -2 l'avant-dernier...

```
>>> tab = [666, 42, 1789, 1968]
>>> tab[-1]
1968
>>> tab[-2]
1789
>>> tab[-3]
42
>>> tab[-len(tab)]
666
```

Pour $i\in\lbrace 1, 2, ..., \text{len}(tab) \rbrace$, on pourra lire `tab[-i]` comme un raccourci pour
`tab[len(tab)-i]`.
