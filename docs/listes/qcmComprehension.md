#  QCM

!!! note  
    Les QCM sont là pour vous aider à contrôler ce que vous avez retenu.
    Si vous ne répondez pas à toutes les questions sans hésitation, c'est sans doute
    qu'il faut refaire des lectures des pages précédentes.

Trouvez la ou les bonnes réponses.


## QCM 

  
Quelle est la réponse de l'interpréteur python en dernière ligne?

``` 
>>> a = [1,2,3]
>>> a = [str(x) for x in a]
>>> a[1] + a[2]

```

- [ ] 3
- [ ] 5
- [ ] '12'
- [ ] '23'


??? solution "Réponse"

    - [ ] 3
    - [ ] 5
    - [ ] '12'
    - [X] '23'
    
    En ligne 1, on construit la liste contenant les entiers 1, 2, 3.
    En ligne 2, on construit la liste des chaînes de caractères définies à partir des entiers de la liste précédente.
    
    ```
    >>> str(2)
    '2'
    ```
    
    ```
    >>> a = [1, 2, 3]
    >>> type(a[0])
    <class 'int'>
    >>> a = [str(x) for x in a]
    >>> a
    ['1', '2', '3']
    >>> type(a[0])
    <class 'str'>
    ```
    
    En ligne 3, on demande donc la concaténation de deux chaînes de caractères, ici les chaînes '2' et '3'. Leur
    concaténation est '23'.
    
    
    
    
## QCM  


Q'obtient-on avec

```
>>> [a + b for a in ('1', '2', '3') for b in ('4', '5', '6')]

```

- [ ] ['41', '52', '63']
- [ ] ['14', '25', '36']
- [ ] ['14', '24', '34', '15', '25', '35', '16', '26', '36']
- [ ] ['14', '15', '16', '24', '25', '26', '34', '35', '36']



??? solution "Réponse"

    - [ ] ['41', '52', '63']
    - [ ] ['14', '25', '36']
    - [ ] ['14', '24', '34', '15', '25', '35', '16', '26', '36']
    - [X] ['14', '15', '16', '24', '25', '26', '34', '35', '36']


    Le code 
    
    ```python
    liste = [a + b for a in ('1', '2', '3') for b in ('4', '5', '6')]
    ```
    
    
    est équivalent au code suivant:
    
    
    ```python
    liste = []
    for a in ('1', '2', '3'):
        for b in ('4', '5', '6'):
            liste.append(a+b)
    ```
    
    La lecture est la suivante:
    
    + a étant fixé à la valeur '1', b parcourt le tuple ('4', '5', '6'): on ajoute donc '14', puis '15' puis '16'
    à la liste.
    + a prend ensuite la valeur '2' et b parcourt le tuple ('4', '5', '6'): on ajoute donc '24', puis '25' puis '26'
    à la liste.
    + a prend ensuite la valeur '3' et b parcourt le tuple ('4', '5', '6'): on ajoute donc '34', puis '35' puis '36'
    à la liste.
    
    

## QCM  

Quelle est la réponse de l'interpréteur Python en dernière ligne:

```
>>> a = [i*i for i in range(5)]
>>> a[-1]

```

- [ ] Une erreur
- [ ] 0
- [ ] 16
- [ ] 25


??? solution "Réponse" 

    
    - [ ] Une erreur
    - [ ] 0
    - [X] 16
    - [ ] 25
    
    ```
    >>> a = [i*i for i in range(5)]
    >>> a
    [0, 1, 4, 9, 16]
    >>> a[-1]
    16
    ```



## QCM 

Après l'affectation suivante:

```python
alpha = [x for x in 'abcdefghijklmnopqrstuvwxyz']
```

on accède à la lettre 'b' avec:

- [ ] alpha[1]
- [ ] alpha[2]
- [ ] alpha['b']
- [ ] alpha(1)



??? solution "Réponse" 

    - [X] alpha[1]
    - [ ] alpha[2]
    - [ ] alpha['b']
    - [ ] alpha(1)
    
    ```
    >>> alpha = [x for x in 'abcdefghijklmnopqrstuvwxyz']
    >>> alpha
    ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    >>> alpha[1]
    'b'
    ```

 
    
    
 



## QCM 


 

```python
def multipleDe10(x):
    """
    x -- entier
    
    renvoie True si l'écriture décimale de x se termine par 0, False sinon
    """
    return x%10 == 0


def filtre(liste, fonction):
    """
    liste -- liste d'entiers
    fonction -- fonction prenant un entier en paramètre et renvoyant True ou False.
    
    renvoie la liste des éléments de liste tel que fonction(element) est vrai.
    """
    return [elt for elt in liste if fonction(elt)]
```


Quelle est la valeur de   `filtre([i*j for i in range(1,10) for j in range(1,10)], multipleDe10)`?

 
- [ ] [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
- [ ] [] 
- [ ] [10, 20, 30, 40]
- [ ] [10, 20, 10, 20, 30, 40, 30, 40]


??? solution "Réponse"

    
    - [ ] [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
    - [ ] [] 
    - [ ] [10, 20, 30, 40]
    - [X] [10, 20, 10, 20, 30, 40, 30, 40]
    
    
    On rappelle que le code 
    
    ```python
    tab = [i*j for i in range(1,10) for j in range(1,10)]
    ``` 
    
    est équivalent au code:
    
    ```python
    tab = []
    for i in range(1,10):
        for j in range(1,10):
            tab.append(i*j)
    ```


    On a donc:
    
    ```
    >>> tab = [i*j for i in range(1,10) for j in range(1,10)]
    >>> tab
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 2, 4, 6, 8, 10, 
    12, 14, 16, 18, 3, 6, 9, 12, 15, 18, 21, 24, 
    27, 4, 8, 12, 16, 20, 24, 28, 32, 36, 5, 10, 
    15, 20, 25, 30, 35, 40, 45, 6, 12, 18, 24, 30, 
    36, 42, 48, 54, 7, 14, 21, 28, 35, 42, 49, 56, 
    63, 8, 16, 24, 32, 40, 48, 56, 64, 72, 9, 18, 
    27, 36, 45, 54, 63, 72, 81]
    ```

    
   
