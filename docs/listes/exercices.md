# Exercices



 

## Exercice 



Donner un code possible pour le corps de la fonction suivante:

```python
def max_avec_absent(tab, i):
    """
    tab -- liste d'entiers
    i -- indice de liste
    
    
    Renvoie un couple (j, v) où j est l'indice de la valeur v
    dans tab et où v est la valeur maximale de tab privée de tab[i].
    >>> max_avec_absent([2, 8, 3, 5], 0)
    (1, 8)
    >>> max_avec_absent([2, 8, 3, 5], 1)
    (3, 5)
    """
```


??? solution "Réponse"

    Un code possible:
    

    ```python
    def max_avec_absent(tab, i):
        """
        tab -- liste d'entiers
        i -- indice de liste
        
        Renvoie un couple (j, v) où j est l'indice de la valeur v
        dans tab et où v est la valeur maximale de tab privée de tab[i].
        >>> max_avec_absent([2, 8, 3, 5], 0)
        (1, 8)
        >>> max_avec_absent([2, 8, 3, 5], 1)
        (3, 5)
        """
        assert len(tab) > 1, "Attention tab doit contenir au moins deux éléments."
        if i == 0: 
            indice = 1
            maxi = tab[1]
        else:
            indice = 0
            maxi = tab[0]
        for k, v in enumerate(tab):
            if k!=i and v > maxi:
                indice = k
                maxi = v
        return (indice, maxi)
    ```



## Exercice  


Donner un code possible pour le corps de la fonction suivante:

```python
def predecesseur(tab, v):
    """
    tab -- liste d'entiers
    préconditions:  len(tab) > 1, éléments de tab tous distincts
    v -- valeur d'un élément de tab
    
    renvoie la plus grande valeur de tab  qui soit strictement inférieure à v.
    renvoie  -inf si cette valeur n'existe pas.
    >>> predecesseur([3, 2, 8, 5], 2)
    -inf
    >>> predecesseur([3, 2, 8, 5], 3)
    2
    >>> predecesseur([3, 2, 8,  5],8)
    5
    """
```


??? solution "Réponse"

    Un code possible:
    
        
    ```python
    import math

    def tous_distincts(tab):
        """
        tab -- liste 
        
        renvoie True si tous les éléments de tab sont différents, False sinon.
        """
        for i in range(len(tab)-1):
            for j in range(i+1, len(tab)):
                if tab[i] == tab[j]: return False
        return True

    def predecesseur(tab, v):
        """
        tab -- liste d'entiers
        préconditions:  len(tab) > 1, éléments de tab tous distincts
        v -- valeur d'un élément de tab
        
        renvoie la plus grande valeur de tab  qui soit strictement inférieure à v.
        renvoie  -inf si cette valeur n'existe pas.
        >>> predecesseur([3, 2, 8, 5], 2)
        -inf
        >>> predecesseur([3, 2, 8, 5], 3)
        2
        >>> predecesseur([3, 2, 8,  5],8)
        5
        """
        assert v in tab, "L'argument v doit être élément de tab." 
        assert tous_distincts(tab), "Les éléments de tab doivent être distincts."
        assert len(tab)>1, "tab doit contenir au moins deux éléments."
        
        pred = -math.inf  
        for w in tab:
            if pred < w < v:
                pred = w
        return pred
    ```



## Exercice  


Donner un code possible pour le corps de la fonction suivante:

```python
def maxi(liste, f):
    """
    liste -- liste d'entiers
    f -- fonction int -> int
    
    renvoie le maximum des f(x), x parcourant la liste
    """
```

Tester ensuite votre fonction avec une fonction `valeur_absolue` que vous aurez définie.

On doit par exemple obtenir:

```python
>>> maxi([2, 8, -9, 3], valeur_absolue)
9
```


??? solution "Un code possible"


    ```python
    def valeur_absolue(x):
        if x > 0:
            return x
        else:
            return -x
            
     
     
    def maxi(liste, f):
        """
        liste -- liste d'entiers
        f -- fonction int -> int
        
        renvoie le maximum des f(x), x parcourant la liste
        """
        m = f(liste[0])
        for v in liste:
            if f(v) > m:
                m = f(v)
        return m
        
        
     
    print(maxi([2, 8, -9, 3], valeur_absolue))
    ```
    
    
## Exercice  


&Eacute;crire un code possible pour la fonction suivante:



```python
def maxiEcartMoy(liste):
    """
    liste -- liste de nombres
    
    renvoie la plus grande distance entre un nombre de la liste et la moyenne de cette liste.
    >>> maxiEcartMoy([8, 10, 12, 14]) # moyenne: 11, distance 3 entre 8 et 11 (ou entre 14 et 11)
    3.0
    """
```

??? solution "Un code possible"


    ```python
    def distance(a,b):
        """
        a et b sont deux nombres
        
        renvoie la distance entre ces deux nombres.
        """
        if a > b:
            return a-b
        else:
            return b-a
            
            
            
            
    def moyenne(liste):
        """
        liste -- liste de nombres
        
        renvoie la moyenne des nombres de la liste.
        """
        s = 0
        for v in liste:
            s += v
        return s/len(liste)
        
        
        
        
    def maxiEcartMoy(liste):
        """
        liste -- liste de nombres
        
        renvoie la plus grande distance entre un nombre de la liste et la moyenne de cette liste.
        >>> maxiEcartMoy([8, 10, 12, 14]) # moyenne: 11, distance 3 entre 8 et 11 (ou entre 14 et 11)
        3.0
        """
        moy = moyenne(liste)
        m = distance(liste[0], moy)
        for v in liste:
            d = distance(v, moy)
            if d > m: m = d
        return m
        

     
    print(maxiEcartMoy([8, 10, 12, 14]))
    ```
