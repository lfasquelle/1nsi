# Définir une liste en compréhension




## Définir une liste en compréhension


Quelques exemples illustrent la notion:


+ Définir la liste des entiers pairs compris entre 2 et 20.

 

```python
>>> A = [i for i in range(2,21) if i%2 == 0]
>>> A
[2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
```


+ Définir une liste au hasard

!!! note
    La fonction randint du module random permet de générer des entiers au hasard.
    `randint(a,b)`  (où a et b sont des entiers, a &le; b)  choisit un entier au hasard
    entre a et b (bornes a et b comprises).
    
    
Une liste de longueur 20 avec des éléments au hasard entre -10 et 10:

```python
>>> from random import randint
>>> A = [randint(-10,10) for _ in range(20)]
>>> A
[-1, 2, -5, -4, -10, -10, 2, -10, 2, 9, 5, 1, -8, 6, 1, 10, -3, 1, 1, 2]
>>> A = [randint(-10,10) for _ in range(20)]
>>> A
[-3, 6, 9, -2, 10, -7, -9, 3, 2, 8, 1, 4, 10, -10, 8, 8, 10, -4, -7, -2]

```
 
!!! remarque
    On a utilisé `_` comme nom de variable parcourant 0, 1, ..., 19 ci-dessus.
    Un nom de variable peut en effet commencer par un underscore en langage python. Et on peut
    choisir l'underscore seul. On l'utilisera quand on a besoin, comme ici, d'un nom de variable "anonyme":
    la syntaxe pour parcourir les valeurs de 0 à 19 (`range(20)`) demande en effet explicitement un nom de variable, 
    mais cette variable n'est pas utilisée ici explicitement (elle ne sert que de compteur sous-jacent pour être certain de créer 
    20 valeurs), un nom "anonyme" tel que l'underscore permet de souligner cette *absence de rôle* autre que contrôler
    le nombre d'exécutions du corps de boucle.
    
    
    !!! note "Précisions"
    Rappelons ici les règles de nommage des variables python.
    
    Le nom d'une variable en Python peut être constitué de lettres minuscules (a à z), de lettres majuscules (A à Z), 
    de nombres (0 à 9) ou du caractère souligné (_). Vous ne pouvez pas utiliser d'espace dans un nom de variable.

    Un nom de variable ne doit pas débuter par un chiffre.  
    En général, ne faîtes pas débuter un nom de variable par le caractère _  car nous verrons que ce choix
    a une signification dans le monde de la programmation python, que vous découvrirez plus tard.

    Il faut absolument éviter d'utiliser un mot « réservé » par Python comme nom de variable 
    (par exemple : print, range, for, from, etc.) car vous redéfiniriez alors localement ce nom, ce qui peut être source
    de bugs.

    Enfin, Python est sensible à la casse, ce qui signifie que les variables TesT, test ou TEST sont différentes.


## Exercice  


Proposer un code pour la fonction suivante:

```python
def sousListeMultiplesDe5(tab):
    """
    tab -- liste d'entiers
    
    renvoie une liste ne contenant que les éléments de tab 
    qui sont multiples de 5.
    """
```

Par exemple:

```
>>> sousListeMultiplesDe5([2, 5, 7, 10, 19, 20, 21])
[5, 10, 20]
```


??? solution "Un code"

    ```python
    def sousListeMultiplesDe5(tab):
        """
        tab -- liste d'entiers
        
        renvoie une liste ne contenant que les éléments de tab 
        qui sont multiples de 5.
        """
        return [elt for elt in tab if elt%5 == 0]
    ```


## Exercice  



Proposer un code pour la fonction suivante:

```python
def avantPivot(tab, valeur):
    """
    tab -- liste d'entiers
    valeur -- entier
    
    renvoie une liste contenant les éléments de tab qui sont < valeur.
    >>> avantPivot([2, 6, 3, 9, 4, 42], 7)
    [2, 6, 3, 4]
    """
```


??? solution "Un code possible"




    ```python
    def avantPivot(tab, valeur):
        """
        tab -- liste d'entiers
        valeur -- entier
        
        renvoie une liste contenant les éléments de tab qui sont < valeur.
        >>> avantPivot([2, 6, 3, 9, 4, 42], 7)
        [2, 6, 3, 4]
        """
        return [elt for elt in tab if elt < valeur]
    ```






## Exercice  

Proposer un code qui permette de copier une liste A dans une liste B.





!!! important  
    Si l'on dispose d'une liste A, le code `B = A` ne copie pas A dans B. 
    Ce code consiste à donner un second nom à la même liste.
    On a ainsi un seul objet en mémoire, qui porte deux noms.
    
    Dans l'exercice, on veut deux objets, l'un s'appelle A, l'autre B. Et leurs contenus
    sont les mêmes.
    
    Si l'on compare une liste à une bouteille:
    
    + Avec `B = A`, notre bouteille porte les étiquettes A et B.
    + Tandis que dans l'exercice, on veut une bouteille étiquetée A, une bouteille étiquetée B,
    mais les deux bouteilles ont des contenus identiques (par exemple toutes deux
    contiennent un litre de lait).
    
    Nous revenons sur ce principe dans les pages suivantes.
    
    
    
Par exemple:


```
>>> A = [42, 666, 1789]
>>> B = copie(A)
>>> B
[42, 666, 1789]
>>> B is A # B et A ne sont pas le même objet
False
>>> B == A # B et A ont le même contenu
True
```

??? solution "Un code"

    ```python
    def copie(tab):
        """
        tab -- liste

        renvoie une liste de même contenu que tab
      
        """
        return [element for element in tab]
    ```
    
    Si l'on n'a pas besoin d'une fonction,
    on peut écrire directement:
    
    ```
    B = [elt for elt in A]
    ```
    
    !!! note
        Lorsque les éléments de A sont eux-mêmes des listes,  cette fonction de copie
        ne fonctionnerait pas tout à fait comme attendu...
        Nous en reparlerons.



  


