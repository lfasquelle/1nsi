# Concaténer deux listes



## En créant une nouvelle liste 


On peut créer une nouvelle liste en concaténant deux listes avec l'opérateur +:

```
>>> a = [1,3,7]
>>> b = [42, 666, 1789]
>>> c = a + b
>>> c
[1, 3, 7, 42, 666, 1789]
```

Attention, on crée bien une nouvelle liste.
Dans le code suivant:


```
>>> a = [1,3,7]
>>> b = [42, 666, 1789]
>>> a = a + b
>>> a
[1, 3, 7, 42, 666, 1789]
```
on n'a pas modifié la liste [1, 3, 7] intiale bien que l'on donne l'étiquette a à la nouvelle liste.

+ En ligne 1, on crée l'objet liste [1, 3, 7] puis on lui "colle" l'étiquette a.
+ En ligne 2, on crée l'objet liste [42, 666, 1789] puis on lui colle l'étiquette b.
+ En ligne 3, on concatène les deux listes étiquetées a et b, on crée ainsi un nouvel objet liste [1, 3, 7, 42, 66, 1789]
puis on décolle l'étiquette a de son ancien objet pour venir la coller sur ce nouvel objet (l'objet [1, 3, 7] n'est 
donc à ce stade plus étiqueté et donc n'est plus accessible).




## En prolongeant une liste existante

  
&Eacute;crire le corps possible de la fonction ci-dessous:

```python
def prolonger(liste1, liste2):
    """
    liste1 -- de type list
    liste2 -- de type list
    
    Prolonge liste1 (en place, sans créer de nouvelle liste) 
    par les éléments de liste2
    (la fonction ne renvoie rien)
    
    >>> liste1 = [1, 2, 3]
    >>> liste2 = [42, 666, 777]
    >>> prolonger(liste1, liste2)
    >>> liste1
    [1, 2, 3, 42, 666, 777]
    """
``` 


??? solution "Un code possible"

    ```python
    def prolonger(liste1, liste2):
        """
        liste1 -- de type list
        liste2 -- de type list
        
        Prolonge liste1 (en place, sans créer de nouvelle liste) 
        par les éléments de liste2
        (la fonction ne renvoie rien)
        
        >>> liste1 = [1, 2, 3]
        >>> liste2 = [42, 666, 777]
        >>> prolonger(liste1, liste2)
        >>> liste1
        [1, 2, 3, 42, 666, 777]
        """
        for element in liste2:
            liste1.append(element)
    ```

    Vérifiez, en utilisant `id`, que votre objet liste1 est bien le même objet (dont le contenu a changé)
    avant l'appel de la fonction et après.
    Voir par exemple [ce fichier ipynb](fichiers/concat.ipynb) ou [sa version html](fichiers/concat.html).
    
    !!! note
        On rappelle qu'une fonction comme la précédente qui modifie un objet défini en-dehors de la fonction
        est dite "fonction à effet de bord".
    
    
??? solution "Remarque"
    Le langage python propose en fait déjà en natif une telle fonction.
    Voir par exemple [cette page](https://www.tutorialspoint.com/python/list_extend.htm).
    
