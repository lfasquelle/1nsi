# Des compteurs




!!! important
    On rappelle que l'entraînement à la programmation est indispensable pour apprendre à programmer...




   
## Exercice 1

&Eacute;crire un corps possible pour la fonction suivante:

```python
def fin_en(syllabe, liste_mots):
    """
    liste_mots -- liste de chaîne de caractères
    syllabe -- chaîne de caractères
    
    renvoie le nombre de chaînes dans liste_mots terminant par syllabe
    
    >>> fin_en('e', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
    3
    >>> fin_en('er', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
    0
    >>> fin_en('er', ['permettre', 'autoriser', 'légitimer', 'accepter', 'tolérer'])
    4
    """
```


??? solution "Un code possible"

    Ce [fichier ipynb](fichiers/compteur1.ipynb) propose un code possible.
    
    [Version statique html](fichiers/compteur1.html):
    
    <iframe src="fichiers/compteur1.html" width=100% height=400px></iframe>
    
    
    
## Exercice 2

&Eacute;crire une fonction python spécifiée comme suit:

```python
def au_moins_un(lettre, liste_mots):
    """
    lettre -- un caractère
    liste_mots -- liste de chaînes de caractères
    
    renvoie le nombre d'éléments de la liste contenant au moins une fois 
    le caractère lettre
    
    >>> au_moins_un('a', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
    3
    >>> au_moins_un('g', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
    0
    """
```

??? solution "Un code"

    ```python
    def au_moins_un(lettre, liste_mots):
        """
        lettre -- un caractère
        liste_mots -- liste de chaînes de caractères
        
        renvoie le nombre d'éléments de la liste contenant au moins une fois 
        le caractère lettre
        
        >>> au_moins_un('a', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
        3
        >>> au_moins_un('g', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
        0
        """
        compteur = 0
        for mot in liste_mots:
            if lettre in mot:
                compteur += 1
        return compteur
    ```


## Exercice 3

&Eacute;crire une fonction python spécifiée comme suit:



```python
def liste_exactement_un(lettre, liste_mots):
    """
    lettre -- un caractère
    liste_mots -- liste de chaînes de caractères
    
    renvoie le nombre d'éléments de la liste contenant exactement une fois 
    le caractère lettre
    
    >>> liste_exactement_un('a', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
    0
    >>> liste_exactement_un('c', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
    3
    """
```



??? solution "Un code"

    Comme d'habitude, on  **doit** découper en fonctions...
    
    ```python
    def chaine_exactement_un(caractere, mot):
        """
        mot -- chaîne de caractères
        caractère -- un caractère
        
        renvoie True si mot présente exactement un caractère égal à caractere
        et renvoie False sinon.
        >>> chaine_exactement_un('a', 'arbre')
        True
        >>> chaine_exactement_un('a', 'abracadabra')
        False
        """
        compteur = 0
        for carac in mot:
            if carac == caractere:
                compteur += 1
        return compteur == 1
        
    def liste_exactement_un(lettre, liste_mots):
        """
        lettre -- un caractère
        liste_mots -- liste de chaînes de caractères
        
        renvoie le nombre d'éléments de la liste contenant exactement une fois 
        le caractère lettre
        
        >>> liste_exactement_un('a', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
        0
        >>> liste_exactement_un('c', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
        3
        """
        compteur = 0
        for mot in liste_mots:
            if chaine_exactement_un(lettre, mot):
                compteur += 1
        return compteur
    ```







## Exercice  4


&Eacute;crire une fonction python spécifiée comme suit:




```python
def liste_au_plus_un(lettre, liste_mots):
    """
    lettre -- un caractère
    liste_mots -- liste de chaînes de caractères
    
    renvoie le nombre d'éléments de la liste contenant au plus une fois 
    le caractère lettre
    
    >>> liste_au_plus_un('a', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
    2
    >>> liste_au_plus_un('z', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
    5
    """
```



??? solution "Un code"

    Comme d'habitude, on  **doit** découper en fonctions...
    
    ```python
    def chaine_au_plus_un(caractere, mot):
        """
        mot -- chaîne de caractères
        caractère -- un caractère
        
        renvoie True si mot présente au plus une occurrence
        de  caractere
        et renvoie False sinon.
        >>> chaine_au_plus_un('a', 'arbre')
        True
        >>> chaine_au_plus_un('a', 'abracadabra')
        False
        >>> chaine_au_plus_un('a', 'biniou')
        True
        """
        compteur = 0
        for carac in mot:
            if carac == caractere:
                compteur += 1
        return compteur <= 1
        
    def liste_au_plus_un(lettre, liste_mots):
        """
        lettre -- un caractère
        liste_mots -- liste de chaînes de caractères
        
        renvoie le nombre d'éléments de la liste contenant au plus une fois 
        le caractère lettre
        
        >>> liste_au_plus_un('a', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
        2
        >>> liste_au_plus_un('z', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
        5
        """
        compteur = 0
        for mot in liste_mots:
            if chaine_au_plus_un(lettre, mot):
                compteur += 1
        return compteur
    ```







## Exercice  5

&Eacute;crire une fonction python spécifiée comme suit:




```python
def liste_nombre_total(lettre, liste_mots):
    """
    lettre -- un caractère
    liste_mots -- liste de chaînes de caractères
    
    renvoie le nombre d'occurrences de lettre dans les mots de liste_mots.
    
    >>> liste_nombre_total('a', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
    9
    >>> liste_nombre_total('z', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
    0
    """
```


??? solution "Un code"

    ```python
    def chaine_nombre_total(lettre, chaine):
        """
        chaine -- chaîne de caractères
        lettre -- caractère
        
        renvoie le nombre d'occurrences de lettre dans chaine.
        >>> chaine_nombre_total('e', 'chaînette')
        2
        >>> chaine_nombre_total('a', 'Barbapapa')
        4
        """
        compteur = 0
        for carac in chaine:
            if carac == lettre:
                compteur += 1
        return compteur
        
    def liste_nombre_total(lettre, liste_mots):
        """
        lettre -- un caractère
        liste_mots -- liste de chaînes de caractères
        
        renvoie le nombre d'occurrences de lettre dans les mots de liste_mots.
        
        >>> liste_nombre_total('a', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
        9
        >>> liste_nombre_total('z', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
        0
        """
        compteur = 0
        for mot in liste_mots:
            compteur += chaine_nombre_total(lettre, mot)
        return compteur
    ```



??? solution "Un autre découpage"


    ```python
    def chaine_nombre_total(lettre, chaine):
        """
        chaine -- chaîne de caractères
        lettre -- caractère
        
        renvoie le nombre d'occurrences de lettre dans chaine.
        >>> chaine_nombre_total('e', 'chaînette')
        2
        >>> chaine_nombre_total('a', 'Barbapapa')
        4
        """
        compteur = 0
        for carac in chaine:
            if carac == lettre:
                compteur += 1
        return compteur
        
    def somme(liste_entiers):
        """
        liste_entiers -- liste d'entiers
        
        renvoie la somme des éléments de liste_entiers
        """
        s = 0
        for element in liste_entiers:
            s += element
        return s
        
        
    def liste_nombre_total(lettre, liste_mots):
            """
            lettre -- un caractère
            liste_mots -- liste de chaînes de caractères
            
            renvoie le nombre d'occurrences de lettre dans les mots de liste_mots.
            
            >>> liste_nombre_total('a', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
            9
            >>> liste_nombre_total('z', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'])
            0
            """
            return somme([chaine_nombre_total(lettre, mot) for mot in liste_mots])
    ```

    
## Exercice  6


&Eacute;crire une fonction python spécifiée comme suit:




```python
def liste_exactement(lettre, liste_mots, n):
        """
        lettre -- un caractère
        liste_mots -- liste de chaînes de caractères
        n -- entier naturel
        
        renvoie le nombre de chaînes  de liste_mots
        qui compte exactement n occurrences de lettre.
        
        >>> liste_exactement('a', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'], 2)
        2
        >>> liste_exactement('a', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'], 5)
        1
        """
```

??? solution "Un code"

    ```python
    def chaine_exactement(lettre, chaine, n):
        """
        chaine -- chaîne de caractères
        lettre -- caractère
        n -- entier naturel
        
        renvoie True si chaine présente exactement n occurrences de lettre.
        
        >>> chaine_exactement('e', 'chaînette', 2)
        True
        >>> chaine_exactement('a', 'Barbapapa', 5)
        False
        """
        compteur = 0
        for carac in chaine:
            if carac == lettre:
                compteur += 1
        return compteur == n
        
        
    def somme(liste_entiers):
        """
        liste_entiers -- liste d'entiers
        
        renvoie la somme des éléments de liste_entiers
        """
        s = 0
        for element in liste_entiers:
            s += element
        return s
        
        
    def liste_exactement(lettre, liste_mots, n):
            """
            lettre -- un caractère
            liste_mots -- liste de chaînes de caractères
            n -- entier naturel
            
            renvoie le nombre de chaînes  de liste_mots
            qui compte exactement n occurrences de lettre.
            
            >>> liste_exactement('a', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'], 2)
            2
            >>> liste_exactement('a', ['abracadabra', 'entourloupe', 'machination', 'escroquerie', 'passe-passe'], 5)
            1
            """
            return somme([1 for mot in liste_mots if chaine_exactement(lettre, mot, n)])
    ```
    
    
    
    
    

## Exercice  7
 


&Eacute;crire une fonction python spécifiée comme suit:




```python
def nb_occ_maxi_lettres(tab, lettre):
    """
    tab -- liste de chaînes de caractères
    lettre -- caractère
    
    renvoie le nombre de chaînes de la liste tab contenant
    un nombre d'occurrences de lettre égal au maximum des nombres
    d'occurrence de lettre dans les chaînes de tab.
    
    >>> nb_occ_maxi_lettres(['aha', 'abraca', 'ah?', 'amanda'], 'a') 
    2
    >>> nb_occ_maxi_lettres(['aha', 'abraca', 'ah?', 'amanda'], 'z') 
    4
    """
```

??? solution "Un code"

    ```python
    def nb_lettres(mot, lettre):
        """
        mot -- chaîne de caractères
        lettre -- caractère
        
        renvoie le nombre d'occurrences de lettre dans mot
        """
        compteur = 0
        for x in mot:
            if x == lettre:
                compteur += 1
        return compteur

    def maxi_lettres(tab, lettre):
        """
        tab -- liste de chaînes de caractères
        lettre -- caractère
        
        renvoie le nombre maximal d'occurrences de lettre 
        pour les chaînes de la liste tab.
        
        >>> maxi_lettres(['aha', 'abraca', 'ah?'], 'a') # renvoie 3 car abraca présente 3 'a'
        3
        """
        compteur = 0
        maxi = 0
        for mot in tab:
            effectif = nb_lettres(mot, lettre)
            if effectif > maxi:
                maxi = effectif
        return maxi


    def nb_occ_maxi_lettres(tab, lettre):
        """
        tab -- liste de chaînes de caractères
        lettre -- caractère
        
        renvoie le nombre de chaînes de la liste tab contenant
        un nombre d'occurrences de lettre égal au maximum des nombres
        d'occurrence de lettre dans les chaînes de tab.
        
        >>> nb_occ_maxi_lettres(['aha', 'abraca', 'ah?', 'amanda'], 'a') 
        2
        >>> nb_occ_maxi_lettres(['aha', 'abraca', 'ah?', 'amanda'], 'z') 
        4
        """
        compteur = 0
        maxi = maxi_lettres(tab, lettre)
        for mot in tab:
            if nb_lettres(mot, lettre) == maxi:
                compteur += 1
            else: 
                pass
        return compteur
    ```












## Exercice 8

 


&Eacute;crire une fonction python spécifiée comme suit:




```python
def compte_occ_entiers(liste, n):
    """
    n -- entier naturel 
    liste -- liste d'entiers entre 0 et n
    
    renvoie une liste compteurs telle que compteurs[i] est le nombre
    d'occurrences de i dans liste (pour i entre 0 et n).
    
    >>> compte_occ_entiers([4, 3, 4, 2], 9)
    [0, 0, 1, 1, 2, 0, 0, 0, 0, 0]
    >>> compte_occ_entiers([0, 1, 2, 3, 7, 8, 9], 9)
    [1, 1, 1, 1, 0, 0, 0, 1, 1, 1]
    """
```

??? solution "Un code"

    ```python
    def compte_occ_entiers(liste, n):
        """
        n -- entier naturel 
        liste -- liste d'entiers entre 0 et n
        
        renvoie une liste compteurs telle que compteurs[i] est le nombre
        d'occurrences de i dans liste (pour i entre 0 et n).
        
        >>> compte_occ_entiers([4, 3, 4, 2], 9)
        [0, 0, 1, 1, 2, 0, 0, 0, 0, 0]
        >>> compte_occ_entiers([0, 1, 2, 3, 7, 8, 9], 9)
        [1, 1, 1, 1, 0, 0, 0, 1, 1, 1]
        """
        compteurs = [0 for i in range(0,n+1)]
        for entier in liste:
            compteurs[entier] += 1
        return compteurs        
    ```
