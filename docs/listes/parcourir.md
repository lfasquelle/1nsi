# Parcourir les éléments d'une liste


Comme pour les chaînes et les tuples, pour parcourir les éléments d'une liste L on utilisera:

- `for element in L`  
- `for indice, element  in enumerate(L) `  
- `for indice in range(len(L)) `   


## Parcours des valeurs


On peut parcourir les valeurs d'une liste par `for in`:

```
>>> tab = [42, 666, 1789, 1968, 1991, 2001]
>>> for valeur in tab:
...     print(valeur)
... 
42
666
1789
1968
1991
2001
```


## Parcours des indices et valeurs


 
On obtient indice, valeur par:

```
>>> tab = [42, 666, 1789, 1968, 1991, 2001]
>>> for indice, valeur in enumerate(tab):
...     print(indice, valeur)
... 
0 42
1 666
2 1789
3 1968
4 1991
5 2001
```


## Parcours par les indices

```
>>> tab = [42, 666, 1789, 1968, 1991, 2001]
>>> for indice in range(len(tab)):
...     print(indice, tab[indice])
... 
0 42
1 666
2 1789
3 1968
4 1991
5 2001
```


## Somme d'éléments


Dans les programmes demandés ci-dessous, on va sommer des éléments de  liste.

On a déjà rencontré ce type d'algorithme à plusieurs reprises: il s'agit du principe
d'accumulateur dont la non maîtrise serait, à ce stade de l'année, rédhibitoire et 
serait le signe qu'il faut absolument vous entraîner plus intensément à la programmation.

Rappelons le principe:

+ On utilise une variable s initialisée à 0 dans laquelle on va accumuler (par addition) un à un les
éléments de la liste que l'on veut garder.
+ Pour réaliser cette accumulation, on parcourt les éléments de la liste (boucle for): on ajoute
chacun des éléments ainsi rencontrés à la variable s.
+ En sortie de boucle, s désigne la valeur somme des éléments qu'il fallait sommer.



### Exercice  

Proposer le code d'une fonction qui prend en paramètre une liste (on supposera
que les éléments de la liste sont des entiers) et renvoie en sortie
la somme des éléments de cette liste.



 

??? solution "Un code"

    ```python
    def somme(tab):
        """
        tab -- liste d'entiers
        
        renvoie la somme des éléments de la liste
        >>> somme([])
        0
        >>> somme([3])
        3
        >>> somme([1,2,3])
        6
        """
        s = 0
        for element in tab: s += element
        return s
    ```


    On peut également utiliser les autres façons de parcourir les éléments de liste (mais c'est ici inutile).
    
    


### Exercice  

Proposer un code pour la fonction suivante:

```python
def sommeIndicesPairs(tab):
    """
    tab -- liste de nombres
    
    renvoie la somme des éléments d'indices pairs de la liste
    >>> sommeIndicesPairs([])
    0
    >>> sommeIndicesPairs([3])
    3
    >>> sommeIndicesPairs([3,2])
    3
    >>> sommeIndicesPairs([3,2,4,5])
    7
    """
    
```

??? solution "Un code"

    ```python
    def sommeIndicesPairs(tab):
        """
        tab -- liste de nombres
        
        renvoie la somme des éléments d'indices pairs de la liste
        >>> sommeIndicesPairs([])
        0
        >>> sommeIndicesPairs([3])
        3
        >>> sommeIndicesPairs([3,2])
        3
        >>> sommeIndicesPairs([3,2,4,5])
        7
        """
        s = 0
        for indice, element in enumerate(tab): 
            if indice%2 == 0:
                s += element
        return s
    ```





### Exercice 

Proposer un code pour la fonction suivante:

```python
def sommeElementsPairs(tab):
    """
    tab -- liste de nombres
    
    renvoie la somme des éléments pairs de la liste
    >>> sommeElementsPairs([])
    0
    >>> sommeElementsPairs([3])
    0
    >>> sommeElementsPairs([3,2])
    2
    >>> sommeElementsPairs([3,2,4,5])
    6
    """
```


??? attention 
	Distinguer bien la différence entre la demande de cet exercice
	et celle de l'exercice précédent. Dans le présent exercice, on impose 
	une parité sur les **éléments** de la liste, tandis que dans l'exercice
	précédent, le contrôle de parité se faisait sur les **indices**.

??? solution "Un code"

    ```python
    def sommeElementsPairs(tab):
        """
        tab -- liste de nombres
        
        renvoie la somme des éléments pairs de la liste
        >>> sommeElementsPairs([])
        0
        >>> sommeElementsPairs([3])
        0
        >>> sommeElementsPairs([3,2])
        2
        >>> sommeElementsPairs([3,2,4,5])
        6
        """
        s = 0
        for element in tab: 
            if element%2 == 0:
                s += element
        return s
    ```
