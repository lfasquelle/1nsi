# Des accumulateurs


Les compteurs de la page précédente utilisent le *principe d'accumulateur*  déjà utilisé de nombreuses fois.

On continue avec ce principe incontournable mais en accumulant des caractères ou des éléments dans une liste
(plutôt qu'en accumulant des entiers par additions comme avec les compteurs).


## Masque

&Eacute;crire un corps pour la fonction suivante:


```python
def masque(mot, tab):
    """
    mot -- de type str
    tab -- liste d'entiers compris entre 0 et len(mot)-1.
    
    renvoie une chaîne de caractères construites avec les caractères de mot
    dont les indices dans mot sont donnés par tab.
    Exemples:
    >>> masque("cou", [0,1,0,1])
    'coco'
    >>> masque("macron", [0,1,3,3,4,5])
    'marron'
    >>> masque("pantalonnade", [1,2,1,3,6,5,11])
    'anatole'
    """
    
    
```


??? solution

    ```python
    def masque(mot, tab):
        """
        mot -- de type str
        tab -- liste d'entiers compris entre 0 et len(mot)-1.
        
        renvoie une chaîne de caractères construites avec les caractères de mot
        dont les indices dans mot sont donnés par tab.
        Exemples:
        >>> masque("cou", [0,1,0,1])
        'coco'
        >>> masque("macron", [0,1,3,3,4,5])
        'marron'
        >>> masque("pantalonnade", [1,2,1,3,6,5,11])
        'anatole'
        """
        
        resultat = ''
        for k in tab:
            resultat += mot[k]
        return resultat
    ```


## Compactage



&Eacute;crire un corps pour la fonction suivante:


```python
def compactage(tab):
    """
    tab -- liste non vide d'entiers triée en ordre croissant.
    
    renvoie une liste de même contenu que tab mais sans doublon.
    >>> compactage([2,2, 3, 3, 4,4, 4, 4, 5, 6, 6, 8])
    [2, 3, 4, 5, 6, 8]
    """
 
```

??? solution 



    ```python
    def compactage(tab):
        """
        tab -- liste non vide d'entiers triée en ordre croissant.
        
        renvoie une liste de même contenu que tab mais sans doublon.
        >>> compactage([2,2, 3, 3, 4,4, 4, 4, 5, 6, 6, 8])
        [2, 3, 4, 5, 6, 8]
        """
        
        resultat = [tab[0]]
        for i in range(1, len(tab)):
            if tab[i] != tab[i-1]:
                resultat.append(tab[i])
        return resultat

     
    assert compactage([2,2, 3, 3, 4,4, 4, 4, 5, 6, 6, 8]) == [2, 3, 4, 5, 6, 8]
    ```
