# Entraînement




Les exercices des pages qui suivent sont des exercices d'entraînement.

Ils sont assez nombreux: la maîtrise des listes est en effet un objectif essentiel du programme de première NSI.


On rappelle que, contrairement au reste des pages, ils ne sont pas à connaître parfaitement. Mais
ne négligez pas le travail d'entraînement, indispensable pour acquérir le niveau nécessaire en programmation.
