# QCM


!!! note  
    Les QCM sont là pour vous aider à contrôler ce que vous avez retenu.
    Si vous ne répondez pas à toutes les questions sans hésitation, c'est sans doute
    qu'il faut refaire des lectures des pages précédentes.

Trouvez la ou les bonnes réponses.



## QCM 1


Après les lignes:

```
a = [2, 3, 4]
b = a
b[2] = 666
```

a a pour valeur:

- [ ] [2, 3, 4]
- [ ] [2, 3, 666]


??? solution "Réponse"

    - [ ] [2, 3, 4]
    - [X] [2, 3, 666]
    
    

## QCM 2


Après les lignes:

```
a = [2, 3, 4]
b = [x for x in a]
b[2] = 666
```

a a pour valeur:

- [ ] [2, 3, 4]
- [ ] [2, 3, 666]


??? solution "Réponse"

    - [X] [2, 3, 4]
    - [ ] [2, 3, 666]
    
    
    

## QCM 3


Après les lignes:

```
def f(liste):
    liste[0] = 1515
    
a = [3, 4, 5]
f(a)
```

a a pour valeur:

- [ ] [3, 4, 5]
- [ ] [1515, 4, 5]


??? solution "Réponse"

    - [ ] [3, 4, 5]
    - [X] [1515, 4, 5]







## QCM 4


Après les lignes:

```
def f(liste):
    liste.append(2020)
    
a = [3, 4, 5]
f(a)
```

a a pour valeur:

- [ ] [3, 4, 5]
- [ ] [3, 4, 5, 2020]


??? solution "Réponse"

    - [ ] [3, 4, 5]
    - [X] [3, 4, 5, 2020]
    
    



## QCM 5


Après les lignes:

```
def f(liste):
    liste = liste + [2020]
    
a = [3, 4, 5]
f(a)
```

a a pour valeur:

- [ ] [3, 4, 5]
- [ ] [3, 4, 5, 2020]


??? solution "Réponse"

    - [X] [3, 4, 5]
    - [ ] [3, 4, 5, 2020]    
