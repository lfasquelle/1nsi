# Des requêtes


On dispose de listes construites comme définies ci-dessous:


+ Liste de personnes constituée de tuples (identifiant, nom, prénom, année de naissance).

```python
personnes = [ ('pers1', 'Labrosse', 'Adam', 2000), 
('pers2','Gemlamorte', 'Adèle', 1985), 
('pers3','Auboisdormant', 'Abel', 2001),
('pers4','Etpan', 'Ahmed', 1975), 
('pers5','Térieur', 'Alain', 1999), 
('pers6','Térieur', 'Alex', 1976), 
('pers7','Tanrien', 'Jean', 2010),
('pers8','Ouzi', 'Jacques', 1950),
('pers9','Deuf', 'John', 2006),
('pers10','Provist', 'Alain', 2011)
]
```

+ Liste de cours constituée de tuples (identifiant du cours, identifiant de personne, intitulé du cours).
L'identifiant de personne est lié à la liste `personnes` précédente et désigne
la personne qui enseigne ce cours.

```python
cours = [('crs1', 'pers2', 'théorie des graphes'),
('crs2', 'pers4', 'programmation objet'),
('crs3', 'pers6', 'programmation fonctionnelle'),
('crs4', 'pers8', 'théorie des automates'),
('crs5', 'pers10', 'base de données relationnelle'),
('crs6', 'pers3', 'réseaux'),
('crs7', 'pers9', 'logique')
]
```

Par exemple, on lit avec le dernier tuple `('crs7', 'pers9', 'logique')` que le cours
de logique est identifié par le code `crs7` et est assuré par la personne d'identifiant
`'pers9'`,  c'est à dire par John Deuf.


## Question

Créer la liste des tuples (intitulé de cours, nom de l'enseignant dispensant ce cours, prénom
de l'enseignant).


??? solution

	```python
	[(intitule, nom, prenom) 
	for (id_crs, id_pers, intitule) in cours 
	for (id_pers2, nom, prenom, annee) in personnes 
	if id_pers == id_pers2]
	```
	
	
## Question

Un enseignant est une personne de la liste personnes qui enseigne au moins un cours.
Créer la liste des enseignants (liste des couples (nom, prenom)).

??? solution

	```python
	[(nom, prenom) 
	for (id_pers, nom, prenom, _) in personnes
	if id_pers in [identif_pers for (_,identif_pers,_) in cours]
	]
	```
