# Présence d'un élément dans une liste



!!! important
    Les algorithmes et programmes de cette page sont considérés comme des 
    algorithmes de référence.   
    Vous devez parfaitement les maîtriser et être capables de redonner leur code
    très rapidement.


## Exercice  

&Eacute;crire un code possible pour la fonction suivante:

```python
def estPresent(tab, valeur):
    """
    tab -- liste d'entiers
    valeur -- entier
    
    renvoie True si valeur est élément de tab, False sinon
    >>> estPresent([], 3)
    False
    >>> estPresent([2,6], 3)
    False
    >>> estPresent([3,2,6], 3)
    True
    """
```


??? solution "Un code"

    ```python
    def estPresent(tab, valeur):
        """
        tab -- liste d'entiers
        valeur -- entier
        
        renvoie True si valeur est élément de tab, False sinon
        >>> estPresent([], 3)
        False
        >>> estPresent([2,6], 3)
        False
        >>> estPresent([3,2,6], 3)
        True
        """
        for element in tab:
            if element == valeur: return True
        return False
    ```

    Ou, plus concis:
    
    ```python
    def estPresent(tab, valeur):
        """
        tab -- liste d'entiers
        valeur -- entier
        
        renvoie True si valeur est élément de tab, False sinon
        >>> estPresent([], 3)
        False
        >>> estPresent([2,6], 3)
        False
        >>> estPresent([3,2,6], 3)
        True
        """
        return valeur in tab
    ```






## Exercice  

&Eacute;crire un code possible pour la fonction suivante:

```python
def indicePremiereOccurrence(tab, valeur):
    """
    tab -- liste d'entiers
    valeur -- entier
    
    renvoie l'indice de la première occurrence de valeur dans tab
    si valeur est élément de tab. Renvoie None sinon.
    >>> indicePremiereOccurrence([], 3)
    None
    >>> indicePremiereOccurrence([2,6], 3)
    None
    >>> indicePremiereOccurrence([3,2,6,3], 3)
    0
    >>> indicePremiereOccurrence([2,6,3], 3)
    2 
    """
```


??? solution "Un code"


    ```python
    def indicePremiereOccurrence(tab, valeur):
        """
        tab -- liste d'entiers
        valeur -- entier
        
        renvoie l'indice de la première occurrence de valeur dans tab
        si valeur est élément de tab. Renvoie None sinon.
        >>> indicePremiereOccurrence([], 3)
        None
        >>> indicePremiereOccurrence([2,6], 3)
        None
        >>> indicePremiereOccurrence([3,2,6,3], 3)
        0
        >>> indicePremiereOccurrence([2,6,3], 3)
        2 
        """
        for j, v in enumerate(tab):
            if valeur == v: return j
        return None
    ```


    !!! important
        La ligne `return None` n'est pas obligatoire: en python, toute fonction
        sans `return` explicite renvoie `None`.












## Exercice  

&Eacute;crire un code possible pour la fonction suivante:

```python
def indiceDerniereOccurrence(tab, valeur):
    """
    tab -- liste d'entiers
    valeur -- entier
    
    renvoie l'indice de la dernière occurrence de valeur dans tab
    si valeur est élément de tab. Renvoie None sinon.
    >>> indiceDerniereOccurrence([], 3)
    None
    >>> indiceDerniereOccurrence([2,6], 3)
    None
    >>> indiceDerniereOccurrence([3,2,6,3], 3)
    3
    >>> indiceDerniereOccurrence([2,6,3], 3)
    2 
    """
```


??? solution "Une solution"

    On parcourt la liste en partant de la fin et on s'arrête dès qu'on a un élément 
    égal à valeur:

    ```python
    def indiceDerniereOccurrence(tab, valeur):
        """
        tab -- liste d'entiers
        valeur -- entier
        
        renvoie l'indice de la dernière occurrence de valeur dans tab
        si valeur est élément de tab. Renvoie None sinon.
        >>> indiceDerniereOccurrence([], 3)
        None
        >>> indiceDerniereOccurrence([2,6], 3)
        None
        >>> indiceDerniereOccurrence([3,2,6,3], 3)
        3
        >>> indiceDerniereOccurrence([2,6,3], 3)
        2 
        """
        for i in range(len(tab)-1, -1, -1):
            if valeur == tab[i]: return i
        return None
    ```
    
     
    
    

??? solution "Une autre solution"

    On parcourt la liste dans le sens croissant des indices. Mais
    on ne retient que le dernier indice coïncidant avec la demande: pour cela,
    il suffit de ne pas interrompre le parcours de liste lorsqu'on a déjà trouvé
    une coïncidence.
    
    ```python 
    def indiceDerniereOccurrence(tab, valeur):
        """
        tab -- liste d'entiers
        valeur -- entier
        
        renvoie l'indice de la dernière occurrence de valeur dans tab
        si valeur est élément de tab. Renvoie None sinon.
        >>> indiceDerniereOccurrence([], 3)
        None
        >>> indiceDerniereOccurrence([2,6], 3)
        None
        >>> indiceDerniereOccurrence([3,2,6,3], 3)
        3
        >>> indiceDerniereOccurrence([2,6,3], 3)
        2 
        """
        present = False
        for i, v in enumerate(tab):
            if v == valeur: 
                present = True
                indice = i
        if present: 
            return indice
        else:
            return None  
    ``` 

    !!! important
        La ligne `return None` n'est pas obligatoire: en python, toute fonction
        sans `return` explicite renvoie `None`.
