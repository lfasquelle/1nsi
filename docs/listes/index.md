# Tableau, liste python



Les objets de type list, de type tuple, de type str sont tous des séquences: une "collection" de valeurs 
avec des éléments indexés dans un certain ordre.

+ Nous avons vu que dans une séquence str, tous les objets de la séquence   sont de même nature: 
ce sont tous des caractères.

+ Dans une séquence de type tuple, les objets de la séquence peuvent être de types variés.

+ Dans une séquence de type list, les objets peuvent être également de types variés. Les listes et les tuples 
se ressemblent beaucoup de prime abord, avec toutefois une différence notable: les tuples sont immuables, les
listes sont muables.


## Remarque

Dans ces pages, nous manipulerons:

+ des tableaux, type abstrait.
+ des listes python, que l'on considérera comme implémentant le type abstrait tableau.



!!! note
    Nous préciserons parfois "liste python" ou "python-liste" car:
    
    + le terme liste peut aussi désigner un type abstrait de données (différent du type tableau).
    + d'autres langages de programmation peuvent nommer liste des objets n'ayant pas tout à fait
    les mêmes propriétés que les listes python.
    
     



