def compactage(tab):
    """
    tab -- liste non vide d'entiers triée en ordre croissant.
    
    renvoie une liste de même contenu que tab mais sans doublon.
    >>> compactage([2,2, 3, 3, 4,4, 4, 4, 5, 6, 6, 8])
    [2, 3, 4, 5, 6, 8]
    """
    
    resultat = [tab[0]]
    for i in range(1, len(tab)):
        if tab[i] != tab[i-1]:
            resultat.append(tab[i])
    return resultat

 
assert compactage([2,2, 3, 3, 4,4, 4, 4, 5, 6, 6, 8]) == [2, 3, 4, 5, 6, 8]
