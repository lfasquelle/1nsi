

def f1(tab, i):
    """
    tab: liste d'entiers
    i: indice d'un élément de tab
    """
    nb_elements = len(tab)
    compteur = 0
    for j in range(i+1, nb_elements):
        if tab[j] < tab[i]:
            compteur += 1
    return compteur
        
        
        
def nombre_inversion(tab):
    compteur = 0
    for i in range(0, len(tab)-1):
        compteur += f1(tab,i)
    return compteur
    
tab = [1, 5, 7]
 
