# Tableau

 


 

Un [tableau](https://fr.wikipedia.org/wiki/Tableau_(structure_de_donn%C3%A9es))
est un [type de donnée abstrait](https://fr.wikipedia.org/wiki/Type_abstrait).

Un tableau est une structure qui peut contenir des éléments.

+ Le nombre d'éléments du tableau est appelé  sa longueur, cette longueur est   fixe.
+ Les éléments du tableau sont tous du même type (par exemple sont tous des entiers, ou tous des caractères...).
+ Chaque élément est repéré par un indice. On supposera ici que les indices sont compris entre 0 et longueur(tableau)-1.




tab étant un tableau et i un indice de ce tableau:

+ on obtient la valeur de l'élément d'indice i par la notation tab[i].
+ on change la valeur de l'élément d'indice i par affectation: tab[i] ← valeur.


!!! note
    Pour chercher des compléments sur le web sur les types de données abstraits:  
    le sigle TDA est souvent utilisé ([ADT](https://en.wikipedia.org/wiki/Abstract_data_type) en anglais pour abstract data type).


    Qu'est ce qu'un type de données abstrait:
    
    + [cnam](http://deptinfo.cnam.fr/Enseignement/CycleA/SD/cours/tr03-types-abstraits.pdf)
    + [lri](https://www.lri.fr/~hivert/COURS/CFA-L3/05-TDA.pdf)
    + [wikipedia](https://fr.wikipedia.org/wiki/Type_abstrait)
    
    Des sources sur le type tableau:
    
    + [wikipedia](https://fr.wikipedia.org/wiki/Tableau_(structure_de_donn%C3%A9es))
    + [brilliant](https://brilliant.org/wiki/arrays-adt/)

