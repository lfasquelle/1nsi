# Filtrer les éléments d'une liste




##  Un exemple


 


On dispose d'une liste d'entiers `X = [2, 7, 8, 23, 42, 64, 31]`.   
On aimerait définir une liste `F` qui ne contienne que les éléments pairs de la liste `X`.  
On peut procéder ainsi: 


```python
X = [2, 7, 8, 23, 42, 64, 31]
F = []
for n in X:
    if n%2 == 0:
        F.append(n)
```

qu'on peut lire ainsi:

```
X ← [2, 7, 8, 23, 42, 64, 31]
F ← liste vide
Pour chaque élément n de la liste X:
    si n est pair:
        ajouter n en fin de liste F
```

On peut utiliser une définition par compréhension. 
Python permet en effet de préciser une définition par compréhension à l'aide d'une condition:

```python
X = [2, 7, 8, 23, 42, 64, 31]
F = [n for n in X if n%2 == 0]
```

qu'on peut lire ainsi:

```
X ← [2, 7, 8, 23, 42, 64, 31]
F ←  la liste des éléments n pour n parcourant les valeurs paires de X
``` 

 
    
  
  

## Liste de multiples   


Proposer un code pour la fonction suivante:

```python
def sousListeMultiplesDe5(tab):
    """
    tab -- liste d'entiers
    
    renvoie une liste ne contenant que les éléments de tab 
    qui sont multiples de 5.
    """
```

Par exemple:

```
>>> sousListeMultiplesDe5([2, 5, 7, 10, 19, 20, 21])
[5, 10, 20]
```


??? solution "Un code"

    ```python
    def sousListeMultiplesDe5(tab):
        """
        tab -- liste d'entiers
        
        renvoie une liste ne contenant que les éléments de tab 
        qui sont multiples de 5.
        """
        return [elt for elt in tab if elt%5 == 0]
    ```


## Pivot



Proposer un code pour la fonction suivante:

```python
def avantPivot(tab, valeur):
    """
    tab -- liste d'entiers
    valeur -- entier
    
    renvoie une liste contenant les éléments de tab qui sont < valeur.
    >>> avantPivot([2, 6, 3, 9, 4, 42], 7)
    [2, 6, 3, 4]
    """
```


??? solution "Un code possible"




    ```python
    def avantPivot(tab, valeur):
        """
        tab -- liste d'entiers
        valeur -- entier
        
        renvoie une liste contenant les éléments de tab qui sont < valeur.
        >>> avantPivot([2, 6, 3, 9, 4, 42], 7)
        [2, 6, 3, 4]
        """
        return [elt for elt in tab if elt < valeur]
    ```

 
## Liste des voyelles d'un mot

Ecrire un corps possible pour la fonction suivante:

```python
def listeVoyelles(mot):
    """
    mot -- chaîne de caractères
    renvoie une liste des voyelles de la chaîne mot 
    (dans le même ordre que dans mot)
    """
```

Exemple:

```
>>> listeVoyelles('informatique')
['i', 'o', 'a', 'i', 'u', 'e']
```

??? solution 

    ```python
    def listeVoyelles(mot):
        """
        mot -- chaîne de caractères
        renvoie une liste des voyelles de la chaîne mot 
        (dans le même ordre que dans mot)
        """
        return [caractere for caractere in mot if caractere in 'aeiouy']
    ```


## Liste des Arsène

On dispose d'une liste de personnes sous la forme de couples (nom, prénom). Par exemple:

```
L = [ ('Dupont', 'Paul'), ('Dupond', 'Arsène'), ('Martin', 'Antoine'), ('Malibu', 'Arsène') ]
```

On aimerait construire la liste des noms des personnes prénommées Arsène. Dans l'exemple:

```
[ 'Dupond', 'Malibu']
```

Définir cette seconde liste par compréhension à partir de la liste L.

??? solution

    ```python
    P = [nom for (nom, prenom) in L if prenom == 'Arsène']
    ```

## Liste des personnes de plus de 45 ans.

On dispose d'une liste de personnes sous la forme (nom, prénom, âge). Par exemple:

```
L = [ ('Babou', 'Antoinette', 34),
      ('Zazu', 'Pascal', 50),
      ('Nabucho', 'Dorine', 25),
      ('Babe', 'Flore', 52)
      ]
```

Définir par compréhension une liste des prénoms des personnes ayant plus de 45 ans.

??? solution 
    
    ```python
    V = [ prenom for (_, prenom, age) in L if age > 45]
    ```
    On a utilisé le nom de variable `_` pour les noms de famille parce que cette variable ne joue aucun rôle ici.   
    Vous pouvez utiliser un autre nom pour cette variable si vous préférez. L'utilisation de ce nom de variable `_` permet
    en général une lecture plus facile en évitant au lecteur de "charger" sa mémoire avec un nom de variable inutile.
    
    
