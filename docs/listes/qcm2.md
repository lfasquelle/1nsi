# QCM

!!! note  
    Les QCM sont là pour vous aider à contrôler ce que vous avez retenu.
    Si vous ne répondez pas à toutes les questions sans hésitation, c'est sans doute
    qu'il faut refaire des lectures des pages précédentes.

Trouvez la ou les bonnes réponses.

## QCM  


Avec la fonction suivante:

```python
def f(tab, valeur):
    """
    tab -- liste d'entiers
    valeur -- entier

    """
    present = False
    for i, v in enumerate(tab):
        if v == valeur: 
            present = True
            indice = i
    if present: 
        return indice
```


`f([3, 5, 3, 7, 8], 3)` a pour valeur:

- [ ] None
- [ ] 0
- [ ] 2
- [ ] 3

??? solution "Réponse"

    - [ ] None
    - [ ] 0
    - [X] 2
    - [ ] 3

    Voir [cet exercice](presence.md#exercice_3).
    
    
    
## QCM  




Avec la fonction suivante:

```python
def f(tab, valeur):
    """
    tab -- liste d'entiers
    valeur -- entier

    """
    present = False
    for i, v in enumerate(tab):
        if v == valeur: 
            present = True
            indice = i
    if present: 
        return indice
```


`f([3, 5, 3, 7, 8], 4)` a pour valeur:

- [ ] None
- [ ] 0
- [ ] 2
- [ ] 3

??? solution "Réponse"

    - [X] None
    - [ ] 0
    - [ ] 2
    - [ ] 3
    
    Il n'y a pas de return explicite dans ce cas dans le code: la valeur renvoyée par 
    la fonction est donc `None`.
    
    
    
## QCM  


Le code suivant:


```python
def f(liste):
    """
    liste -- liste d'entiers

    """
    a = liste[0]  
    for element in liste:
        if element > a: a = element
    return a
```


renvoie:

- [ ] le plus grand élément de liste.
- [ ] le plus petit élément de liste.
- [ ] l'élément de plus grand indice dans liste.
- [ ] l'élément de plus petit indice dans liste.


??? solution "Réponse"

    - [X] le plus grand élément de liste.
    - [ ] le plus petit élément de liste.
    - [ ] l'élément de plus grand indice dans liste.
    - [ ] l'élément de plus petit indice dans liste.
    
    
## QCM 


Avec la fonction:

```python
def f(liste):
    """
    liste -- liste d'entiers, non vide

    """
    a = liste[0]  
    indice = 0 #  
    for i, element in enumerate(liste):
        if element > a: 
            a = element
            indice  = i 
    return indice 
```

```
f([3, 5, 3, 7, 8, 3, 8])
```

a pour valeur:


- [ ] 3
- [ ] 8
- [ ] 4
- [ ] 6

??? solution "Réponse"

    - [ ] 3
    - [ ] 8
    - [X] 4
    - [ ] 6

    La fonction renvoie l'indice de la première occurrence du maximum.
