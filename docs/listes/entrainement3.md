# Le jeu de cartes 




!!! important
    On rappelle que l'entraînement à la programmation est indispensable pour apprendre à programmer...




   
## Exercice 1

On veut représenter un jeu de cartes par les tuples (couleur, hauteur): 

```
coeur = [ ('coeur', 1), ('coeur', 2), ..., ('coeur', 10), ('coeur', 11), ('coeur', 12), ('coeur', 13)]
```
 
(on a choisi 11, 12, 13 au lieu de valet, dame, roi).

+ &Eacute;crire une fonction python spécifiée comme suit:

```python
def couleur(couleur):
    """
    couleur -- chaîne  qui vaut 'coeur' ou 'carreau' ou 'trèfle' ou 'pique'
    
    renvoie la liste des cartes de cette couleur.
    """
```


??? solution "Une réponse"

    [Ce fichier ipynb](fichiers/couleur.ipynb) propose un code possible.
    
    [Version html du fichier](fichiers/couleur.html):
    
    <iframe src="fichiers/couleur.html" width="100%" height="400px"></iframe> 
    
    
+ &Eacute;crire une fonction python générant maintenant un jeu de cartes complet (les 4 &times; 13 cartes)
sous forme d'une liste.


??? solution "Une réponse"

    [Ce fichier ipynb](fichiers/jeucartes.ipynb) propose un code possible.
    
    [Version html du fichier](fichiers/jeucartes.html):
    
    <iframe src="fichiers/jeucartes.html" width="100%" height="400px"></iframe> 
    
    
    
## Exercice 2

On dispose d'un jeu de cartes comme défini par la fonction de l'exercice précédent.

Les joueurs commencent avec un jeu personnel représenté par une liste vide.

```python
jeu = jeu = jeu_de_cartes() # construction du jeu par la fonction de l'exercice précédent
antoine = [] # jeu initial du joueur Antoine
basile = [] # jeu initial du joueur Basile
```

+ &Eacute;crire une fonction python spécifiée comme suit:

```python
from random import randint
def distribue_une_carte(joueur):
    """
    joueur -- liste des cartes d'un joueur
    
    tire une carte au hasard dans le jeu de cartes, la supprime du jeu de cartes,
    l'ajoute au jeu du joueur.
    """
```


!!! important

    On utilisera la méthode [pop du type list](https://docs.python.org/fr/2/tutorial/datastructures.html).
    
    ```
    >>> tab = [ 'a', 'b', 'c', 'd', 'e']
    >>> lettre = tab.pop(2)
    >>> lettre
    'c'
    >>> tab
    ['a', 'b', 'd', 'e']
    ```
    
    On voit sur cet exemple que, tab désignant une liste python et i un indice de cette liste, l'instruction:
    
    ```python
    valeur = tab.pop(i)
    ```
    
    + récupère la valeur de tab[i] et la "stocke" dans la variable valeur.
    + efface la valeur tab[i] de la liste (tous les éléments de tab qui ont un indice supérieur à i 
    voient alors leur indice décrémenter d'une unité).
    
    
??? solution "Un code possible"

    
    [Ce fichier ipynb](fichiers/distribue1.ipynb) propose un code possible.
    
    [Version html du fichier](fichiers/distribue1.html):
    
    <iframe src="fichiers/distribue1.html" width="100%" height="400px"></iframe> 



## Exercice 3

On poursuit l'exercice précédent.

En utilisant la fonction de distribution d'une carte à un joueur définie précédemment, écrire
un corps possible pour la fonction suivante:

```python
def distribue_jeu(n, joueur1, joueur2):
    """
    n -- nombre de cartes à distribuer à chaque joueur.
    joueur1 -- liste des cartes du joueur 1
    joueur2 -- liste des cartes du joueur 2
    
    distribue n cartes à chaque joueur.
    """
```

La distribution se fera en alternant les joueurs: une carte au joueur 1, une carte au joueur 2,
une carte au joueur 1, une carte au joueur 2, ...

??? solution "Un code possible"

    
    [Ce fichier ipynb](fichiers/distribue2.ipynb) propose un code possible.
    
    [Version html du fichier](fichiers/distribue2.html):
    
    <iframe src="fichiers/distribue2.html" width="100%" height="400px"></iframe> 



## Exercice 4

Le jeu peut maintenant être joué avec plus de deux joueurs.

On doit donc modifier la fonction précédente de distribution pour le cas de k joueurs.

Compléter la fonction suivante:

```python
def distribue_jeu(n, liste_joueurs):
    """
    n -- nombre de cartes à distribuer à chaque joueur.
    liste_joueurs -- liste des listes des cartes des joueurs
     
    
    distribue n cartes à chaque joueur.
    """
```


??? solution "Un code possible"

    
    [Ce fichier ipynb](fichiers/distribue3.ipynb) propose un code possible.
    
    [Version html du fichier](fichiers/distribue3.html):
    
    <iframe src="fichiers/distribue3.html" width="100%" height="400px"></iframe> 
    
    
    
## Exercice 5

&Eacute;crire un code pour la fonction suivante:

```python
def filtre(jeu, hauteur):
    """
    hauteur: entier entre 1 et 13 
    jeu: une liste de cartes 
    
    renvoie la liste des cartes de hauteur "hauteur" présentes dans jeu. 
    """
```

Exemple ci-dessous (en utilisant la fonction de création du jeu déjà définie dans les exercices précédents): comment
obtenir tous les as?

```
>>> jeu_complet = jeu_de_cartes()
>>> filtre(jeu_complet, 1)
[('coeur', 1), ('carreau', 1), ('trèfle', 1), ('pique', 1)]
``` 



??? solution "Un code possible"

    
    [Ce fichier ipynb](fichiers/filtrejeu.ipynb) propose un code possible.
    
    [Version html du fichier](fichiers/filtrejeu.html):
    
    <iframe src="fichiers/filtrejeu.html" width="100%" height="400px"></iframe> 
