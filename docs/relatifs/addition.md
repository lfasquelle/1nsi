# Addition

Une des raisons du choix de la représentation en complément à deux s'explique 
par l'algorithme d'addition de deux entiers avec leur écriture en complément à deux.



## Somme de 32 et 12.

+ Donner le code de  32 et le code de  12 en complément à deux sur 8 bits.
+ Additionner 32 et 12 à l'aide de ces codes. Comment peut-on procèder?


??? solution "Les codes"

    32 et 12 sont des positifs, ils sont codés par leur écriture binaire usuelle.
    
    32 est codé par 0010 0000.
    
    12 est codé par 0000 1100.
    
    
??? solution "somme"

    Comme 32 et 12 sont codés par leur écriture binaire usuelle, on effectue la somme comme
    appris à l'école élémentaire (mais en base deux):
    
    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-cly1{text-align:left;vertical-align:middle}
    </style>
    <table class="tg">
      <tr>
        <th class="tg-cly1"></th>
        <th class="tg-cly1">0</th>
        <th class="tg-cly1">0</th>
        <th class="tg-cly1">1</th>
        <th class="tg-cly1">0</th>
        <th class="tg-cly1">0</th>
        <th class="tg-cly1">0</th>
        <th class="tg-cly1">0</th>
        <th class="tg-cly1">0</th>
      </tr>
      <tr>
        <td class="tg-cly1">+</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
      </tr>
      <tr style="border-top:3px solid black;">
        <td class="tg-cly1"></td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
      </tr>
    </table>
    
    Et 0010 1100<sub>deux</sub> = 44<sub>dix</sub>
    
    
    

## Somme de 44 et 11.

+ Donner le code de  44 et le code de 11 en complément à deux sur 8 bits.
+ Additionner 44 et 11 à l'aide de ces codes. Comment peut-on procèder?


??? solution "Les codes"

    44 et 11 sont des positifs, ils sont codés par leur écriture binaire usuelle.
    
    44 est codé par 0010 1100.
    
    11 est codé par 0000 1011.
    
    
??? solution "somme"

    Comme 44 et 11 sont codés par leur écriture binaire usuelle, on effectue la somme comme
    appris à l'école élémentaire (mais en base deux):
    
    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-cly1{text-align:left;vertical-align:middle}
    .tg .tg-0lax{text-align:left;vertical-align:top}
    </style>
    <table class="tg">
      <tr style="color:red;font-size:0.5rem;">
        <th class="tg-0lax">retenues</th>
        <th class="tg-0lax"></th>
        <th class="tg-0lax"></th>
        <th class="tg-0lax"></th>
        <th class="tg-0lax">1</th>
        <th class="tg-0lax"></th>
        <th class="tg-0lax"></th>
        <th class="tg-0lax"></th>
        <th class="tg-0lax"></th>
      </tr>
      <tr>
        <td class="tg-cly1"></td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
      </tr>
      <tr>
        <td class="tg-cly1">+</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">1<br></td>
        <td class="tg-cly1">1<br></td>
      </tr>
      <tr style="border-top:4px solid black;">
        <td class="tg-cly1"></td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0<br></td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1<br></td>
        <td class="tg-cly1">1<br></td>
      </tr>
    </table>
    
    Et 0011 0111<sub>deux</sub> =  55<sub>dix</sub>.

## Somme de 32 et -12.


+ Donner le code de  32 et le code de -12 en complément à deux sur 8 bits.
+ Additionner 32 et -12 à l'aide de ces codes. Comment peut-on procèder?


??? solution "Les codes"

    32 est codé par 0010 0000 (voir plus haut).
    
    -12 est négatif. Son code est donné par l'écriture binaire de  -12 + 2<sup>8</sup> = 244.
    
    244<sub>dix</sub> = 1111 0100<sub>deux</sub>.
    
    Le code en complément à 2 sur 8 bits de -12 est 1111 0100.
    
     
??? solution "Somme"

    Les codes n'étant pas les codes binaires usuels, il n'y a aucune raison qu'une addition binaire
    usuelle donne le bon résultat.
    
    En fait, il suffit effectivement d'additionner au sens usuel en binaire les deux codes 
    pour obtenir le résultat. C'est là l'une des raisons du choix de la représentation des entiers
    relatifs en complément à deux: on préserve un algorithme d'addition très simple.
    
    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-cly1{text-align:left;vertical-align:middle}
    .tg .tg-0lax{text-align:left;vertical-align:top}
    </style>
    <table class="tg">
      <tr style="color:red; font-size:0.5rem;">
        <th class="tg-0lax">retenues</th>
        <th class="tg-0lax">1</th>
        <th class="tg-0lax">1</th>
        <th class="tg-0lax">1</th>
        <th class="tg-0lax"></th>
        <th class="tg-0lax"></th>
        <th class="tg-0lax"></th>
        <th class="tg-0lax"></th>
        <th class="tg-0lax"></th>
        <th class="tg-0lax"></th>
      </tr>
      <tr>
        <td class="tg-0lax"></td>
        <td class="tg-cly1"></td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0 </td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
      </tr>
      <tr>
        <td class="tg-0lax">+</td>
        <td class="tg-cly1"></td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
      </tr>
      <tr style="border-top:4px solid black;">
        <td class="tg-0lax"></td>
        <td class="tg-cly1" style="color:green">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0<br></td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
      </tr>
    </table>
    
    Comme le code est sur 8 bits, le 1 à gauche généré par une retenue disparaît et le code 
    retenu est 0001 0100.
    
    Or 0001 0100 est le code en complément à deux de l'entier 2<sup>4</sup>+ 2<sup>2</sup> = 20, c'est dire 32-12.



## Somme  de  -96 et -12.


Vérifier de même que la somme de -96 et -12 peut se faire par une simple addition binaire
sur les codes de ces deux entiers en complément à deux.

??? solution "Réponse"

    On a vu que -12 st codé en complément à deux (sur 8 bits) par 1111 0100.
    
    -96 + 2<sup>8</sup> =  160 = 10100000<sub>deux</sub>.
    Le code en complément à deux de -96 sur 8 bits est 1010 0000.
    
    L'addition:
    
    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-cly1{text-align:left;vertical-align:middle}
    .tg .tg-0lax{text-align:left;vertical-align:top}
    </style>
    <table class="tg">
      <tr style="color:red; font-size:0.5rem;">
        <th class="tg-0lax">retenues</th>
        <th class="tg-0lax">1</th>
        <th class="tg-0lax">1</th>
        <th class="tg-0lax">1</th>
        <th class="tg-0lax"></th>
        <th class="tg-0lax"></th>
        <th class="tg-0lax"></th>
        <th class="tg-0lax"></th>
        <th class="tg-0lax"></th>
        <th class="tg-0lax"></th>
      </tr>
      <tr>
        <td class="tg-0lax"></td>
        <td class="tg-cly1"></td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0 </td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
      </tr>
      <tr>
        <td class="tg-0lax">+</td>
        <td class="tg-cly1"></td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
      </tr>
      <tr style="border-top:4px solid black;">
        <td class="tg-0lax"></td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0<br></td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
      </tr>
    </table>
    
    Le résultat est donc 1001 0100.
    
    1001 0100<sub>deux</sub> =  148<sub>dix</sub>.
    Et 148 - 2<sup>8</sup> = -108.
    
    Et on a bien -96 -12 = -108.
    
    
    
## max + 1 = min.
    
Vous êtes maintenant en mesure d'expliquer pourquoi le code suivant en langage C affichait -32768:

```c
int main() {
    signed short x = 32767; 
    signed short y = 1;
    signed short z = x + y;
    printf("Somme x + y = %hd", z);
}
```

Expliquez!

On rappelle que les signed short du langage C sont des entiers codés en complément à deux sur 16 bits.

??? solution "Réponse"

    32767 = 2<sup>15</sup> -1 = 111 1111 1111 1111<sub>deux</sub>.
    
    Le code en complément à 2 sur 16 bits de 32767 est donc 0111 1111 1111 1111.
    
    Le code de 1 est  0000 0000 0000 0001.
    
    Effectuons la somme:
    
    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-cly1{text-align:left;vertical-align:middle}
    .tg .tg-0lax{text-align:left;vertical-align:top}
    </style>
    <table class="tg">
      <tr style="color:red; font-size:0.5rem;">
        <th class="tg-cly1">retenues</th>
        <th class="tg-cly1">1</th>
        <th class="tg-cly1">1</th>
        <th class="tg-cly1">1</th>
        <th class="tg-cly1">1</th>
        <th class="tg-cly1">1</th>
        <th class="tg-cly1">1</th>
        <th class="tg-cly1">1</th>
        <th class="tg-cly1">1</th>
        <th class="tg-cly1">1</th>
        <th class="tg-0lax">1</th>
        <th class="tg-0lax">1</th>
        <th class="tg-0lax">1</th>
        <th class="tg-0lax">1</th>
        <th class="tg-0lax">1</th>
        <th class="tg-0lax">1</th>
        <th class="tg-0lax"></th>
      </tr>
      <tr>
        <td class="tg-cly1"></td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">1</td>
        <td class="tg-0lax">1</td>
        <td class="tg-0lax">1</td>
        <td class="tg-0lax">1</td>
        <td class="tg-0lax">1</td>
        <td class="tg-0lax">1</td>
        <td class="tg-0lax">1</td>
        <td class="tg-0lax">1</td>
      </tr>
      <tr>
        <td class="tg-cly1">+</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-0lax">0</td>
        <td class="tg-0lax">0</td>
        <td class="tg-0lax">0</td>
        <td class="tg-0lax">0</td>
        <td class="tg-0lax">0</td>
        <td class="tg-0lax">0</td>
        <td class="tg-0lax">1</td>
      </tr>
      <tr style="border-top:4px solid black;">
        <td class="tg-cly1"></td>
        <td class="tg-cly1">1</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-cly1">0</td>
        <td class="tg-0lax">0</td>
        <td class="tg-0lax">0</td>
        <td class="tg-0lax">0</td>
        <td class="tg-0lax">0</td>
        <td class="tg-0lax">0</td>
        <td class="tg-0lax">0</td>
        <td class="tg-0lax">0</td>
      </tr>
    </table>
    
    On obtient 1000 0000 0000 0000.
    
    1000 0000 0000 0000<sub>deux</sub> = 2<sup>15</sup> = 32768.
    Et 32768 - 2<sup>16</sup> = -32768.
    
    Le code   1000 0000 0000 0000 est donc le code de l'entier -32768.
    
    
## Somme de 32767 et 2.

Qu'obtiendra-t-on si on somme les deux entiers 32767 et 2 de type signed short dans un programme C?

??? solution "A la main"

    L'addition:
    
    
    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
    </style>
    <table class="tg">
      <tr style="color:red; font-size:0.5rem;">
        <th class="tg-0pky">retenues</th>
        <th class="tg-0pky">1</th>
        <th class="tg-0pky">1</th>
        <th class="tg-0pky">1</th>
        <th class="tg-0pky">1</th>
        <th class="tg-0pky">1</th>
        <th class="tg-0pky">1</th>
        <th class="tg-0pky">1</th>
        <th class="tg-0pky">1</th>
        <th class="tg-0pky">1</th>
        <th class="tg-0pky">1</th>
        <th class="tg-0pky">1</th>
        <th class="tg-0pky">1</th>
        <th class="tg-0pky">1</th>
        <th class="tg-0pky">1</th>
        <th class="tg-0pky"></th>
        <th class="tg-0pky"></th>
      </tr>
      <tr>
        <td class="tg-0pky"></td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">1</td>
        <td class="tg-0pky">1</td>
        <td class="tg-0pky">1</td>
        <td class="tg-0pky">1</td>
        <td class="tg-0pky">1</td>
        <td class="tg-0pky">1</td>
        <td class="tg-0pky">1</td>
        <td class="tg-0pky">1</td>
        <td class="tg-0pky">1</td>
        <td class="tg-0pky">1</td>
        <td class="tg-0pky">1</td>
        <td class="tg-0pky">1</td>
        <td class="tg-0pky">1</td>
        <td class="tg-0pky">1</td>
        <td class="tg-0pky">1</td>
      </tr>
      <tr>
        <td class="tg-0pky">+</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">1</td>
        <td class="tg-0pky">0</td>
      </tr>
      <tr style="border-top:4px solid black;">
        <td class="tg-0pky"></td>
        <td class="tg-0pky">1</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">0</td>
        <td class="tg-0pky">1</td>
      </tr>
    </table>
    
    
    1000 0000 0000 0001<sub>deux</sub> =  2<sup>15</sup> + 1 = 32769.
    Et 32769 - 2<sup>16</sup> =  -32767.
    
    On devrait donc obtenir -32767.
    
??? solution "Avec du code C"

    Le code:
    
    ```c
    #include<stdio.h>

    int main() {

        signed short x = 32767; 
        signed short y = 2;
        signed short z = x + y;
        printf("Somme x + y = %hd", z);
    }
    ```
    
    affiche:
    
    ```
    Somme x + y = -32767
    ```

