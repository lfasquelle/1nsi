# Codage en complément à deux

 
 


On explique le codage utilisé sur un type "entier signé sur un octet" (pour faciliter la manipulation: les
entiers en jeu sont plus petits qu'avec 16 bits).


## Combien?

Combien d'entiers peut-on coder sur 8 bits?


??? solution "Combien"

    On dispose de 2<sup>8</sup> = 256 codes différents.
    
    
## Lesquels?

+ Si sur ces 8 bits, on code uniquement les entiers positifs à partir de 0 (par leur écriture en base 2), quel
est le plus grand entier codé?
+ Si on décide d'utiliser ces codes sur 8 bits pour coder 
les entiers d'un intervalle [ e<sub>min</sub>; e<sub>max</sub>]
comportant **autant** d'entiers strictement négatifs que d'entiers positifs ou nuls, que valent
e<sub>min</sub> et e<sub>max</sub>?

??? solution "Entiers positifs"
    On code les entiers de 0000 0000<sub>deux</sub> = 0 à 1111 1111<sub>deux</sub> = 255.
    
??? solution "Entiers relatifs"

    Le plus petit sera -2<sup>7</sup> = -128.  
    Le plus grand sera 2<sup>7</sup> - 1 = 127.
    
    
    
## Comment sont-ils codés en machine?

Pour coder les entiers relatifs de -128 à 127, le codage utilisé est en général le suivant:

Les codes ayant un 0 à gauche désignent les positifs:
   
+ 0000 0000 représente 0, 
+ 0000 0001 représente 1, 
+ 0000 00010 représente 2, 
+ 0000 0011 représente 3, 
+ 0000 0100 représente 4, 
+ ..., 
+ 0111 1111 représente 127

Il s'agit donc de l'écriture en base deux de ces entiers.


Les codes ayant un 1 à gauche désignent les négatifs. Par contre, les 7 bits qui suivent ne sont plus à lire comme
une écriture en base deux. Par exemple, pour -127, on n'utilise pas 1111 1111 mais 1000 0001.  
L'idée est la suivante:  
après le code 0111 1111 qui désigne +127 (le dernier positif), on repart à -128:

+ 1000 0000 représente -128,
+ 1000 0001 représente -127,
+ 1000 0010 représente -126,
+ ...
+ 1111 1111 représente -1.

Ce codage est appelé "complément à deux": pour savoir comment coder un négatif sur 8 bits, 
on ajoute 2<sup>8</sup> à ce négatif,
l'écriture en base deux de l'entier positif obtenu est alors le code de notre entier négatif.


Pour avoir une image de ce choix, vous pouvez imaginer que l'on tourne sur un cercle: dans un sens, on compte
en positif les pas, dans l'autre sens on les compte en négatif. En complément à 2 sur 8 bits, on découpe
en 2<sup>8</sup> = 256 arcs de même longueur.

![](images/complement8.png)

On voit sur cette image que -1 va correspondre à 255 et sera donc codé par l'écriture binaire de 255.

Si on pense "en tour", on voit que pour passer de -1 à 255, il faut un tour complet, il faut donc ajouter 256 = 2<sup>8</sup>
à -1 pour obtenir l'entier donnant son code.
De même, si l'on fait un tour complet dans le sens positif à partir de -6, on tombe sur 250. C'est donc l'écriture
binaire de 250 qui donne le code complément à 2 sur 8 bits de -6.




    

## Exercice 1

Donner le code de l'entier -100<sub>dix</sub> avec la représentation "complément à deux sur 8 bits".

??? solution "Réponse"

    -100 + 2<sup>8</sup> = 156.
    
    156 = 2<sup>7</sup> + 2<sup>4</sup> + 2<sup>3</sup> + 2<sup>2</sup>, 
    d'où 156 = 1001 1100<sub>deux</sub>.
    
    En complément à deux sur 8 bits, -100 est codé 1001 1100.
    

!!! important

    Ainsi un même code peut représenter 156 et -100... Il faut donc évidemment savoir
    de quel code il s'agit.
    
    Par exemple en langage C, la valeur d'une variable de type signed short (entier signé sur 2 octets)
    sera codée en complément à deux sur 16 bits,
    tandis que la valeur d'une variable de type unsigned short (entier positif sur 2 octets) sera codé
    par son écriture usuelle en base deux.  
    C'est le type de la variable (donné par le programmeur en langage C) qui permet de savoir si le code
    doit être lu comme un code "complément deux" ou comme un code "écriture binaire usuelle".
    
    
## Exercice 2

Donner le code en complément à deux sur un octet des entiers:

+ a = -126
+ b = +126
+ c = -1
+ d = 1

??? solution "126 et 1"
    Le code des positifs est facile: il s'agit de l'écriture binaire usuelle.
    
    b = 126 est donc codé par 0111 1110 en complément à deux sur 8 bits.
    c = 1 est codé par 0000 0001.
    
??? solution "-126 et -1"

    + Pour obtenir le code de -1, on ajoute 2<sup>8</sup> =  256 à cette valeur. 
    On obtient 255.  
    On calcule alors l' écriture binaire usuelle de 255: 255 = 1111 1111.      
    Le code en complément à deux sur un octet de l'entier -1 est donc 1111 1111.
    
    + Pour obtenir le code de -126, on ajoute 2<sup>8</sup> =  256 à cette valeur. 
    On obtient 130.  
    On calcule l'écriture binaire usuelle de 130:  
    130 = 2<sup>7</sup> + 2<sup>1</sup> = 1000 0010<sub>deux</sub>.  
    Le code en complément à deux sur un octet de l'entier -126 est donc 1000 0010.


## Exercice 3

+ Le code suivant est le code binaire usuel d'un entier naturel: 1011 0101. Lequel?
+ Le code suivant est le code en complément à 2 sur 8 bits d'un entier: 1011 0101. Lequel?


??? solution "code binaire"

    L'entier positif codé en binaire par 1011 0101 est l'entier 1011 0101<sub>deux</sub>!
    Son code correspond à son écriture usuelle en base deux, il n'y a rien à calculer.
    
    Toutefois, on est plus habitué à la base dix. En base dix, cet entier est $2^7+2^5+2^4+ 2^2+2^0 = 181$.
    
??? solution "code complément à 2"

    L'entier ainsi codé en complément à 2 est un négatif (puisque le bit de gauche est 1).
    Cet entier est 181 - $2^8$ = -75.

    
    
## Code en complément à 2 sur 4 bits.

Imaginer de même comment représenter sur un cercle un codage en complément à 2 sur 4 bits.
Et donner le code de -1 puis celui de -6.

??? solution  "Une réponse"

    Dans ce cas, on ne partage plus le cercle en 2<sup>8</sup> = 256 arcs égaux comme avec 8 bits, mais 
    en 2<sup>4</sup> = 16 arcs égaux.  
    Le cercle avec un code en complément à deux sur 4 bits:
    
    ![](images/complement4.png)
    
    On voit que le code en complément à 2 sur 4 bits  de -1 est donné par l'écriture binaire de 15 soit 1111.
    Le code de -6 serait donné par l'écriture binaire de 10 (-6 + un tour = -6 + 16 =  10), soit 1010.
    
    
!!! note "Remarque"

    Dans la pratique, on n'utilise pas un tel code, car on code sur un nombre entier d'octets.
    On trouve en fait dans la pratique des codes sur 8 bits, 16 bits, 32 bits, 64 bits.

## Code sur deux octets.

De la même façon, le code en complément à deux sur 2 octets (soit 16 bits) s'obtient comme suit:

+ Un entier positif n compris entre 0 et 2<sup>15</sup>-1 = 32767 est codé
sur 16 bits par son écriture binaire usuelle (le code commence  donc à gauche par un 0).
+ Un entier négatif n compris entre -2<sup>15</sup> et -1 est codé par l'écriture binaire sur 16 bits de l'entier
n + 2<sup>16</sup> (soit n + un tour sur le cercle, le cercle étant cette fois découpé en  2<sup>16</sup>  arcs égaux).

!!! note
    C'est par exemple ainsi que sont codés les signed short en langage C.
    
    
## Exercice 5

Donner le code en complément à deux sur 16 bits des entiers suivants:

+ a = 2
+ b = -2
+ c = 32767
+ d = 32768
+ e = -32767
+ f =  -32768

??? solution "a"

    2<sub>dix</sub> = 10<sub>deux</sub> d'où le code: 0000 0000 0000 0010.

??? solution "b"

    -2 + 2<sup>16</sup> = 65534.
    65534<sub>dix</sub> = 1111 1111 1111 1110<sub>deux</sub>.
    
    Le code de -2 en complément à deux sur 16 bits est donc 1111 1111 1111 1110.

??? solution "c"

    32767<sub>dix</sub> = 111 1111 1111 1111<sub>deux</sub>
    d'où le code en complément à deux sur 16 bits: 0111 1111 1111 1111.

??? solution "d"
    C'est impossible! Les entiers codés en complément à deux sur 16 bits sont les entiers
    entre -32768 et +32767.
    
??? solution "e"

    -32767 + 2<sup>16</sup>  = 32 769.
    
    32 769<sub>dix</sub> = 1000 0000 0000 0001<sub>deux</sub>.
    Le code en complément à deux sur 16 bits de -32767 est donc 1000 0000 0000 0001.

??? solution "f"

    -32768 + 2<sup>16</sup> = 32 768.
    
    32 768<sub>dix</sub> = 1000 0000 0000 0000<sub>deux</sub>.
    Le code en complément à deux sur 16 bits de -32768 est donc 1000 0000 0000 0000.


