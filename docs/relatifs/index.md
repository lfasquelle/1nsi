#  Les entiers relatifs


 
Nous avons vu que le codage binaire des entiers positifs est assez naturel: leur écriture en base 2 est un 
code qui est déjà sous la forme de 0 et 1.

Et pour les entiers négatifs? Comment coder le signe?


Une idée simple serait de convenir que 0 désigne le signe + et 1 le signe - (ou 0 le signe - et 1 le signe +).
Ainsi le premier bit à gauche désignerait le signe, les autres bits seraient le codage de la valeur absolue de l'entier
dans l'écriture binaire usuelle.  
Ce n'est pas cette idée qui est utilisée en pratique pour diverses raisons (comportements vis à vis
des opérations usuelles notamment).
