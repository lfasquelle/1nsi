# Programmons



Faisons le point:

le principe du code complément à deux sur n bits permet de représenter 
les entiers m tels que $-2^{n-1} \leqslant m \leqslant 2^{n-1}-1$:

+ les entiers m tels que $0 \leqslant m \leqslant 2^{n-1}-1$ sont représentés par leur écriture binaire usuelle
(avec au moins un 0 à gauche)
+ les entiers m tels que $-2^{n-1} \leqslant m \leqslant -1$ sont représentés par l' écriture binaire usuelle
de l'entier $m + 2^n$ (le bit de gauche est toujours 1).



## Exercice 1

&Eacute;crire un code possible pour le corps de la fonction python suivante:

```python
def codeComplementA2(entier, longueur):
    """
    longueur -- entier naturel au moins égal à 2
    entier -- entier relatif représentable en complément à 2 sur longueur bits.
    
    renvoie la chaîne de caractère correspondant au code complément à 2 sur longueur bits
    de l'entier.
    >>> codeComplementA2(-6, 8)
    1111 1010
    >>> codeComplementA2(-32767, 16)
    1000 0000 0000 0001 
    >>> codeComplementA2(32767, 16)
    0111 1111 1111 1111
    >>> codeComplementA2(32768, 16)
    AssertionError: Entier non représentable sur 16 bits.
    """
```


??? solution "Un code possible"

    Un code possible.  
    On a introduit d'autres fonctions pour décomposer le code.
    Rappelons que tous vos programmes devront être ainsi décomposés en petites unités fonctionnelles.
    
    ```python
        
    def codeBinaire(entier):
        """
        entier -- entier naturel 
        
        renvoie la chaîne de caractères correspondant 
        à l'écriture binaire de entier.
        """
        if entier == 0: return '0'
        chaine = ''
        while entier != 0:
            chaine = str(entier % 2) + chaine
            entier = entier//2
        return chaine



    def completeCode(entier, longueur):
        """
        longueur -- entier > 1.
        entier -- entier naturel, de code binaire 
        ayant au plus longueur chiffre.
        
        renvoie la chaîne du code binaire (de type str) de entier 
        complétée à gauche par des 0 pour que la longueur 
        de ce code soit exactement de longueur longueur.
        """
        code = codeBinaire(entier)
        lg = len(code)
        while lg < longueur:
            code = '0' + code
            lg = len(code)
        return code
        
        
    def augmentLisibilite(code):
        """
        code -- chaine de caractères de longueur multiple de 4. 
        
        renvoie la chaîne en insérant un blanc tous les 4 caractères.
        """
        chaine =  ''
        for  i, c in enumerate(code):
            if (i+1)%4 == 0:
                chaine = chaine + c  + ' ' 
            else:
                chaine = chaine + c
        return chaine
        

    def codeComplementA2(entier, longueur):
        """
        longueur -- entier naturel au moins égal à 2
        entier -- entier relatif représentable 
        en complément à 2 sur longueur bits.
        
        renvoie la chaîne de caractère correspondant 
        au code complément à 2 sur longueur bits
        de l'entier.
        >>> codeComplementA2(-6, 8)
        1111 1010
        >>> codeComplementA2(-32767, 16)
        1000 0000 0000 0001 
        >>> codeComplementA2(32767, 16)
        0111 1111 1111 1111
        >>> codeComplementA2(32768, 16)
        AssertionError: Entier non représentable sur 16 bits.
        """
        assert -2**(longueur-1) <= entier < 2**(longueur-1), f"Entier non représentable sur {longueur} bits."  
        if -2**(longueur-1) <= entier < 0: 
            entier = entier + 2**longueur
        return augmentLisibilite(completeCode(entier, longueur))
          
    ```
    
    En python version < 3.6 (sans f-string):
    
    ```python
    def codeComplementA2(entier, longueur):
        """
        longueur -- entier naturel au moins égal à 2
        entier -- entier relatif représentable 
        en complément à 2 sur longueur bits.

        renvoie la chaîne de caractère correspondant 
        au code complément à 2 sur longueur bits
        de l'entier.
        >>> codeComplementA2(-6, 8)
        1111 1010
        >>> codeComplementA2(-32767, 16)
        1000 0000 0000 0001 
        >>> codeComplementA2(32767, 16)
        0111 1111 1111 1111
        >>> codeComplementA2(32768, 16)
        AssertionError: Entier non représentable sur 16 bits.
        """
        assert -2**(longueur-1) <= entier < 2**(longueur-1), "Entier non représentable sur {} bits.".format(longueur)  
        if -2**(longueur-1) <= entier < 0: 
            entier = entier + 2**longueur
        return augmentLisibilite(completeCode(entier, longueur))
    ```
