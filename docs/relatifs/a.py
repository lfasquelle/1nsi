
def codeBinaire(entier):
    """
    entier: entier naturel 
    
    renvoie la chaîne de caractères correspondant à l'écriture binaire de entier.
    """
    if entier == 0: return '0'
    chaine = ''
    while entier != 0:
        chaine = str(entier % 2) + chaine
        entier = entier//2
    return chaine



def completeCode(entier, longueur):
    """
    longueur: entier > 1.
    entier: entier naturel, de code binaire ayant au plus longueur chiffre.
    
    Complète le code binaire (de type str) de entier à gauche par des 0 pour que la longueur 
    de ce code soit exactement de longueur longueur.
    """
    code = codeBinaire(entier)
    lg = len(code)
    while lg < longueur:
        code = '0' + code
        lg = len(code)
    return code
    
    
def augmentLisibilite(code):
    """
    code: chaine de caractères de longueur multiple de 4. 
    
    renvoie la chaîne en insérant un blanc tous les 4 caractères.
    """
    chaine =  ''
    for  i, c in enumerate(code):
        if (i+1)%4 == 0:
            chaine = chaine + c  + ' ' 
        else:
            chaine = chaine + c
    return chaine
    

def codeComplementA2(entier, longueur):
    """
    longueur: entier naturel au moins égal à 2
    entier: entier relatif représentable en complément à 2 sur longueur bits.
    
    renvoie la chaîne de caractère correspondant au code complément à 2 sur longueur bits
    de l'entier.
    >>> codeComplementA2(-6, 8)
    1111 1010
    >>> codeComplementA2(-32767, 16)
    1000 0000 0000 0001 
    >>> codeComplementA2(32767, 16)
    0111 1111 1111 1111
    >>> codeComplementA2(32768, 16)
    AssertionError: Entier non représentable sur 16 bits.
    """
    assert -2**(longueur-1) <= entier < 2**(longueur-1), f"Entier non représentable sur {longueur} bits."  
    if -2**(longueur-1) <= entier < 0: 
        entier = entier + 2**longueur
    return augmentLisibilite(completeCode(entier, longueur))
        
        
        
n = 32768
longueur = 16
print(codeComplementA2(n, longueur))
