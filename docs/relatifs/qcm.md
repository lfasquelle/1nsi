# QCM


## QCM 1

Sur 32 bits, on peut écrire:

- [ ] 2<sup>32</sup> codes distincts.
- [ ] 32<sup>2</sup> codes distincts.
- [ ] 2<sup>32</sup>-1 codes distincts.
- [ ] 32<sup>2</sup>-1 codes distincts.


??? solution "Réponse"

    - [X] 2<sup>32</sup> codes distincts.
    - [ ] 32<sup>2</sup> codes distincts.
    - [ ] 2<sup>32</sup>-1 codes distincts.
    - [ ] 32<sup>2</sup>-1 codes distincts.



## QCM 2

En complément à deux sur 64 bits, on code:

- [ ] les entiers m tels que    -2<sup>64</sup> &le; m  &le; 2<sup>64</sup>-1
- [ ] les entiers m tels que    -2<sup>63</sup> &le; m  < 2<sup>63</sup>
- [ ] les entiers m tels que    -2<sup>32</sup> &le; m  &le; 2<sup>32</sup>-1

??? solution "Réponse"


    - [ ] les entiers m tels que    -2<sup>64</sup> &le; m  &le; 2<sup>64</sup>-1
    - [X] les entiers m tels que    -2<sup>63</sup> &le; m  < 2<sup>63</sup>
    - [ ] les entiers m tels que    -2<sup>32</sup> &le; m  &le; 2<sup>32</sup>-1



## QCM 3

En complément à deux sur 32 bits, on code:

- [ ] autant d'entiers strictement négatifs que d'entiers positifs ou nuls.
- [ ] autant d'entiers strictement négatifs que d'entiers strictement positifs.
- [ ] 0 a deux codes différents.



??? solution "Réponse"

    - [X] autant d'entiers strictement négatifs que d'entiers positifs ou nuls.
    - [ ] autant d'entiers strictement négatifs que d'entiers strictement positifs.
    - [ ] 0 a deux codes différents.
    
    
## QCM 4

On veut coder en complément à deux le produit 1 500 000 &times; 250.

Il suffit d'un codage en complément à deux sur:


- [ ] un octet
- [ ] deux octets
- [ ] quatre octets
- [ ] huit octets

??? solution "Réponse"

    - [ ] un octet
    - [ ] deux octets
    - [X] quatre octets
    - [X] huit octets
    
    1 500 000 &times; 250 = 375 000 000.  
    2<sup>15</sup>-1 = 32 767: un codage en complément à deux sur 16 bits ne suffit pas.  
    2<sup>31</sup>-1 = 2 147 483 647: un codage en complément à deux sur 32 bits suffit (et a fortiori sur 64 bits).
    
    
    
## QCM 5

On veut coder en complément à deux la différence 40000 - 35000.

Il suffit d'un codage en complément à deux sur:



- [ ] un octet
- [ ] deux octets
- [ ] quatre octets
- [ ] huit octets


??? solution "Réponse"

    - [ ] un octet
    - [ ] deux octets
    - [X] quatre octets
    - [X] huit octets
    
    Si on veut coder uniquement le résultat 5000 alors le code sur deux octets suffit  puisque sur deux octets,
    on code les entiers positifs jusqu'à 2<sup>15</sup>-1 = 32 767 (qui est > 5000).  
    Mais si l'on veut coder la différence (donc coder 40 000 et 35 000), deux octets ne suffisent plus
    (mais quatre octets suffisent puisque 2<sup>31</sup>-1 = 2 147 483 647).
