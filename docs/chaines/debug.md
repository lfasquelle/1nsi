# Débuguer



## Qu'est ce qu'un bug?



!!! note 
    Extrait de [wikipedia](https://fr.wikipedia.org/wiki/Bug_(informatique))
    
    En informatique, un bug (insecte en anglais) ou bogue (au Québec et en France) 
    est un défaut de conception d'un programme informatique à l'origine d'un dysfonctionnement.

    La gravité du dysfonctionnement peut aller de bénigne, 
    causant par exemple des défauts d'affichage mineurs, à majeure, 
    tels un crash système pouvant entraîner de graves accidents, 
    par exemple la destruction en vol de la première fusée Ariane 5, en 1996.

    Un bug peut résider dans une application, 
    dans les logiciels tiers utilisés par cette application, 
    voire dans le firmware d'un composant matériel 
    comme ce fut le cas du bug de la division du Pentium. 
     
     
Un bug, c'est essentiellement un défaut de programmation.

En apprenant à programmer, vous commettrez deux types de bugs:

+ l'erreur de syntaxe: non respect de la syntaxe du langage, oubli d'une indentation, oubli d'un paramètre
dans une fonction... 
Dans ce cas, votre programme ne s'exécute même pas. Toute erreur de ce type devra être corrigée, que ce soit durant
les séances d'exercices ou pour un travail en-dehors du temps de classe. Pour un devoir à rendre, la présence
d'une telle erreur entraîne la note minimale.
+ l'erreur de logique: le programme ne fait pas tout à fait ce qui est attendu, vous avez oublié un ou des 
cas... Il faut également autant que possible éliminer ce genre d'erreurs. 


## Corriger un bug de syntaxe

Les outils pour corriger un tel bug:

+ Relire le cours.
+ Consulter la documentation python en ligne ou en local (fonction help...)
+ Utiliser les f-strings et la fonction print pour afficher les résultats intermédiaires et comprendre à
quel moment une variable n'est pas du type attendu.
+ On peut également utiliser le site [python tutor](http://pythontutor.com/visualize.html#mode=edit) qui permet
de visualiser l'état des variables au fur et à mesure du déroulement, ce qui facilite le repèrage de la source 
du bug. 
+ L'affichage des états des variables peut aussi être obtenu avec les bons éditeurs dédiés 
([spyder par exemple](https://docs.spyder-ide.org/variableexplorer.html)).



## Corriger un bug de logique



+ Toujours anticiper en prévoyant un jeu de tests de  vos fonctions, c'est à dire des exemples de paramètres
et du résultat qui doit être obtenu. Penser toujours aux cas extrêmes: par exemple, pour une fonction concernant
les chaînes de caractères, que se passe-t-il pour la chaîne vide?
+  Utiliser les f-strings et la fonction print pour afficher les résultats intermédiaires et comprendre à
quel moment une variable n'a plus la valeur espérée.
+ Afficher l'état des variables avec les outils proposés par un bon IDE ou pythontutor par exemple.
+ &Eacute;crire une fonction de tests.
+ Utiliser les outils de debug d'un bon IDE ([par exemple spyder](https://docs.spyder-ide.org/debugging.html)).



## Exercice 


Antoine doit écrire une fonction python calculant pour un entier n, le nombre $n! = 1\times 2\times 3 \times ... \times n$.

```python
def factoriel(n):
    """
    n -- entier naturel > 0
    
    renvoie n!
    >>> 1!
    1
    >>> 2!
    2
    >>> 3!
    6
    >>> 5!
    120
    """
```

!!! important
    Rappelons que le docstring et un jeu de tests présentés comme ci-dessus
    sont toujours attendus pour vos fonctions.  
    
    
Antoine propose le code suivant:



```python
def factoriel(n):
    """
    n -- entier naturel > 0
    
    renvoie n!
    >>> 1!
    1
    >>> 2!
    2
    >>> 3!
    6
    >>> 5!
    120
    """
    f = 0
    for i in range(n):
        f = f*i
    return f
```

+ Utiliser doctest pour savoir quels tests réussissent.
+ Insérer des `print` dans ce code pour suivre l'évolution des variables et corriger ce code.



??? solution "Rappel de la commande doctest"

    Si le script se trouve dans le fichier factoriel.py, dans la console entrez:
    
    ```
    $ python3 -m doctest -v factoriel.py
    ```
    
    
??? solution "Réponse possible"
    Affichage de messages possibles pour suivre l'évolution des valeurs des variables:
    [fichier ipynb](fichiers/factoriel.ipynb) ([version html](fichiers/factoriel.html)).
    
    Des corrections:
    
    + les tests doivent utilisés le nom de la fonction!
    + f ne doit pas être initialisé à 0 mais à 1.
    + la première valeur prise par i ne doit pas être 0 mais 1.
    
    Enfin une dernière erreur:
    
    + la dernière valeur prise par i doit être n et non n-1.
    
    D'où une proposition de code:
    
    ```python
    def factoriel(n):
        """
        n -- entier naturel > 0

        renvoie n!
        >>> factoriel(1)
        1
        >>> factoriel(2)
        2
        >>> factoriel(3)
        6
        >>> factoriel(5)
        120
        """
        f = 1
        for i in range(1, n+1):
            f = f*i
        return f
    ```
    
    Test (version python >= 3.6):
    
    ```
    for i in range(6): print(f"{i}! = {factoriel(i)}")
    ```

    Test avec une version python < 3.6:
    
    ```
    for i in range(6): print("{}! = {}".format(i, factoriel(i)))
    ```
    
    !!! note
        Lorsqu'on a fini de débuguer, il ne faut pas oublier de supprimer tous les print de debugage.
        C'est ce qui fait qu'il est, à terme, préférable d'écrire des fonctions de tests indépendantes:
        on évite d'ajouter, enlever, ajouter, enlever... des lignes de print pour débuguer.
        Mais les print (ou l'affichage des valeurs des variables via un outil adéquat) restent souvent
        pratiques sur des petits programmes
        pour repèrer à quel endroit on a commis une erreur de conception.
         


??? note "Une utilisation de doctest sans passer par la ligne de commande"

    Copier et coller le code ci-dessous dans un éditeur (geany, spyder...). Tester (notamment
    en glissant des erreurs dans le code de la fonction pour visualiser la réponse de doctest).
    
    ```python
    def factoriel(n):
        """
        n -- entier naturel > 0

        renvoie n!
        >>> factoriel(1)
        1
        >>> factoriel(2)
        2
        >>> factoriel(3)
        6
        >>> factoriel(5)
        120
        """
        f = 1
        for i in range(1, n+1):
            f = f*i
        return f

    # tests des fonctions pouvant être testées avec doctest:
    import doctest
    doctest.testmod()
    ```
