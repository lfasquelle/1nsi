# Chaînes et fichiers



Dans cette page d'exercices, nous allons un peu plus loin en créant un fichier html à l'aide de Python.

Le code mélange donc du python, du css, de l'html... Il vous faudra peut-être revenir plus tard dans l'année sur ce code
pour bien comprendre le rôle de chaque instruction.

## Exercice 1

On considère le code Python suivant:

```python

def structure(v):
    return """
    <!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta charset="utf-8">
        <title> table </title>
        <style>
            html{{font-size: 20px;}}
            body{{width: 80%; margin: 0 auto;}}
            table{{border: 2px ridge orange;}}
            td{{padding:3px;}}
        </style>
    </head>
    
    <body>
    <table>
    {}
    </table>
    </body>
    </html>
    """.format(v)



def table(n, s):
    """
    n -- entier > 0
    s -- entier > 0
    """
    ch =''
    for k in range(0, s+1):
        ligne = "<tr><td>{}&times;{}</td><td>=</td><td>{}</td></tr>".format(k, n, k*n)
        ch = ch + ligne + '\n'
    return ch
     
     
     
def creeFichier(nom, chaine):
    """
    nom -- chaîne de caractères, nom d'un fichier à créer
    chaine -- chaîne de caractères, chaîne à inscrire dans le fichier à créer
    """
    # on crée un fichier html qui sera nommé nom:
    with open(nom + '.html', 'w') as f:
        # on écrit la chaine dans ce fichier:
        print(chaine, file=f)
        
creeFichier('table7', structure(table(7, 12)))    
```


+ Tester ce code python. Quel est son effet?

??? solution "Réponse"
    Le code crée (dans le même répertoire que le fichier du script python) un fichier
    html nommé table7.html et contenant un tableau de la table du 7.
    
    
    
    
+ Dans l'exécution précédente, que vaut la variable `v` paramètre de la fonction `structure` lors
de l'exécution de cette fonction `structure`?

??? solution "Réponse"
    La fonction `structure` est appelée avec l'argument `table(7, 12)`. Lors de l'exécution
    de la fonction `structure`, la variable `v` reçoit donc cette valeur.
    
    La valeur de `table(7, 12)` est la chaîne suivante:
    
    ```
    <tr><td>0&times;7</td><td>=</td><td>0</td></tr>
    <tr><td>1&times;7</td><td>=</td><td>7</td></tr>
    <tr><td>2&times;7</td><td>=</td><td>14</td></tr>
    <tr><td>3&times;7</td><td>=</td><td>21</td></tr>
    <tr><td>4&times;7</td><td>=</td><td>28</td></tr>
    <tr><td>5&times;7</td><td>=</td><td>35</td></tr>
    <tr><td>6&times;7</td><td>=</td><td>42</td></tr>
    <tr><td>7&times;7</td><td>=</td><td>49</td></tr>
    <tr><td>8&times;7</td><td>=</td><td>56</td></tr>
    <tr><td>9&times;7</td><td>=</td><td>63</td></tr>
    <tr><td>10&times;7</td><td>=</td><td>70</td></tr>
    ```
    
    Cette chaîne contient le code html des lignes de la table de multiplication du 7 (sans les 
    balises `<table></table>`).
    
    
    
+ Modifier le nécessaire pour créer un fichier table9.html contenant la table du 9 de 0&times;9 à 20&times;9.

??? solution "Réponse"
    Il suffit de modifier la dernière ligne en la ligne suivante:
    
    ```
    creeFichier('table9', structure(table(9, 20))
    ```
    
+ Dans ce qui est renvoyé par la fonction `structure`, vous pouvez constater que les règles css
utilisent apparemment deux accolades. Pouvez-vous expliquer pourquoi?

??? solution "Réponse"
    Avec une simple accolade, la méthode python `format` chercherait à interpréter ce qu'il y a à l'intérieur de 
    l'accolade.
    
    La double accolade annule cette interprétation par la méthode `format` et dans le fichier html produit, vous 
    pouvez vérifier que les règles css ne font intervenir qu'une seule accolade, comme le veut la syntaxe 
    du css.  En d'autres termes dans le fichier **html** produit, on lit:
    
    ```html
    <style>
        html{font-size: 20px;}
        body{width: 80%; margin: 0 auto;}
        table{border: 2px ridge orange;}
        td{padding:3px;}
    </style>
    ```
    
    Les doubles accolades du code python ont disparues pour laisser place à des accolades simples.



+ En ligne 34 du code `ch = ch + ligne + '\n'`, quel est le rôle du `\n` ? 
Si on le supprime (en remplaçant la ligne 34 par `ch = ch + ligne`), quelles différences obtiendra-t-on 
dans le fichier produit ?

??? solution "Réponse"

    `\n` permet d'aller à la ligne dans le fichier texte produit. Attention: dans le fichier **texte** (en ouvrant 
    avec un éditeur de texte), 
    par contre pour le rendu html obtenu en ouvrant le fichier avec un navigateur, aucune différence!.
    
    Avec le `\n`, les lignes de code html générées pour le tableau sont:
    
    ```html
    <tr><td>0&times;7</td><td>=</td><td>0</td></tr>
    <tr><td>1&times;7</td><td>=</td><td>7</td></tr>
    <tr><td>2&times;7</td><td>=</td><td>14</td></tr>
    <tr><td>3&times;7</td><td>=</td><td>21</td></tr>
    <tr><td>4&times;7</td><td>=</td><td>28</td></tr>
    <tr><td>5&times;7</td><td>=</td><td>35</td></tr>
    <tr><td>6&times;7</td><td>=</td><td>42</td></tr>
    <tr><td>7&times;7</td><td>=</td><td>49</td></tr>
    <tr><td>8&times;7</td><td>=</td><td>56</td></tr>
    <tr><td>9&times;7</td><td>=</td><td>63</td></tr>
    <tr><td>10&times;7</td><td>=</td><td>70</td></tr>
    <tr><td>11&times;7</td><td>=</td><td>77</td></tr>
    <tr><td>12&times;7</td><td>=</td><td>84</td></tr>
    ```
    
    et sans le `\n` (tout est sur une seule ligne): 
    
    ```
    <tr><td>0&times;7</td><td>=</td><td>0</td></tr><tr><td>1&times;7</td><td>=</td><td>7</td></tr><tr><td>2&times;7</td><td>=</td><td>14</td></tr><tr><td>3&times;7</td><td>=</td><td>21</td></tr><tr><td>4&times;7</td><td>=</td><td>28</td></tr><tr><td>5&times;7</td><td>=</td><td>35</td></tr><tr><td>6&times;7</td><td>=</td><td>42</td></tr><tr><td>7&times;7</td><td>=</td><td>49</td></tr><tr><td>8&times;7</td><td>=</td><td>56</td></tr><tr><td>9&times;7</td><td>=</td><td>63</td></tr><tr><td>10&times;7</td><td>=</td><td>70</td></tr><tr><td>11&times;7</td><td>=</td><td>77</td></tr><tr><td>12&times;7</td><td>=</td><td>84</td></tr>
    ```


??? note "Une version plus complète"

    ```python
    def structure(entier, deroulement):
        return """
        <!DOCTYPE html>
        <html lang="fr">
        <head>
            <meta charset="utf-8">
            <title> somme </title>
            <style>
            html{{font-size: 18px;}}
            body{{margin: 2rem auto; width: 80%;}}
            </style>
        </head>

        <body>

        <p>Pour calculer la somme des entiers de 1 à n, on utilise la fonction python suivante: </p>
        
        <pre>
        <code>
        def somme(n):
        s = 0
        for k in range(1, n+1):
            s = s + k

        return s
        </code>
        </pre>


        <p>Le détail du déroulement avec l'entier {}: </p>
        {}

        </body>
        </html>
        """.format(entier, deroulement)



    def somme(n):
        """
        n -- entier > 0
        """
        s = 0
        somme_chaine = '0'
        ch = "<code>s = 0</code> : la variable s contient 0.<br><br>"

        for k in range(1, n+1):
            s = s + k
            somme_chaine = somme_chaine + '+' + str(k)
            ligne = "La variable k a pour valeur {}.<br>".format(k)
            ligne += "<code>s = s+k</code> : la variable s contient la somme {}, soit {}.<br><br>".format(somme_chaine, s)
            ch = ch + ligne + '\n'

        return ch



    def creeFichier(nom, chaine):
        """
        nom -- chaîne de caractères, nom d'un fichier à créer
        chaine -- chaîne de caractères, chaîne à inscrire dans le fichier à créer
        """
        # on crée un fichier html qui sera nommé nom:
        with open(nom + '.html', 'w') as f:
            # on écrit la chaine dans ce fichier:
            print(chaine, file=f)


    n = 9
    creeFichier('somme' + str(n), structure(n, somme(n)))
    ```

    [fichier html résultat](fichiers/somme9.html)


## Exercice 2

Dans cet exercice, on reprend une partie du code précédent pour afficher dans une page html le détail
de fonctionnement d'un algorithme de calcul de la somme des n premiers entiers naturels.


```python

def structure(v):
    return """
    <!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta charset="utf-8">
        <title> somme </title>
        <style>
        html{{font-size: 18px;}}
        body{{margin: 2rem auto; width: 80%;}}
        </style>
    </head>
    
    <body>
     
     
    {}
     
    </body>
    </html>
    """.format(v)



def somme(n):
    """
    n -- entier > 0
    """
    s = 0
    somme_chaine = '0'
    ch = ''
    
    for k in range(1, n+1):
        s = s + k
        somme_chaine = somme_chaine + '+' + str(k)
        ligne = "La variable s contient la somme {}, soit {}.<br>".format(somme_chaine, s)
        ch = ch + ligne + '\n'
        
    return ch
     
     
     
def creeFichier(nom, chaine):
    """
    nom -- chaîne de caractères, nom d'un fichier à créer
    chaine -- chaîne de caractères, chaîne à inscrire dans le fichier à créer
    """
    # on crée un fichier html qui sera nommé nom:
    with open(nom + '.html', 'w') as f:
        # on écrit la chaine dans ce fichier:
        print(chaine, file=f)
        
creeFichier('somme9', structure(somme(9)))
```



+ Testez ce fichier.
+ Modifiez le pour proposer de façon analogue le détail de fonctionnement d'une fonction calculant
le produit des n premiers entiers naturels non nuls.


??? solution "Un code possible"

    ```python
    def structure(v):
        return """
        <!DOCTYPE html>
        <html lang="fr">
        <head>
            <meta charset="utf-8">
            <title> produit </title>
            <style>
            html{{font-size: 18px;}}
            body{{margin: 2rem auto; width: 80%;}}
            </style>
        </head>
        
        <body>
         
         
        {}
         
        </body>
        </html>
        """.format(v)



    def produit(n):
        """
        n -- entier > 1
        """
        p = 1
        produit_chaine = '1'
        ch = ''
        
        for k in range(2, n+1):
            p = p * k
            produit_chaine = produit_chaine + '&times;' + str(k)
            ligne = "La variable p contient le produit {}, soit {}.<br>".format(produit_chaine, p)
            ch = ch + ligne + '\n'
            
        return ch
         
         
         
    def creeFichier(nom, chaine):
        """
        nom -- chaîne de caractères, nom d'un fichier à créer
        chaine -- chaîne de caractères, chaîne à inscrire dans le fichier à créer
        """
        # on crée un fichier html qui sera nommé nom:
        with open(nom + '.html', 'w') as f:
            # on écrit la chaine dans ce fichier:
            print(chaine, file=f)
            
    creeFichier('produit', structure(produit(12)))
    ```
