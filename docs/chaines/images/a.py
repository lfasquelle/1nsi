from PIL import Image

# ouverture de l'image:
source = Image.open('feux.png')

# récupération de ses dimensions :
largeur, hauteur = source.size
print(f"Le nombre de colonnes est {largeur}, le nombre de lignes est {hauteur}.")


# récupération de la couleur du coin supérieur gauche:
couleur_coin_haut_gauche = source.getpixel((0,0))
# récupération de la couleur du coin inférieur droit:
couleur_coin_bas_droit = source.getpixel((largeur-1,hauteur-1))


print(f"Le pixel en haut à gauche (colonne 0, ligne 0) a pour couleur {couleur_coin_haut_gauche}.")
print(f"Le pixel en bas à droite (colonne {largeur-1}, ligne {hauteur-1}) a pour couleur {couleur_coin_bas_droit}.")
