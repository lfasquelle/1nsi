# Préfixes et suffixes


## Vocabulaire: préfixe

Pour un mot donné m, on appelle préfixe de ce mot m tout mot constitué des premières lettres du mot m.

Exemples:

+ Les préfixes du mot "carambar" sont c, ca, car, cara, caram, caramb, caramba, carambar.
+ Les préfixes du mot "NSI" sont N, NS, NSI.


## Exercice 1

&Eacute;crire un corps possible pour la fonction ci-dessous:

```python
def prefixe(mot, n):
    """
    mot -- de type string
    n -- entier > 0
    précondition: n <= nombre de lettres de mot
    
    renvoie le préfixe de longueur n de mot
    
    >>> prefixe('carambar', 3)
    'car'
    """
```


??? solution "Un code"

    ```python
    def prefixe(mot, n):
        """
        mot -- de type string
        n -- entier > 0
        précondition: n <= nombre de lettres de mot
        
        renvoie le préfixe de longueur n de mot
        
        >>> prefixe('carambar', 3)
        'car'
        """
        assert n <= len(mot), "Attention, pas de préfixe plus long que le mot!"
        pref = ''
        for k in range(n):
            pref = pref + mot[k]
        return pref
    ```
    
    
    
    Remarque:  prefixe('carambar', 0) renvoie la chaîne vide. Dans l'étude théorique des mots, on définit
    souvent la chaîne vide (notée en général ε -- lire epsilon -- dans les cours de théorie des mots) comme
    étant un préfixe de tout mot.
    
    
    

## Exercice 2 

&Eacute;crire  un code possible pour le corps de la fonction ci-dessous:


```python
def prefixes(mot):
    """
    mot -- de type string (ne comportant pas de virgule)
    précondition: mot n'est pas une chaîne vide
    
    renvoie une chaîne constituée de tous les préfixes 
    (de longueur > 0) de mot, 
    séparés par une virgule.
    
    >>> prefixes('carambar')
    'c,ca,car,cara,caram,caramb,caramba,carambar'
    """
```

??? solution "Un code"

    On utilise la fonction de l'exercice précédent.
    

    ```python
    def prefixes(mot):
        """
        mot -- de type string (ne comportant pas de virgule)
        précondition: mot n'est pas une chaîne vide
        
        renvoie une chaîne constituée de tous les préfixes 
        (de longueur > 0) de mot, 
        séparés par une virgule.
        
        >>> prefixes('carambar')
        'c,ca,car,cara,caram,caramb,caramba,carambar'
        """
        assert mot != '', "Attention, mot ne doit pas être une chaîne vide."
        ch = mot[0]
        for k in range(2, len(mot)+1):
            pref = prefixe(mot, k)
            ch = ch + ',' + pref
        return ch
    ```




## Vocabulaire: suffixe



Pour un mot donné m, on appelle suffixe de ce mot m tout mot constitué des dernières lettres du mot m.

Exemples:

+ Les suffixes du mot carambar sont r, ar, bar, mbar, ambar, rambar, arambar, carambar.
+ Les suffixes du mot NSI sont I, SI, NSI.


## Exercice 3

&Eacute;crire  un code possible pour le corps de la fonction ci-dessous:


```python
def suffixe(mot, n):
    """
    mot -- de type string
    n -- entier > 0
    précondition: n <= nombre de lettres de mot
    
    renvoie le suffixe de longueur n de mot
    
    >>> suffixe('carambar', 3)
    'bar'
    """
```

??? solution "Un code"


    ```python
    def suffixe(mot, n):
        """
        mot -- de type string
        n -- entier > 0
        précondition: n <= nombre de lettres de mot
        
        renvoie le suffixe de longueur n de mot
        
        >>> suffixe('carambar', 3)
        'bar'
        """
        assert n <= len(mot), "Attention, pas de suffixe plus long que le mot!"
        suff = ''
        for k in range(len(mot)-1, len(mot)-1-n, -1):
            suff = mot[k] + suff
        return suff
    ```
    
    Remarque: on peut aussi convenir que le mot vide est suffixe de tout mot. On l'obtient avec n = 0 
    dans le code précédent.


??? solution "variante"


    ```python
    def suffixe(mot, n):
        """
        mot -- de type string
        n -- entier > 0
        précondition: n <= nombre de lettres de mot
        
        renvoie le suffixe de longueur n de mot
        
        >>> suffixe('carambar', 3)
        'bar'
        """
        assert n <= len(mot), "Attention, pas de suffixe plus long que le mot!"
        suff = ''
        for k in range(len(mot)-n, len(mot)):
            suff = suff + mot[k]
        return suff
    ```




## Exercice 4 

&Eacute;crire  un code possible pour le corps de la fonction ci-dessous:


```python
def suffixes(mot):
    """
    mot -- de type string (ne comportant pas de virgule)
    précondition: mot n'est pas une chaîne vide
    
    renvoie une chaîne constituée de tous les suffixes 
    (de longueur > 0) de mot, 
    séparés par une virgule.
    
    >>> suffixes('carambar')
    'r,ar,bar,mbar,ambar,rambar,arambar,carambar'
    """
```

??? solution "Un code"

    On utilise la fonction de l'exercice précédent.
    


    ```python
    def suffixes(mot):
        """
        mot -- de type string (ne comportant pas de virgule)
        précondition: mot n'est pas une chaîne vide
        
        renvoie une chaîne constituée de tous les suffixes 
        (de longueur > 0) de mot, 
        séparés par une virgule.
        
        >>> suffixes('carambar')
        'r,ar,bar,mbar,ambar,rambar,arambar,carambar'
        """
        assert mot != '', "Attention, mot ne doit pas être une chaîne vide."
        ch = mot[-1]
        for k in range(2, len(mot)+1):
            ch = ch + ',' + suffixe(mot, k)
        return ch
    ```
    
    
    
## Vocabulaire: bord

On appelle bord d'un mot m tout mot qui est à la fois préfixe et suffixe du mot m, sans être le mot m lui-même.

Exemples:

+ Les bords de abacaba: ab, aba.  
+ Les bords de abcabcab: ab, abcab.
+ "je suis" est un bord de la chaîne "je suis comme je suis".


On considère également que la chaîne vide est un bord de toute chaîne.



## Exercice 5

&Eacute;crire un code possible pour le corps de la fonction ci-dessous:

```python
def lePlusLongBord(mot):
    """
    mot -- chaîne de caractères
    
    renvoie le plus long bord de mot.
    
    >>> lePlusLongBord('abcabcab')
    'abcab'
    """
```


??? solution "Un code"

    On utilise les fonctions `suffixe` et `préfixe` des exercices précédents.
    
    

    ```python
    def lePlusLongBord(mot):
        """
        mot -- chaîne de caractères
        
        renvoie le plus long bord de mot.
        
        >>> lePlusLongBord('abcabcab')
        'abcab'
        """
        bord = ''
        for k in range(1,len(mot)):
            if suffixe(mot,k) == prefixe(mot, k):
                bord = suffixe(mot,k)
        return bord
    ```

 
