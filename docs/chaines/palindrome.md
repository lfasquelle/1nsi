# Palindrome




## Vocabulaire: miroir


On appelle miroir d'un mot, le mot écrit à contre-sens.

Exemples:

+ le miroir de "soleil" est "lielos".
+ le miroir de "ados" est "soda".
+ le miroir de "épater" est "retapé".
+ le miroir de "repas" est "saper".

!!! note
    Lorsque un mot et un miroir ont tous deux une signification, on parle 
    [d'anacyclique](https://fr.wikipedia.org/wiki/Anacyclique).
    
    
    
    

## Exercice 1



&Eacute;crire un code possible pour la fonction:

```python
def miroir(mot):
    """
    mot -- chaîne de caractères
    
    renvoie la chaîne miroir de mot.
    
    >>> miroir('soleil')
    'lielos'
    """
```


??? solution "Un code"




    ```python
    def miroir(mot):
        """
        mot -- chaîne de caractères
        
        renvoie la chaîne miroir de mot.
        
        >>> miroir('soleil')
        'lielos'
        """
        ch = ''
        for lettre in mot:
            ch = lettre + ch
        return ch
    ```



## Vocabulaire: palindrome

On appelle palindrome un mot qui est égal à son miroir.

Exemples:


+ radar, rotor, kayak, été, ici, tôt, ressasser
+ "engagelejeuquejelegagne"
+ "karineallaenirak"







## Exercice 2





&Eacute;crire un code possible pour la fonction:

```python
def est_palindrome(mot):
    """
    mot -- chaîne de caractères
    renvoie True si mot est égal à son miroir, False sinon.
    
    >>> est_palindrome('été')
    True
    >>> est_palindrome('hiver')
    False
    """
```

+ &Eacute;crire une version utilisant la fonction miroir de l'exercice précédent.
+ &Eacute;crire une version n'utilisant pas la fonction miroir de l'exercice précédent.

??? solution "Un code avec miroir."

    On utilise la fonction miroir précédente.


    ```python
    def est_palindrome(mot):
        """
        mot -- chaîne de caractères
        renvoie True si mot est égal à son miroir, False sinon.
        
        >>> est_palindrome('été')
        True
        >>> est_palindrome('hiver')
        False
        """
        return mot == miroir(mot)
    ```


??? solution "Un code sans miroir."

    Le principe:
    
    - si la première lettre n'est pas égale à la dernière, ce n'est pas un palindrome.
    - Sinon, si la seconde lettre n'est pas égale à l'avant-dernière, ce n'est pas un palindrome.
    - Sinon, ...
    
    Et on peut arrêter les tests en arrivant à la moitié du mot...


    ```python
    def est_palindrome(mot):
        """
        mot -- chaîne de caractères
        renvoie True si mot est égal à son miroir, False sinon.
        
        >>> est_palindrome('été')
        True
        >>> est_palindrome('hiver')
        False
        """
        for k in range(0,len(mot)//2+1):
            if mot[k] != mot[len(mot)-1-k]:
                return False
        return True
    ```

