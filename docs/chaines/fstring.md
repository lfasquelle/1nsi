# f-string



## f-string

Placer un f devant une chaîne de caractères permet d'intégrer les valeurs de variables dans la chaîne.



```
>>> a = 5
>>> b = 6
>>> chaine = f"a a pour valeur {a} et b a pour valeur {b}."
>>> chaine
'a a pour valeur 5 et b a pour valeur 6.'
```


Cette notation est très pratique pour avoir des messages explicites.


Cette possibilité n'a été introduite qu'à la version 3.6 de Python.
Pour une version antérieure à la 3.6, on utilisera format (moins lisible):

```
>>> a = 5
>>> b = 6
>>> chaine = "a a pour valeur {} et b a pour valeur {}.".format(a, b)
>>> chaine
'a a pour valeur 5 et b a pour valeur 6.'
```

On peut également utiliser la concaténation de chaînes et la conversion de types pour aboutir à la même chaîne:

```
>>> a = 5
>>> b = 6
>>> chaine = "a a pour valeur " + str(a) + " et b a pour valeur " + str(b) + "." 
>>> chaine
'a a pour valeur 5 et b a pour valeur 6.'
```


## Exercice  1

&Eacute;crire un corps possible pour la fonction 



```python
def repeter(ch, effectif):
    """
    ch -- de type str
    effectif -- entier, >0
    
    renvoie la chaîne constituée de effectif concaténation de ch.
    >>> repeter('*', 3)
    '***'
    >>> repeter('cou', 2)
    'coucou'
    """
```


!!! attention

    Encore le principe *accumulateur* !

??? solution "Un code possible"

    ```python
    def repeter(ch, effectif):
        """
        ch -- de type str
        effectif -- entier, >0
        
        renvoie la chaîne constituée de effectif concaténation de ch.
        >>> repeter('*', 3)
        '***'
        >>> repeter('cou', 2)
        'coucou'
        """
        resultat = ''
        for k in range(effectif):
            resultat = resultat + ch
        return resultat
    ```


    Remarque. On peut écrire  `resultat += ch` à la place de `resultat = resultat + ch`. A vous de décider laquelle
    des deux écritures vous préférez.







## Exercice 2


 
    
    
&Eacute;crire un code possible pour le corps de la fonction suivante:

```python
def etoile(n):
    """
    n -- entier naturel > 0
    
    renvoie une chaine triangle d'étoiles
    >>> print(etoile(3))
    *
    **
    ***
    >>> print(etoile(4))
    *
    **
    ***
    ****
    """
```


!!! attention

    Utilisez la fonction de l'exercice précédent pour simplifier la tâche de celui-ci.
    
    Ce conseil est un conseil de bonne pratique à respecter:  
    décomposer toute tâche en plus petites tâches réparties dans des fonctions.
     
    Ce découpage en petites fonctions permet:
    
    - une meilleure lisibilité du code, le nom des fonctions étant choisi de façon à expliciter leur rôle.
    - une correction plus facile: chaque fonction peut être pensée séparément. 
    Chacune ayant une petite tâche élémentaire, elle sera plus facile à concevoir et à corriger.
    
    
    
!!! note "Passer à la ligne"

    On rappelle qu'un passage à la ligne s'obtient en ajoutant `\n` à la chaîne de caractères.
    
    

??? solution "Un code possible"
    
    On utilise la fonction de l'exercice précédent.
    
    
    ```python
    def etoile(n):
        """
        n -- entier naturel > 0
        
        renvoie une chaine triangle d'étoiles
        >>> print(etoile(3))
        *
        **
        ***
        >>> print(etoile(4))
        *
        **
        ***
        ****
        """
        ch = ''
        for i in range(1, n+1):
            ch = ch + repeter('*', i) + "\n"
        return ch
    ```


??? solution "Avec un f-string"

    Pour une version python >= 3.6:
    
    ```
    def etoile(n):
        """
        n -- entier naturel > 0

        renvoie une chaine triangle d'étoiles
        >>> print(etoile(3))
        *
        **
        ***
        >>> print(etoile(4))
        *
        **
        ***
        ****
        """
        ch = ''
        for i in range(1, n+1):
            ch +=  f"{repeter('*', i)}\n"
        return ch
    ```


