# Accèder aux caractères d'une chaîne




## Longueur

La longueur d'une chaîne de caractères est le nombre de caractères de la chaîne.

La fonction `len` (abréviation de length) permet de connaître cette longueur.

```
>>> a = "J'aime Python."
>>> len(a)
14
```

Comptez les caractères dans la chaîne ci-dessus: vous constatez que le blanc, l'apostrophe, le point 
sont des caractères comme les autres.

## Numérotation

Dans une chaîne de caractères, chaque caractère est numéroté  de 0 à longueur(chaîne)-1.

On peut obtenir le caractère numéro i par `chaine[i]`:

```
>>> a = "J'aime Python."
>>> a[0]
'J'
>>> a[1]
"'"
>>> a[2]
'a'
>>> a[len(a)-1]
'.'
>>> a[len(a)-2]
'n'
```


Python autorise également de numéroter -1 le dernier élément, -2 l'avant-dernier...:


```
>>> a = "J'aime Python."
>>> a[-1]
'.'
>>> a[-2]
'n'
>>> a[-len(a)]
'J'
```

Il faut comprendre le code `a[-1]` comme une abréviation de `a[len(a)-1]`.
De même `a[-2]` comme une abréviation de `a[len(a)-2]`...

## Immuable

Une chaîne est immuable, ce qui signifie qu'on ne peut pas la modifier après l'avoir définie.


```
>>> ch = "Coucou les loulous."
>>> ch[0] = 'B'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'str' object does not support item assignment
```

On a voulu modifier la première lettre: cela provoque une erreur.  
L'erreur indiquée est `'str' object does not support item assignment`, ce qui signifie ici qu' un objet de type `str` (*string*)
ne peut pas se voir affecté une modification.


!!! attention
    Il s'agit là encore d'une erreur que vous devez connaître et savoir corriger par vous-même.




## Exercice

Avec les lignes suivantes:

```
>>> a = "Bonjour "
>>> b = "les loulous."
>>> a = a + b
>>> a
'Bonjour les loulous.'
```

contredit-on ce que nous avons dit plus haut sur le fait qu'une chaîne est immuable?

??? solution "Réponse"
    La réponse est non. Il faut bien comprendre que la ligne `a = a + b` ne modifie pas l'objet chaîne de caractères `"Bonjour "`.
    Cette ligne `a = a + b` crée un nouvel chaîne puis lui accole une étiquette a.  
    
    Nous détaillons un peu ci-dessous ce processus.
    
    En ligne 1, nous créons l'objet "Bonjour ", puis nous lui accolons l'étiquette a.  
    a désigne l'objet de type str, de valeur 'bonjour':  
    ![](images/bonjour.png)
      
    En ligne 2, nous créons l'objet "les loulous." puis nous lui accolons l'étiquette b. 
    ![](images/bonjour2.png) 
    
    En ligne 3, nous concaténons les objets étiquetés a et b, c'est à dire nous fabriquons un nouvel 
    objet "Bonjour les loulous." puis nous lui accolons l'étiquette a.
    ![](images/bonjour3.png)
    
  
    
    A ce stade, l'objet "Bonjour " n'a plus d'étiquette (et n'est du coup plus accessible) mais l'objet
    "Bonjour " n'a pas été modifié: un nouvel objet a été créé et une étiquette a été décollée d'un objet pour
    être recollée sur un autre (ou en d'autres termes, la variable a ne désigne plus l'objet de départ de valeur
    "Bonjour", mais désigne un nouvel objet).
