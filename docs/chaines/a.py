

def repeter(ch, effectif):
    """
    ch -- de type str
    effectif -- entier, >0

    renvoie la chaîne constituée de effectif concaténation de ch.
    >>> repeter('*', 3)
    '***'
    >>> repeter('cou', 2)
    'coucou'
    """
    resultat = ''
    for k in range(effectif):
        resultat = resultat + ch
    return resultat




def etoile(n):
    """
    n -- entier naturel > 0

    renvoie une chaine triangle d'étoiles
    >>> print(etoile(3))
    *
    **
    ***
    >>> print(etoile(4))
    *
    **
    ***
    ****
    """
    ch = ''
    for i in range(1, n+1):
        ch = ch + repeter('*', i) + "\n"
    return ch


print(etoile(5))
