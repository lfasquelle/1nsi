# Affectation d'une variable





## Algorithme

Lorsque nous décrirons les algorithmes en pseudo-code, nous utiliserons la syntaxe suivante:

```
a ← 3
```

qui signifiera que la variable a reçoit la valeur 3.

Rappelons que dans l'écriture suivante:

```
a ← 3
a ← a + 6
```

on ne peut comprendre la seconde ligne qu'en se rappelant que, **dans une affectation, on commence toujours par l'expression qui 
se trouve à droite de la flèche** (et en programmation python: on commence par évaluer l'expression qui se trouve à droite du symbole `=`).

+ En ligne 1, la variable a reçoit la valeur 3.
+ En ligne 2, on effectue la partie droite: on calcule donc 3+6 = 9. Puis on affecte cette valeur 9 à a. 
a vaut ainsi 9 après la seconde ligne.


## En python

 

En python, affecter la valeur 3 à la variable `a` se code comme suit:

```python
a = 3
```

Python crée alors un objet de type int dont la valeur est 3.

Nous schématiserons cela ainsi: 

![](affectation/affectation01.png)



L'opération d'affectation `a ← a + 6` se code en python comme suit:

```python
a = a + 6
```

Ici encore, bien se souvenir que la lecture se fait en commençant par l'opération de droite.



!!! attention

    En mathématiques, l'écriture a = a + 3, dans laquelle a est un nom pour une constante est toujours une égalité fausse
    (ou une équation sans solution si a désigne une variable inconnue). Un nombre a 
    n'est pas égal à lui-même plus 3.  
    Dans cette écriture mathématique, la lettre `a` désigne le même objet des deux côtés de l'égalité.
     
    En langage python, l'écriture `a = a+3` se lit très différemment et la lettre `a` ne joue pas le même rôle des deux côtés de l'égalité.
    
    - on commence par lire la partie droite: `a+3`. Cette partie droite n'a de sens que si `a` est déjà un nom 
    attribué à un objet (de type numérique ici).
    On effectue alors l'opération `a+3` avec la valeur désignée par `a`.
    - Ensuite, on effectue la partie `a = ` qui consiste à dire que l'on va appeler `a` la valeur obtenue (on perd ainsi la valeur
    désignée auparavant).
    
    Une façon de dire cela est que
    
    - une étiquette se trouvant dans la partie droite d'une affectation est "en lecture seulement" (on lit la valeur associée
    à cette étiquette),
    - alors que l'étiquette en partie gauche est "en écriture"  (on écrit une nouvelle étiquette 
    que l'on attache à la valeur de la partie droite). 
    
    

### Deux affectations, une étiquette


Que se passe-t-il avec le code:

```python
a = 3
a = a + 6
```
 

+ En ligne 1, création d'un objet de type int recevant la valeur 3 et recevant une "étiquette" (un nom) `a`:   
![](affectation/affectation01.png)
+ En ligne 2, création d'un nouvel objet de type int recevant la valeur 3+6 puis recevant  l'étiquette `a`:  
![](affectation/affectation02.png)


Il sera important dans la suite de comprendre que toute affectation crée un nouvel objet (une nouvelle boîte dans
notre représentation): ce n'est pas la première boîte qui reçoit une nouvelle valeur. 


!!! note 
    Nous continuons à représenter la première boîte pour mieux saisir... 
    mais cette boîte ne porte plus d'étiquette: elle n'est donc plus accessible au programmeur, tout 
    se passe donc comme si elle n'existait plus.



### Deux affectations, deux  étiquettes


Si nous avions exécuté le code suivant:

```python
a = 3
b = a + 6
```

Le schéma aurait été:

+ En ligne 1, création d'un objet de type int recevant la valeur 3 et recevant une "étiquette" (un nom) `a`:   
![](affectation/affectation01.png)
+ En ligne 2, création d'un nouvel objet de type int recevant la valeur 3+6 puis recevant  l'étiquette `b`:  
![](affectation/affectation03.png)

Dans ce cas, les deux boîtes ont toujours une étiquette: elles sont encore toutes deux accessibles.


## Deux étiquettes, un seul objet

En python, lorsqu'il n'y a pas de calcul dans la partie droite de l'affectation mais seulement une étiquette (un nom
de variable), il n'y aura pas cette fois   création 
d'un nouvel objet mais simple ajout d'une étiquette à l'objet.

Le code:

```python
a = 3
b = a
```

se schématisera ainsi:

+ Ligne 1: création d'un objet de type int recevant la valeur 3 et l'étiquette a.  
![](affectation/affectation01.png)
+ Ligne 2: ajout d'une étiquette b sur l'objet existant.  
![](affectation/affectation04.png)

Nous  verrons que ce comportement sera important à comprendre pour des objets plus complexes 
que les int et les float (pour les chaînes, les listes, les tuples notamment).
