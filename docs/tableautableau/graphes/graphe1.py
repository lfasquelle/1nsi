

import matplotlib as mpl
import matplotlib.pyplot as plt
import networkx as nx

# https://networkx.github.io/documentation/stable/tutorial.html#creating-a-graph

G = nx.DiGraph()  
G.add_nodes_from([1, 2, 3, 4])
G.add_edges_from([(2, 1), (3, 1), (3,2), (3,4), (4,1), (4,2)])


# https://networkx.github.io/documentation/stable/reference/drawing.html#module-networkx.drawing.layout
pos = nx.layout.shell_layout(G)


# https://networkx.github.io/documentation/stable/reference/drawing.html
# https://matplotlib.org/3.1.0/api/markers_api.html
nodes = nx.draw_networkx_nodes(G, pos,  node_size=400, node_shape='s', node_color='white', edgecolors='black')
edges = nx.draw_networkx_edges(G,  pos,   arrowstyle='->', arrowsize=20, width=2)
 


labels = {}
 
labels[1] = r'$a$'
labels[2] = r'$b$'
labels[3] = r'$c$'
labels[4] = r'$d$'
 
nx.draw_networkx_labels(G,  pos, labels, font_size=20)
 
ax = plt.gca()
ax.set_axis_off()
plt.show()
