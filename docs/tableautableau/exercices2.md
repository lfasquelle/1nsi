# Quelques exercices 



## Exercice 1

&Eacute;crire une fonction `creeMatrice(n)` qui donne les résultats suivants:


```
creeMatrice(3) = [ [0, 1, 2]
                   [3, 4, 5]
                   [6, 7, 8]
                 ]
```

```
creeMatrice(4) = [ [0, 1, 2, 3]
                   [4, 5, 6, 7]
                   [8, 9, 10, 11]
                   [12, 13, 14, 15]
                 ]
```

De façon plus générale,   
la ligne 0 est constituée des entiers de 0 à n-1,  
la ligne 1 des entiers de n à 2n-1,   
la ligne 2 des entiers de 2n à 3n-1, ...

??? note solution "Un code possible"
    
    Regardons la colonne 1: 0, n, 2n, 3n ...   
    En résumé la colonne 1 contient les entiers $i\times n$ où i est le numéro de ligne.  
    Sur une ligne, on a: in, in+1, in+2, ..., in + (n-1). En bref in+j où j est le numéro de colonne.
    
    D'où le code:
    
    ```python
    def affichage(tab):
        """
        tab -- liste de listes de nombres
        
        affiche chaque liste en passant à la ligne entre les listes.
        """
        for ligne in tab:
            print(ligne)
        
            
            
    def creeMatrice(n):
        """
        n -- entier naturel non nul

        renvoie une matrice de n lignes, n colonnes (contenu défini dans l'énoncé)
        """
        return [ [i*n + j for j in range(n)] for i in range(n)]  
        
        
    affichage(creeMatrice(4))
    ```
    
    
     


## Exercice 2

On dispose de  matrices de taille n&times;n 
(créées par exemple avec `creeMatrice`, fonction définie dans l'exercice précédent).

On aimerait changer tous les éléments de la diagonale en 42.

Par exemple 

```
[ [0, 1, 2]
  [3, 4, 5]
  [6, 7, 8]
]
```

doit devenir

```
[ [42, 1, 2]
  [3, 42, 5]
  [6, 7, 42]
]
```

De même 

```
[ [0, 1, 2, 3]
  [4, 5, 6, 7]
  [8, 9, 10, 11]
  [12, 13, 14, 15]
]
```

doit devenir

```
[ [42, 1, 2, 3]
  [4, 42, 6, 7]
  [8, 9, 42, 11]
  [12, 13, 14, 42]
]
```

Ecrire une fonction python effectuant cette transformation d'une matrice n&times;n.


??? note solution "taille n*n"

    Comment récupérer n lorsqu'on dispose de la matrice A de taille n&times;n?
    
    De deux façons:
    
    + n est le nombre de listes internes, c'est à dire: `n = len(A)`.
    + n est aussi la longueur de chaque liste interne, par exemple `n = len(A[0])`.


??? note solution "Un code possible"

    Les éléments de la diagonale sont les éléments A[0][0], A[1][1], A[2][2]...
    En bref, ce sont les éléments A[i][i], pour i entre 0 et len(A)-1.


    Un code possible:
    
    ```python
    def affichage(tab):
        """
        tab -- liste de listes de nombres

        affiche chaque liste en passant à la ligne entre les listes.
        """
        for ligne in tab:
            print(ligne)



    def creeMatrice(n):
        """
        n -- entier naturel non nul

        renvoie une matrice de n lignes, n colonnes (cf exercice précédent)
        """
        return [ [i*n + j for j in range(n)] for i in range(n)]  


    def diagonale42(matrice):
        """
        matrice --  matrice n*n

        place des 42 sur la diagonale de matrice.
        """
        n = len(matrice)
        for i in range(n):
            matrice[i][i] = 42



    A = creeMatrice(4)
    diagonale42(A)

    affichage(A)
    ```











## Exercice 3

On dispose de  matrices de taille n&times;n 
(créées par exemple avec `creeMatrice`, fonction définie dans l'exercice précédent).

On aimerait changer tous les éléments de la ligne numéro k en 42.

Par exemple, la modification de la ligne 0 de la matrice suivante: 

```
[ [0, 1, 2]
  [3, 4, 5]
  [6, 7, 8]
]
```

doit transformer cette matrice en:

```
[ [42, 42, 42]
  [3, 4, 5]
  [6, 7, 8]
]
```

De même, la modification de la ligne 1 de la matrice suivante: 

```
[ [0, 1, 2, 3]
  [4, 5, 6, 7]
  [8, 9, 10, 11]
  [12, 13, 14, 15]
]
```

doit transformer cette matrice en:


```
[ [0, 1, 2, 3]
  [42, 42, 42, 42]
  [8, 9, 10, 11]
  [12, 13, 14, 15]
]
```

&Eacute;crire une fonction python effectuant cette transformation d'une matrice n&times;n.


??? solution "Un code"


    ```python
    def affichage(tab):
        """
        tab -- liste de listes de nombres

        affiche chaque liste en passant à la ligne entre les listes.
        """
        for ligne in tab:
            print(ligne)



    def creeMatrice(n):
        """
        n -- entier naturel non nul

        renvoie une matrice de n lignes, n colonnes (cf exercice précédent)
        """
        return [ [i*n + j for j in range(n)] for i in range(n)]  


    def changeLigne42(matrice, numero_ligne):
        """
        matrice --  matrice n*n

        place des 42 sur la   ligne numero_ligne de la matrice
        """
        n = len(matrice)
        for i in range(n):
            matrice[numero_ligne][i] = 42



    A = creeMatrice(4)
    changeLigne42(A, 1)

    affichage(A)
    ```








## Exercice 4

On dispose de  matrices de taille n&times;n 
(créées par exemple avec `creeMatrice`, fonction définie dans l'exercice précédent).

On aimerait changer tous les éléments de la **colonne** numéro k en 42.

&Eacute;crire une fonction python effectuant cette transformation d'une matrice n&times;n.


??? solution "Un code"


    ```python
    def affichage(tab):
        """
        tab -- liste de listes de nombres

        affiche chaque liste en passant à la ligne entre les listes.
        """
        for ligne in tab:
            for element in ligne:
                print(element, end="\t") # \t signifie une tabulation après l'élément
            print()



    def creeMatrice(n):
        """
        n -- entier naturel non nul

        renvoie une matrice de n lignes, n colonnes (cf exercice précédent)
        """
        return [ [i*n + j for j in range(n)] for i in range(n)]  


    def changeColonne42(matrice, numero_colonne):
        """
        matrice --  matrice n*n

        place des 42 sur la colonne numero_colonne de la matrice
        """
        n = len(matrice)
        for i in range(n):
            matrice[i][numero_colonne] = 42



    A = creeMatrice(4)
    changeColonne42(A, 2)

    affichage(A)
    ```
