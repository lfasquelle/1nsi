# Réseau routier

 

## Exercice 


!!! note
    Dans cet exercice 1, on met en place les conventions utilisées pour les exercices suivants.

Le schéma ci-dessous représente un réseau de routes entre des villes.

![réseau routier 1](graphes/routes1.png){width=500px}

Les flèches signifient que les routes sont à sens unique.


On représente les liaisons entre villes par la liste de listes (matrice) ci-dessous:

```python
routes = [ [0, 0, 0, 0],
           [1, 0, 0, 0],
           [1, 1, 0, 1],
           [1, 1, 0, 0]
         ]
```

Cette liste de listes est à interpréter comme suit: 
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-zv3q{font-weight:bold;background-color:#fffe65;color:#3531ff;border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-vi0c{font-weight:bold;background-color:#fffe65;color:#3166ff;border-color:inherit;text-align:center;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-c3ow"></th>
    <th class="tg-zv3q">a</th>
    <th class="tg-zv3q">b</th>
    <th class="tg-zv3q">c</th>
    <th class="tg-zv3q">d</th>
  </tr>
  <tr>
    <td class="tg-vi0c">a</td>
    <td class="tg-c3ow">0</td>
    <td class="tg-c3ow">0</td>
    <td class="tg-c3ow">0</td>
    <td class="tg-c3ow">0</td>
  </tr>
  <tr>
    <td class="tg-vi0c">b</td>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">0</td>
    <td class="tg-c3ow">0</td>
    <td class="tg-c3ow">0</td>
  </tr>
  <tr>
    <td class="tg-vi0c">c</td>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">0</td>
    <td class="tg-c3ow">1</td>
  </tr>
  <tr>
    <td class="tg-vi0c">d</td>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">0</td>
    <td class="tg-c3ow">0</td>
  </tr>
</table>


La "ligne" c, "colonne" a est marquée d'un 1: cela traduit le fait qu'il y a une route allant de c vers a.  
La ligne a, colonne c est marquée d'un 0: il n'y a pas de route de a vers c.

### A vous

Donner le code de la liste de listes correspondant au réseau routier ci-dessous (on prend les villes 
dans l'ordre alphabétique comme ci-dessus).

![réseau routier 2](graphes/routes2.png)

!!! attention
    Dans ce réseau, certaines routes n'ont pas de flèches: cela signifie qu'elles sont à double-sens.
    
    On placera donc un 1 par exemple de A vers B mais aussi de B vers A.


??? solution "Réponse"

    <style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-9cr7{font-weight:bold;background-color:#fffe65;color:#3531ff;text-align:center;vertical-align:top}
    .tg .tg-zv3q{font-weight:bold;background-color:#fffe65;color:#3531ff;border-color:inherit;text-align:center;vertical-align:top}
    .tg .tg-g79i{background-color:#fffe65;color:#3166ff;text-align:center;vertical-align:top}
    .tg .tg-baqh{text-align:center;vertical-align:top}
    .tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
    .tg .tg-vi0c{font-weight:bold;background-color:#fffe65;color:#3166ff;border-color:inherit;text-align:center;vertical-align:top}
    </style>
    <table class="tg">
      <tr>
        <th class="tg-c3ow"></th>
        <th class="tg-zv3q">A</th>
        <th class="tg-zv3q">B</th>
        <th class="tg-zv3q">C</th>
        <th class="tg-zv3q">D</th>
        <th class="tg-9cr7">E</th>
      </tr>
      <tr>
        <td class="tg-vi0c">A</td>
        <td class="tg-c3ow">0<br></td>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-vi0c">B</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-vi0c">C</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-baqh">0</td>
      </tr>
      <tr>
        <td class="tg-vi0c">D</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-c3ow">1</td>
        <td class="tg-c3ow">0</td>
        <td class="tg-baqh">1</td>
      </tr>
      <tr>
        <td class="tg-g79i">E</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
        <td class="tg-baqh">0</td>
      </tr>
    </table>
    
    La liste de listes python:
    
    ```python
    routes = [ [0, 1, 1, 0, 1],
               [1, 0, 0, 0, 0],
               [1, 0, 0, 0, 0],
               [0, 0, 1, 0, 1],
               [0, 1, 0, 0, 0]
             ]
    ```














## Exercice 


&Eacute;crire une fonction python prenant en entrée une liste de listes comme ci-dessus représentant un réseau routier,
ainsi que la liste des noms des villes de ce réseau routier
et renvoyant la liste des routes.

Une route allant de A vers B sera représentée par le tuple ('A','B').

```python
def les_routes(reseau, villes):
    """
    reseau -- matrice traduisant la présence de routes 
    villes -- liste des noms des villes (même ordre que pour la matrice)
    
    renvoie la liste des routes 
    sous la forme de tuples (ville d'origine, ville d'arrivée)
    """
```

  
!!! example "Exemple"
    Pour le deuxième réseau routier de l'exercice précédent:
    
    + l'entrée sera:
    
    la matrice traduisant les routes:  
    ```python
    [  [0, 1, 1, 0, 1],
       [1, 0, 0, 0, 0],
       [1, 0, 0, 0, 0],
       [0, 0, 1, 0, 1],
       [0, 1, 0, 0, 0]
    ]
    ```
    et la liste des noms des villes reliées par ces routes:  
    ['A', 'B', 'C', 'D', 'E']  
    
    + la liste  obtenue en sortie contiendra tous les couples ('A','B'), ('B', 'A'), ('A','E'), ('A','C'), ('C','A') ...
    qui traduisent l'existence d'une route entre les deux villes du couple.
    
    
    
??? solution "Un code"
    
    [Ce fichier ipynb](graphes/routes.ipynb) contient un code possible.
    
    [Version statique html](graphes/routes.html).
    
    
## Exercice 


&Eacute;crire une fonction python prenant en entrée une matrice représentant un réseau routier,
ainsi que la liste des noms des villes de ce réseau routier
et renvoyant la liste des routes à sens unique.

La fonction demandée est donc assez proche de celle de l'exercice  précédent mais la liste obtenue en sortie ne doit pas contenir
les routes à double-sens.


 
??? solution "Un code"
    
    [Ce fichier ipynb](graphes/sensunique.ipynb) contient un code possible.
    
    [Version statique html](graphes/sensunique.html) 
