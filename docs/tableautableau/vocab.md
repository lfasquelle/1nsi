# Les tableaux de tableaux
 
 
 

## Tableau de tableaux

Un tableau de tableaux est un tableau dont les éléments sont des tableaux.

Par exemple, `t = [ [1,2,3], [4,5,6], [7,8,9] ]`.
Ce tableau t est constitué des tableaux:

+ `t[0] = [1,2,3]`
+ `t[1] = [4,5,6]`
+ `t[2] = [7,8,9]`


Pour les tableaux composants le tableau, on parlera de tableaux internes.
Ainsi le tableau t a pour tableaux internes t[0], t[1], t[2].


!!! note 
    Dans toutes les situations que nous rencontrerons dans ce cours, les tableaux internes auront 
    tous **la même longueur**.


## Matrice


Pour un tableau de tableaux, on parle aussi de 
[matrice](https://fr.wikipedia.org/wiki/Matrice_(math%C3%A9matiques)).
Chaque tableau interne pourra être appelé ligne de la matrice.
Et les éléments de même indice dans les différentes lignes constituent une
colonne de la matrice.

Le tableau, donné en exemple ci-dessus, peut ainsi être représenté en lignes et colonnes:  
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-baqh{text-align:center;vertical-align:top}
.tg .tg-nrix{text-align:center;vertical-align:middle}
</style>
<table class="tg">
  <tr>
    <th class="tg-nrix">1</th>
    <th class="tg-baqh">2</th>
    <th class="tg-baqh">3</th>
  </tr>
  <tr>
    <td class="tg-baqh">4</td>
    <td class="tg-baqh">5</td>
    <td class="tg-baqh">6</td>
  </tr>
  <tr>
    <td class="tg-baqh">7</td>
    <td class="tg-baqh">8</td>
    <td class="tg-baqh">9</td>
  </tr>
</table>

On parle également de tableau à 2 dimensions.



!!! note
    Par opposition, les tableaux à **une dimension** sont aussi appelés **vecteurs**.
    


## Matrice carrée


On appellera matrice carrée une matrice dont le nombre de lignes est égal 
au nombre de colonnes, c'est à dire un tableau de tableaux dont le nombre de tableaux internes
est égal à la longueur de chacun de ces tableaux internes.

+ Le tableau `t = [ [1,2], [3,4] ]` est une matrice carrée (2 "lignes" `[1,2]` et `[3,4]` et  2 "colonnes").
+ Le tableau `t = [ [1,2,3], [4, 5,6] ]` est une matrice non carrée (2 "lignes" mais 3 "colonnes").
