# Exercices




## Exercice 1

+ En utilisant le module [random](https://docs.python.org/fr/3/library/random.html?highlight=random#module-random),
générer aléatoirement le contenu d'une matrice de NB_LIGNES lignes et NB_COLONNES colonnes (où NB_LIGNES et NB_COLONNES 
sont des constantes que vous définirez au préalable).
+ &Eacute;crire ensuite une fonction d'affichage de votre matrice, une ligne écran correspondant à une ligne de la matrice.
On fera en sorte d'aligner proprement les nombres lors de l'affichage. 


??? note solution "Aide: aligner les affichages"
    
    Avec print, on peut espacer régulièrement les éléments affichés en ajoutant une tabulation après
    chaque affichage.
    
    ```python
    for i in range(1,5):
        for j in range(1,5):
            print(i*j, end='\t') # ajout d'une tabulation en fin de print
        print()
    ```
    
    donne:
    
    ```
    1	2	3	4	
    2	4	6	8	
    3	6	9	12	
    4	8	12	16
    ```
    
    La valeur par défaut de `end` est  `'\n'`, ce qui explique que par défaut on passe à la ligne
    après chaque instruction `print` lorsqu'on ne renseigne pas explicitement `end`.
    
    
    
??? note solution "création de la matrice"


    Un code possible:
    
    ```python
    from random import randint


    NB_LIGNES = 4
    NB_COLONNES = 5

    A = [[randint(0,100) for j in range(NB_COLONNES)] for i in range(NB_LIGNES)]
    ```

??? note solution "Avec affichage"

    ```python
    from random import randint


    NB_LIGNES = 4
    NB_COLONNES = 5

    A = [[randint(0,100) for j in range(NB_COLONNES)] for i in range(NB_LIGNES)]

    def affichage(matrice):
        """
        matrice -- matrice de nombres
        
        Affiche la matrice une ligne écran pour chaque ligne matrice.
        """
        for ligne in matrice:
            for valeur in ligne:
                print(valeur, end="\t") # une tabulation après chaque valeur
            print() # on passe à la ligne sur l'écran à la fin de chaque ligne matrice 


    affichage(A)
    ```









## Exercice 2


&Eacute;crire un corps possible pour la fonction suivante:

```python
def maxi(matrice):
    """
    matrice -- matrice (non nécessairement carrée) d'entiers
    
    renvoie l'élément de valeur maximale de matrice.
    """
```

??? note solution "Un code possible"

    ```python
    def affichage(matrice):
        """
        matrice -- matrice de nombres

        Affiche la matrice une ligne écran pour chaque ligne matrice.
        """
        for ligne in matrice:
            for valeur in ligne:
                print(valeur, end="\t") # une tabulation après chaque valeur
            print() # on passe à la ligne sur l'écran à la fin de chaque ligne matrice 



    def maxim(vecteur):
        """
        vecteur -- liste de nombres

        renvoie l'élément de valeur maximale de la liste vecteur
        """
        m = vecteur[0]
        for element in vecteur:
            if element > m:
                m = element
        return m



    def maxi(matrice):
        """
        matrice -- matrice (non nécessairement carrée) d'entiers

        renvoie l'élément de valeur maximale de matrice.
        """
        # on crée la liste des max de chaque ligne de la matrice:
        liste_des_max = [maxim(ligne) for ligne in matrice]
        # on renvoie le max des max par ligne:
        return maxim(liste_des_max)





    from random import randint


    NB_LIGNES = 4
    NB_COLONNES = 5

    A = [[randint(0,100) for j in range(NB_COLONNES)] for i in range(NB_LIGNES)]

    affichage(A)
    print("Elément maximal de la matrice: ", maxi(A))
    ```

??? note solution "Un second code"

    ```python
    from random import randint


    NB_LIGNES = 4
    NB_COLONNES = 5

    A = [[randint(0,100) for j in range(NB_COLONNES)] for i in range(NB_LIGNES)]

    def affichage(matrice):
        """
        matrice -- matrice de nombres
        
        Affiche la matrice une ligne écran pour chaque ligne matrice.
        """
        for ligne in matrice:
            for valeur in ligne:
                print(valeur, end="\t") # une tabulation après chaque valeur
            print() # on passe à la ligne sur l'écran à la fin de chaque ligne matrice 

      


    def maxi(matrice):
        """
        matrice -- matrice (non nécessairement carrée) d'entiers

        renvoie l'élément de valeur maximale de matrice.
        """
        m = matrice[0][0]
        for ligne in matrice:  # pour chaque liste interne
            for element in ligne: # pour chaque élément de la liste interne 
                if element > m:
                    m = element
        return m
            
            

    affichage(A)
    print("Elément maximal de la matrice: {}.".format(maxi(A)))
    ```
    
    
    
    
??? note solution "Un autre code"

    ```python
    from random import randint


    NB_LIGNES = 4
    NB_COLONNES = 5

    A = [[randint(0,100) for j in range(NB_COLONNES)] for i in range(NB_LIGNES)]

    def affichage(matrice):
        """
        matrice -- matrice de nombres
        
        Affiche la matrice une ligne écran pour chaque ligne matrice.
        """
        for ligne in matrice:
            for valeur in ligne:
                print(valeur, end="\t") # une tabulation après chaque valeur
            print() # on passe à la ligne sur l'écran à la fin de chaque ligne matrice 

      

    def maxi(matrice):
        """
        matrice -- matrice (non nécessairement carrée) d'entiers
        
        renvoie l'élément de valeur maximale de matrice.
        """
        m = matrice[0][0]
        nb_lignes = len(matrice)
        nb_colonnes = len(matrice[0])
        for i in range(nb_lignes):
            for j in range(nb_colonnes):
                if matrice[i][j] > m:
                    m = matrice[i][j]
        return m
        
        

    affichage(A)
    print(f"Elément maximal de la matrice: {maxi(A)}.")
    ```








## Exercice 3

&Eacute;crire une fonction prenant en paramètre une matrice carrée
et modifiant les valeurs de la diagonale (représentée ci-dessous en pointillés)
par des 42.

![](images/diagonaler.png)


??? solution "Aide"

    On cherche à obtenir une "formule" simple pour les coordonnées des cellules de la diagonale
    qui nous intéresse ici.
    
    Il s'agit d'une équation de droite... On cherche un lien affine entre le numéro de colonne j et le
    numéro de ligne i.
    
    On trouve facilement que j = n-1-i  (testez!)

??? note solution "Un code"

    ```python
    from random import randint


    NB_LIGNES = 5
    NB_COLONNES = 5



    def affichage(matrice):
        """
        matrice -- matrice de nombres
        
        Affiche la matrice une ligne écran pour chaque ligne matrice.
        """
        for ligne in matrice:
            for valeur in ligne:
                print(valeur, end="\t") # une tabulation après chaque valeur
            print() # on passe à la ligne sur l'écran à la fin de chaque ligne matrice 

      
    def diagonale42(matrice):
        """
        matrice -- matrice carrée
        
        remplace les éléments de la diagonale principale / par des 42.
        """
        n = len(matrice) # n = nombre de lignes = nombre de colonnes
        for i in range(n):
            matrice[i][n-1-i] = 42 
        
     
    A = [[randint(0,100) for j in range(NB_COLONNES)] for i in range(NB_LIGNES)]
    affichage(A)
    print()
    diagonale42(A)
    affichage(A)
    ```
     
