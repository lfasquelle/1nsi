# Carré magique


!!! attention
    Page facultative, exercices d'entraînement.

L'exemple suivant est extrait [d'une page wikipedia](https://fr.wikipedia.org/wiki/Carr%C3%A9_magique_(math%C3%A9matiques)):  
![](images/carre33.png)

La définition de carré magique donné [sur cette même page](https://fr.wikipedia.org/wiki/Carr%C3%A9_magique_(math%C3%A9matiques)) est la suivante:  
un carré magique d’ordre n est composé de n<sup>2</sup> entiers strictement positifs distincts, 
écrits sous la forme d’un tableau carré. 
Ces nombres sont disposés de sorte que les sommes sur chaque rangée, sur chaque colonne et 
sur chaque diagonale principale soient égales. 
On nomme alors constante magique   la valeur de ces sommes. 


Un exemple de carré magique 4&times;4 extrait de la même page:

![](images/carre44.png)

## Exercice 


On dispose de "carrés" sous la forme de matrices carrées:

```python
A = [ [2, 7, 6],
      [9, 5, 1],
      [4, 3, 8]
    ]
```


```python
B = [ [4, 14, 15, 1],
      [9, 7, 6, 12],
      [5, 11, 10, 8],
      [16, 2, 3, 13]
    ]
```


Votre mission: écrire un corps possible pour la fonction ci-dessous.

```python
def est_magique(matrice):
    """
    matrice -- matrice carrée d'entiers
    
    renvoie True si matrice est un carré magique, False sinon.
    """
```


??? solution "Un code possible"

    Un code possible sur [cette feuille ipynb](fichiers/carre_magique.ipynb).
    
    [Version statique html](fichiers/carre_magique.html):
    
    <iframe src="fichiers/carre_magique.html" 
     width="100%" 
     height="500" 
     style="border:2px solid orange">
    </iframe> 
    
    


