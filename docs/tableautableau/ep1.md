# Epreuve pratique



L'une des épreuves de terminale en spécialité NSI est une épreuve pratique (vous êtes devant un ordinateur et devez coder des programmes).   
Une partie des exercices lors de cette épreuve se limite au programme de première et vous pouvez donc d'ores et déjà vous y entraîner.



## Exercice (sujet 26 des EP 2021, exercice 2)

On considère une *image* en 256 niveaux de gris que l’on représente par une grille de nombres, c’est-à-dire une liste composée de listes (que l'on appellera
listes internes) toutes de longueurs identiques.

La largeur de l’image est donc la longueur d’une  liste  interne et la hauteur de l’image est le nombre de  listes internes.

Chaque  liste interne représente une ligne de l’image et chaque élément d'une liste interne est un entier compris entre 0 et 255, représentant le niveau de gris du pixel
(0 = noir, 255 = blanc).

Compléter le programme ci-dessous :


```python
def nbLig(image):
    '''
    renvoie le nombre de lignes de l'image
    '''
    return ...

def nbCol(image):
    '''
    renvoie la largeur de l'image
    '''
    return ...

 
    
def negatif(image):
    '''
    renvoie le négatif de l'image sous la forme d'une liste de listes,
    c'est à dire une copie du tableau image 
    dans laquelle chaque entier a été remplacé par son complément à 255.
    '''
    
    hauteur = nbLig(image)  
    largeur = nbCol(image)
    
    # on crée une "image" de 0 aux mêmes dimensions que l'argument image :
    L = [[0 for k in range(largeur)] for i in range(hauteur)]  
    
    for i in range(hauteur):
        for j in range(...............):
            L[i][j] = .....................
    return L


def binaire(image, seuil):
    '''
    renvoie une image "binarisée" de l'image sous la forme d'une liste de listes.
    où chaque entier de l'argument image a été remplacé par 0 
    si sa valeur était strictement inférieure au seuil 
    et remplacé par 1 sinon.
    '''
    
    hauteur = nbLig(image)  
    largeur = nbCol(image)
    
    # on crée une "image" de 0 aux mêmes dimensions que l'argument image : 
    L = [[0 for k in range(largeur)] for i in range(hauteur)]  

    for i in range(hauteur):
        for j in range(...):
            if image[i][j] < ... :
                L[i][j] = ...
            else:
                L[i][j] = ...
    return L
```
  
Exemple:

```
>>> img=[ [20, 34, 254, 145, 6], 
		  [23, 124, 237, 225, 69], 
		  [197, 174, 207, 25, 87], 
		  [255, 0, 24, 197, 189]
		 ]
>>> nbLig(img)
4
>>> nbCol(img)
5
>>> negatif(img)
[ [235, 221, 1, 110, 249], 
  [232, 131, 18, 30, 186], 
  [58, 81, 48, 230, 168], 
  [0, 255, 231, 58, 66] ]
>>> binaire(img,120)
[ [0, 0, 1, 1, 0], 
  [0, 1, 1, 1, 0], 
  [1, 1, 1, 0, 0], 
  [1, 0, 0, 1, 1] ]
```  
  
  

??? solution 

	```python
	def nbLig(image):
		'''
		renvoie le nombre de lignes de l'image
		'''
		return len(image)

	def nbCol(image):
		'''
		renvoie la largeur de l'image
		'''
		return len(image[0])


	def negatif(image):
		'''
		renvoie le négatif de l'image sous la forme d'une liste de listes,
		c'est à dire une copie du tableau image 
		dans laquelle chaque entier a été remplacé par son complément à 255.
		'''
		
		hauteur = nbLig(image)  
		largeur = nbCol(image)
		
		# on crée  une "image" de 0 aux mêmes dimensions que l'argument image :
		L = [[0 for k in range(largeur)] for i in range(hauteur)]  
		
		for i in range(hauteur):
			for j in range(largeur):
				L[i][j] = 255 - image[i][j]
		return L


	def binaire(image, seuil):
		'''
		renvoie une image "binarisée" de l'image sous la forme d'une liste de listes.
		où chaque entier de l'argument image a été remplacé par 0 
		si sa valeur était strictement inférieure au seuil 
		et remplacé par 1 sinon.
		'''
		
		hauteur = nbLig(image)  
		largeur = nbCol(image)
		# on crée une "image" de 0 aux mêmes dimensions que l'argument image : 
		L = [[0 for k in range(largeur)] for i in range(hauteur)]  

		for i in range(hauteur):
			for j in range(largeur):
				if image[i][j] < seuil :
					L[i][j] = 0
				else:
					L[i][j] = 1
		return L
	```
 
 
