# Premières manipulations de tableaux de tableaux



## Modifier un élément ou une ligne 

On définit un tableau de tableaux en python de la façon suivante:

```python
A = [ [1,2,3],
      [4,5,6],
      [7,8,9]
    ]
```


!!! note
    Les tableaux, type abstrait de données (TAD), seront implémentés en python par des listes.
    On parlera donc dans dans la suite, souvent indifféremment, de tableau (TAD) ou de liste (Type python).





+ Modifiez l'élément de valeur 2 par la valeur  42.

??? note solution "Solution"

    A est constitué des tableaux: A[0], A[1], A[2].
    
    Dans l'interpréteur python:
    
    ```
    >>> A = [ [1,2,3],
    ...       [4,5,6],
    ...       [7,8,9]
    ...     ]
    >>> A[0]
    [1, 2, 3]
    >>> A[1]
    [4, 5, 6]
    >>> A[2]
    [7, 8, 9]
    ```

    L'élément de valeur 2 est l'élément d'indice 1 dans A[0]. On y accède donc par A[0][1].
    
    Dans l'interpréteur python:
    
    ```
    >>> A = [ [1,2,3],
    ...       [4,5,6],
    ...       [7,8,9]
    ...     ]
    >>> A[0][1]
    2
    >>> A[0][1] = 42
    >>> A
    [[1, 42, 3], [4, 5, 6], [7, 8, 9]]
    ```



+ Remplacez le second tableau (interne) par [666, 69, 96].



??? note solution "Solution"

    Le tableau à modifier est A[1].
    
    Dans l'interpréteur python:
    
    ```
    >>> A = [ [1,2,3],
    ...       [4,5,6],
    ...       [7,8,9]
    ...     ]
    >>> A[1] = [666, 69, 96]
    >>> A
    [[1, 2, 3], [666, 69, 96], [7, 8, 9]]
    ```








## Afficher une matrice

Dans un fichier python:

```python
A = [ [1, 2, 3], [4, 5, 6], [7, 8, 9]]

print(A)
```

Le résultat:

```
[[1, 2, 3], [4, 5, 6], [7, 8, 9]]
```

On aimerait un affichage où chaque liste interne soit affichée sur une ligne distincte:

```
[1, 2, 3]
[4, 5, 6]
[7, 8, 9]
```

Ecrire une fonction python `affichageParLigne(tab)` pour réaliser ce type d'affichage.

??? solution 

    Un script possible:
    
    ```python
    def affichage(tab):
        """
        tab -- liste de listes de nombres
        
        affiche chaque liste en passant à la ligne entre les listes.
        """
        for ligne in tab:
            print(ligne)
        
            
            
    A = [ [1, 2, 3], [4, 5, 6], [7, 8, 9]]

    B = [ [i for i in range(5)],  
          [i*i for i in range(5)], 
          [i*i*i for i in range(5)], 
          [i**4 for i in range(5)], 
          [i**5 for i in range(5)]
        ]
        
    print("Affichage de A: ")
    affichage(A)
    print()
    print("Affichage de B: ")
    affichage(B)
    ```

??? solution "Améiorer l'affichage dans chaque ligne"

    La proposition précédente n'est pas complétement satisfaisante car les nombres d'une même colonne
    ne sont pas alignés sur une même verticale.         
    Si l'on demande par exemple l'affichage de `A = [ [1, 2],  [42, 666]]`, on obtient:
    
    ```
    [1, 2]
    [42, 666]
    ```
    on constate que 666 n'est pas bien aligné avec 2.
    
    Pour améliorer cela, on peut utiliser 
    certaines options de la fonction [`print`](https://docs.python.org/fr/3/library/functions.html#print).
    
    Le script ci-dessous:
    
    ```python
    def affichage(tab):
        """
        tab -- liste de listes de nombres

        affiche chaque liste en passant à la ligne entre les listes.
        """
        for ligne in tab:
            for element in ligne:
                print(element, end="\t")
            print()
    ```
    
    permet d'insérer une tabulation après l'affichage de chaque valeur (avec `end = "\t"`).
    Et lorsqu'une ligne est terminée, on insère un passage à la ligne (avec `print()`).
    
    L'affichage obtenu pour la matrice `A = [ [1, 2],  [42, 666]]` est alors:
    
    ```
    1   2   
    42  666 
    ```
    
    !!! note  
        Par défaut, le paramètre `end` lors d'un appel de la fonction `print` a pour valeur `\n`. C'est ce qui explique
        que, si l'on omet ce paramètre, on passe à la ligne après chaque print. Le fait de donner ici la valeur `\t` à ce 
        paramètre `end` permet d'insérer une tabulation plutôt qu'un passage à la ligne.
        
    !!! note
        L'amélioration proposée n'est encore pas optimale. Essayez par exemple d'afficher 
        `A = [ [1000000000000, 2],  [42, 666]]`... que se passe-t-il ?
       


## Rechercher un élément de valeur maximale


&Eacute;crire un corps possible pour la fonction suivante:

```python
def maxi(matrice):
    """
    matrice -- matrice (non nécessairement carrée) d'entiers
    
    renvoie l'élément de valeur maximale de matrice.
    """
```

??? note solution "Un code possible"

    On a découpé la recherche en deux étapes:
    
    - on commence par écrire une fonction (déjà écrite dans le cours sur les listes) qui renvoie le maximum d'une liste (tableau
    à 1 dimension).
    - on écrit ensuite une fonction qui va renvoyer le maximum parmi les maximums de chaque ligne.
    
    
    ```python
    def affichage(matrice):
        """
        matrice -- matrice de nombres

        Affiche la matrice une ligne écran pour chaque ligne matrice.
        """
        for ligne in matrice:
            for valeur in ligne:
                print(valeur, end="\t") # une tabulation après chaque valeur
            print() # on passe à la ligne sur l'écran à la fin de chaque ligne matrice 



    def maxim(vecteur):
        """
        vecteur -- liste de nombres (non vide)

        renvoie l'élément de valeur maximale de la liste vecteur.
        
        >>> maxim([42, 2, 666, 7])
        666
        >>> maxim([42])
        42
        """
        m = vecteur[0]
        for element in vecteur:
            if element > m:
                m = element
        return m



    def maxi(matrice):
        """
        matrice -- matrice (non nécessairement carrée) d'entiers

        renvoie l'élément de valeur maximale de matrice.
        
        >>> maxi([ [42, 7], [6, 666] ])
        666
        >>> maxi([ [42, 7], [6, 666], [1789, 0] ])
        1789
        """
        # on crée la liste des max de chaque ligne de la matrice:
        liste_des_max = [maxim(ligne) for ligne in matrice]
        # on renvoie le max des max par ligne:
        return maxim(liste_des_max)



    # un test
    
    # on commence par afficher une matrice créée aléatoirement: 
    from random import randint
    NB_LIGNES = 4
    NB_COLONNES = 5
    A = [[randint(0,100) for j in range(NB_COLONNES)] for i in range(NB_LIGNES)]
    affichage(A)
    
    # on calcule maintenant la valeur maximale:
    print("Elément maximal de la matrice: ", maxi(A))
    
    
    # lancement des doctests:
    import doctest
    doctest.testmod(verbose = True)
    ```

