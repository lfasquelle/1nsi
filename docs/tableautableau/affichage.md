# Affichage d'une grille



 

## Affichage texte


Dans cet [autre fichier](affichage/affiche_texte.py), on choisit d'afficher la grille en console.

Lisez le texte de la fonction, essayez d'en comprendre l'essentiel et enfin utilisez ce fichier depuis un autre fichier python
pour afficher une matrice.


??? note "texte du fichier .py"

    ```python
    def afficher(tab, taille_case=3):
        """
        tab - tableau 2D (liste de listes d'entiers ou de string)
        taille_case - entier
        Sortie: None 
        Affichage du tableau en console  
        où chaque case est "large" de taille_case caractères
        Les numéros des lignes s'affichent à gauche du plateau
        Les numéros des colonnes s'affichent au-dessus du plateau
        """

        def ligne_to_str(ligne, numero_ligne):
            """
            renvoie une chaîne de caractères représentant une ligne 
            de la grille
            """
            representation_ligne = "  |" + ((" "*(taille_case+2) + "|")*nb_colonnes) + "\n"

            representation_ligne += str(numero_ligne) + " |" 
            for element in ligne:
                element = str(element)
                while len(element) < taille_case:
                    element = " " + element
                    if len(element) < taille_case: element = element + " "
                representation_ligne += " "+element + " |" 
            representation_ligne += "\n"
            representation_ligne += "  |" + (("_"*(taille_case+2) + "|")*nb_colonnes)  + "\n"
            return representation_ligne


        # affichage des numéros de colonnes:
        nb_colonnes = len(tab[0])
        representation = "  "
        for i in range(0, nb_colonnes):
            num = str(i)
            while len(num) < taille_case+2:
                num = " " + num
                if len(num) < taille_case+2: num = num + " "
            representation += " " + num 
        representation += "\n"

        # représentation des lignes
        representation += "   " + "_" * (nb_colonnes*(taille_case+3)-1) + "\n"
        for numero_ligne, ligne in enumerate(tab):
            representation += ligne_to_str(ligne, numero_ligne)

        print(representation)
    ```

??? solution 

    Une utilisation:
    
    ```python
    from affiche_texte import afficher
    matrice = [ [ i-j for j in range(5)] for i in range(7)]
    afficher(matrice)
    ```
    
    


 


