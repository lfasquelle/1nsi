# Exercices


Deux exercices de création de matrices.



## Exercice 

&Eacute;crire une fonction `creeMatrice(n)` qui donne les résultats suivants:


```
creeMatrice(3) = [ [0, 1, 2]
                   [3, 4, 5]
                   [6, 7, 8]
                 ]
```

```
creeMatrice(4) = [ [0, 1, 2, 3]
                   [4, 5, 6, 7]
                   [8, 9, 10, 11]
                   [12, 13, 14, 15]
                 ]
```

De façon plus générale:
  
+ la ligne 0 est constituée des n premiers entiers naturels (de 0 à $n-1$),  
+ la ligne suivante est constituée des n entiers suivants (de $n$ à $n + (n-1)$),   
+ la ligne suivante est constituée à nouveau des n entiers suivants  (de $2n$ à $2n + (n-1)$)
+ ...

??? note solution "Aide"

    M étant la matrice à créer, regardons la première colonne (indice 0), elle contient : 0, n, 2n, 3n ...
    On aura donc `M[0][0] = 0*n`,  `M[1][0] = 1*n`, `M[2][0] = 2*n`,    `M[3][0] = 3*n`, ...   
    En résumé `M[i][0] = i*n` où i est le numéro de ligne.  
    
    Sur une ligne d'indice i, on trouve les nombres: in, in+1, in+2, ..., in + (n-1). 
    En résumé, `M[i][j] = i*n+j`. 
    
    Il vous reste à créer des boucles python pour créer cette matrice.
    
    

??? note solution "Un code possible"
    
    ```python
    def affichage(tab):
        """
        tab -- liste de listes de nombres
        
        affiche chaque liste en passant à la ligne entre les listes.
        """
        for ligne in tab:
            print(ligne)
        
            
            
    def creeMatrice(n):
        """
        n -- entier naturel non nul

        renvoie une matrice de n lignes, n colonnes (contenu défini dans l'énoncé)
        """
        return [ [i*n + j for j in range(n)] for i in range(n)]  
        
        
    affichage(creeMatrice(4))
    ```
    
    
     





## Exercice  

+ En utilisant le module [random](https://docs.python.org/fr/3/library/random.html?highlight=random#module-random),
générer aléatoirement le contenu d'une matrice de NB_LIGNES lignes et NB_COLONNES colonnes (où NB_LIGNES et NB_COLONNES 
sont des constantes que vous définirez au préalable).

   
??? solution

    Un code possible:
    
    ```python
    from random import randint


    NB_LIGNES = 4
    NB_COLONNES = 5

    A = [[randint(0,100) for j in range(NB_COLONNES)] for i in range(NB_LIGNES)]
    ```


+ &Eacute;crire ensuite une fonction d'affichage de votre matrice, une ligne écran correspondant à une ligne de la matrice.
On fera en sorte d'aligner proprement les nombres lors de l'affichage. 


??? solution "Aide: aligner les affichages"
    
    Avec print, on peut espacer régulièrement les éléments affichés en ajoutant une tabulation après
    chaque affichage.
    
    ```python
    for i in range(1,5):
        for j in range(1,5):
            print(i*j, end='\t') # ajout d'une tabulation en fin de print
        print()
    ```
    
    donne:
    
    ```
    1   2   3   4   
    2   4   6   8   
    3   6   9   12  
    4   8   12  16
    ```
    
    La valeur par défaut de `end` est  `'\n'`, ce qui explique que par défaut on passe à la ligne
    après chaque instruction `print` lorsqu'on ne renseigne pas explicitement `end`.
    
    
 

??? solution

    ```python
    from random import randint


    NB_LIGNES = 4
    NB_COLONNES = 5

    A = [[randint(0,100) for j in range(NB_COLONNES)] for i in range(NB_LIGNES)]

    def affichage(matrice):
        """
        matrice -- matrice de nombres
        
        Affiche la matrice une ligne écran pour chaque ligne matrice.
        """
        for ligne in matrice:
            for valeur in ligne:
                print(valeur, end="\t") # une tabulation après chaque valeur
            print() # on passe à la ligne sur l'écran à la fin de chaque ligne matrice 


    affichage(A)
    ```











