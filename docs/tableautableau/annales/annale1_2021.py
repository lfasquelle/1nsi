

#######################################
### Définition des labyrinthes
#######################################

    
lab1 = [ [1,1,1,1,1,0,0,0,0,0,1],
        [1,0,0,0,1,0,1,0,1,0,1],
        [1,0,1,0,1,0,1,0,1,0,1],
        [1,0,1,0,1,0,1,0,1,0,1],
        [1,0,1,0,1,0,1,0,1,0,1],
        [2,0,1,0,0,0,1,0,1,0,3],
        [1,1,1,1,1,1,1,0,1,0,1],
        [1,0,0,0,1,0,0,0,1,0,1],
        [1,0,1,0,1,0,1,1,1,0,1],
        [1,0,1,1,1,0,1,0,0,0,1],
        [1,0,0,0,0,0,1,1,0,1,1]]
        
                  
lab2 = [ [1,1,1,1,1,1,1],
             [2,0,0,0,0,0,1],
             [1,1,1,1,1,0,1],
             [1,0,1,0,0,0,1],
             [1,0,1,0,1,0,1],
             [1,0,0,0,1,0,1],
             [1,1,1,1,1,3,1]]   

 
BQ1_2 = [[1,1,4], [0,0,0],[1,1,0]]

lab3 = [[1,1,1,1,1,1],
        [2,0,0,0,0,3],
        [1,0,1,0,1,1],
        [1,1,1,0,0,1]]


    
    
##########################################
#### les fonctions de l'énoncé de l'exercice
##########################################

def est_valide(i, j, n, m):
    return  0 <= i <= n-1 and 0 <= j <= m-1
    
def depart(lab):
    n = len(lab)
    m = len(lab[0])
    for ligne in range(0,n):
        for colonne in range(0,m):
            if lab[ligne][colonne] == 2:
                return (ligne, colonne)
                
def nb_cases_vides(lab):
    n = len(lab)
    m = len(lab[0])
    compteur = 0
    for ligne in range(0,n):
        for colonne in range(0,m):
            if lab[ligne][colonne] in (0, 2, 3):
                compteur += 1
    return compteur              
    
    
def voisines(i, j, lab):
    n = len(lab)
    m = len(lab[0])
    voisins = [(i,j-1), (i-1,j), (i+1,j), (i, j+1)]
    voisins_valides = [x for x in voisins if est_valide(x[0], x[1], n, m)]
    return  [x for x in voisins_valides if lab[x[0]][x[1]] != 1 and lab[x[0]][x[1]] != 4]
    
    
def solution(lab):
    case = depart(lab)
    chemin = [case]
    i, j = case[0], case[1]
    while lab[i][j] != 3:
        lab[i][j] = 4
        voisinage = voisines(i, j, lab)
        if voisinage != [] :
            chemin.append(voisinage[0])
        else:
            chemin.pop()
        # mise à jour de la prochaine case à visiter:
        case = chemin[-1]
        i, j = case[0], case[1]
    return chemin
    
    
    
##########################################
### une fonction de représentation 
### qui s'appuie sur le module  turtle
##########################################
from turtle import *


def affichage(lab, soluce = False):
    
    def rectangle(xbg, ybg, largeur, hauteur, r, g, b):
        color((r, g, b))
        penup()
        goto(xbg,ybg)
        pendown()
        goto(xbg + largeur, ybg)
        goto(xbg + largeur, ybg + hauteur)
        goto(xbg, ybg + hauteur)
        goto(xbg, ybg)   
                
    def rectangle_plein(xbg, ybg, largeur, hauteur,  r, g, b):
        color((200, 200, 200))
        fillcolor((r, g, b))
        penup()
        goto(xbg,ybg)
        pendown()
        begin_fill()
        goto( xbg + largeur, ybg )
        goto( xbg + largeur, ybg + hauteur)
        goto(xbg, ybg + hauteur)
        goto(xbg,ybg)   
        end_fill()
        
    def brique(xbg, ybg, largeur):
        rectangle_plein(xbg, ybg,  largeur, largeur//2,  200, 200, 200)
        epsilon = largeur//20
        rectangle_plein(xbg+epsilon, ybg+epsilon, largeur-2*epsilon, largeur//2-2*epsilon,  200, 0, 0)
        
    def mur(xbg, ybg,   largeur, hauteur):
        largeur_brique = largeur//3
        hauteur_brique = largeur_brique//2
        for r in range(0, hauteur, hauteur_brique): 
            for k in range(0, largeur, largeur_brique):
                brique(xbg + k, ybg + r, largeur_brique)

    def dessine_dedale(tab):
        nb_lignes = len(tab)
        nb_colonnes = len(tab[0])
        for ligne in range(0, nb_lignes):
            for colonne in range(0, nb_colonnes):
                rectangle(colonne*cote, (nb_lignes-1-ligne)*cote, cote, cote, 0, 0, 0)
                if tab[ligne][colonne] == 1:
                    mur(colonne*cote, (nb_lignes-1-ligne)*cote, cote, cote)
                elif tab[ligne][colonne] == 2:
                    penup()
                    goto(colonne*cote+cote/2, (nb_lignes-1-ligne)*cote+cote/3)
                    write("D", align="center", font=("Arial", 12, "normal"))
                elif tab[ligne][colonne] == 3:
                    penup()
                    goto(colonne*cote+cote/2, (nb_lignes-1-ligne)*cote+cote/3)
                    write("A", align="center", font=("Arial", 12, "normal"))
                elif tab[ligne][colonne] == 4:
                    penup()
                    goto(colonne*cote+cote/2, (nb_lignes-1-ligne)*cote+cote/3)
                    write("V", align="center", font=("Arial", 12, "normal"))
                else: 
                    penup()
                    goto(colonne*cote+cote/2, (nb_lignes-1-ligne)*cote+cote/3)
                    write(f"({ligne}, {colonne})", align="center", font=("Arial", 12, "normal"))
                
    def dessine_solution(lab):
        nb_lignes = len(lab)
        nb_colonnes = len(lab[0])
        
        for ligne in range(0, nb_lignes):
            for colonne in range(0, nb_colonnes):
                rectangle(colonne*cote, (nb_lignes-1-ligne)*cote, cote, cote, 0, 0, 0)
                if lab[ligne][colonne] == 1:
                    mur(colonne*cote, (nb_lignes-1-ligne)*cote, cote, cote)
           
        color((0, 200, 0))
        for num, case in enumerate(solution(lab)):
            penup()
            goto(case[1]*cote+cote/2, (nb_lignes-1-case[0])*cote+cote/3)
            write(f"{num}", align="center", font=("Arial", 12, "normal"))
            
            
    nb_lignes = len(lab)  
    nb_colonnes = len(lab[0])         
    cote = 6
    setworldcoordinates(-1*cote,-1*cote, (nb_colonnes+1)*cote, (nb_lignes+1)*cote)
    colormode(255)
    delay(0)
    if soluce:
        dessine_solution(lab)
    else: 
        dessine_dedale(lab)       
    hideturtle()
    mainloop()
    
if __name__ == '__main__': 
    #affichage(lab3)
    affichage(lab2, soluce = True)



