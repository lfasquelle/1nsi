

#######################################
### Définition des labyrinthes
#######################################

    
laby = [[0,1,1,1,1,1,1,1,1,1],
        [0,0,0,0,0,0,0,1,0,1],
        [1,0,1,1,1,1,0,1,0,1],
        [1,0,1,0,0,0,0,0,0,1],
        [1,0,1,1,1,1,1,0,1,1],
        [1,0,1,0,0,0,1,0,1,1],
        [1,0,1,0,1,0,1,0,1,1],
        [1,0,1,1,1,0,1,0,1,1],
        [1,0,0,0,0,0,1,0,0,1],
        [1,1,1,1,1,1,1,1,0,0]]
        
  


    
    
##########################################
#### les fonctions de l'énoncé de l'exercice
##########################################

def mur(labyrinthe, i, j):
    return labyrinthe[i][j] == 1
    
    
def voisine(case1, case2):
    ligne1, colonne1 = case1
    ligne2, colonne2 = case2
    d = (ligne1-ligne2)**2 + (colonne1-colonne2)**2
    return d == 1
    
def adjacentes(liste_cases):
    for i in range(len(liste_cases)-1):
        if not voisine(liste_cases[i], liste_cases[i+1]):
            return False
    return True
    
    
def teste(cases, labyrinthe):
    if not adjacentes(cases):
        return False
    possible = True
    i = 0
    while i < len(cases) and possible:
        if mur(labyrinthe, cases[i][0], cases[i][1]):
            possible = False
        i = i+1
    return possible
    
    
def echappe(liste_cases, labyrinthe):
    n = len(labyrinthe)
    if not teste(liste_cases, labyrinthe):
        return False
    if liste_cases[0] != (0,0):
        return False
    if liste_cases[-1] != (n-1, n-1):
        return False
    return True


liste = [(0,0), (1,0), (1,1), (1,2), (1,3), (1,4), (1,5), (1,6), (2,6), 
(3,6), (3,7), (4,7), (5,7), (6,7), (7,7), (8,7), (8,8), (9,8), (9,9)]
print(teste(liste, laby))
print(echappe(liste, laby))

print(echappe([(0,0),(9,9)], laby))

      
##########################################
### une fonction de représentation 
### qui s'appuie sur le module  turtle
##########################################
from turtle import *


def affichage(lab):
    
    def rectangle(xbg, ybg, largeur, hauteur, r, g, b):
        color((r, g, b))
        penup()
        goto(xbg,ybg)
        pendown()
        goto(xbg + largeur, ybg)
        goto(xbg + largeur, ybg + hauteur)
        goto(xbg, ybg + hauteur)
        goto(xbg, ybg)   
                
    def rectangle_plein(xbg, ybg, largeur, hauteur,  r, g, b):
        color((200, 200, 200))
        fillcolor((r, g, b))
        penup()
        goto(xbg,ybg)
        pendown()
        begin_fill()
        goto( xbg + largeur, ybg )
        goto( xbg + largeur, ybg + hauteur)
        goto(xbg, ybg + hauteur)
        goto(xbg,ybg)   
        end_fill()
        
    def brique(xbg, ybg, largeur):
        rectangle_plein(xbg, ybg,  largeur, largeur//2,  200, 200, 200)
        epsilon = largeur//20
        rectangle_plein(xbg+epsilon, ybg+epsilon, largeur-2*epsilon, largeur//2-2*epsilon,  200, 0, 0)
        
    def mur(xbg, ybg,   largeur, hauteur):
        largeur_brique = largeur//3
        hauteur_brique = largeur_brique//2
        for r in range(0, hauteur, hauteur_brique): 
            for k in range(0, largeur, largeur_brique):
                brique(xbg + k, ybg + r, largeur_brique)

    def dessine_dedale(tab, coords = False):
        nb_lignes = len(tab)
        nb_colonnes = len(tab[0])
        
        # flèche indiquant l'entrée en (0,0):       
        penup()
        goto(cote/2, nb_lignes*cote)
        write("↓", align="center", font=("Arial", 20, "bold")) 
        
        
        for ligne in range(0, nb_lignes):
            for colonne in range(0, nb_colonnes):
                rectangle(colonne*cote, (nb_lignes-1-ligne)*cote, cote, cote, 0, 0, 0)
                if tab[ligne][colonne] == 1:
                    mur(colonne*cote, (nb_lignes-1-ligne)*cote, cote, cote)
                else: 
                    if coords:
                        penup()
                        goto(colonne*cote+cote/2, (nb_lignes-1-ligne)*cote+cote/3)
                        write(f"({ligne}, {colonne})", align="center", font=("Arial", 12, "normal"))
                        
        # flèche indiquant la sortie (case en bas à droite):       
        penup()
        goto((nb_colonnes-1)*cote+cote/2, -cote//5)
        write("↓", align="center", font=("Arial", 20, "bold")) 
         
         
          
        #dessin d'un parcours
        color((0, 200, 0))
        for cellule in [(1,4), (1,5), (1,6), (2,6), (3,6), (3,5), (3,4)]:
            penup()
            goto(cellule[1]*cote+cote/2, (nb_lignes-1-cellule[0])*cote+cote/3)
            write("*", align="center", font=("Arial", 20, "normal"))
            
         
            
    nb_lignes = len(lab)  
    nb_colonnes = len(lab[0])         
    cote = 6
    setworldcoordinates(-1*cote,-1*cote, (nb_colonnes+1)*cote, (nb_lignes+1)*cote)
    colormode(255)
    delay(0)
    dessine_dedale(lab)
    hideturtle()
    mainloop()
    
if __name__ == '__main__': 
    pass
     
    #affichage(laby)



