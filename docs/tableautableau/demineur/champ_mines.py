from affichage import *
from random import random

def cree_champ_de_mines(n, p):
    """
    crée une matrice carrée de taille n
    avec des mines disséminées au hasard.
    Chaque cellule reçoit une mine avec une proba p.
    """
    champ = [[0 for col in range(n)] for lig in range(n)]
    for lig in range(n):
        for col in range(n):
            if 0 <= random() < p: champ[lig][col] = "💣" 
    return champ
  
  
  
def voisinage(colonne, ligne, n):
    """
    colonne: entier entre 0 et n-1
    ligne: entier entre 0 et n-1
    n: entier représentant le nombre de lignes 
    (et de colonnes) d'une matrice champ de mines.
    
    renvoie la liste des voisins sous la forme de couples (numéro de colonne, numéro de ligne)
    de la cellule de coordonnées (colonne, ligne).
    (Attention aux cellules de bordure...)
    """
    x = colonne
    y = ligne
    voisins = [ (x-1,y-1), (x,y-1), (x+1,y-1),
                (x-1,y),  (x+1,y),
                (x-1,y+1), (x,y+1), (x+1,y+1) ]
                
    return [ v for v in voisins if  0<= v[0] < n and 0 <= v[1] < n]
    
    
    
    
  
def compte_bombes_voisines(colonne, ligne, champ):
    """
    champ est une matrice carrée représentant un champ de mines.
    colonne, ligne: coordonnées d'une cellule du champ.
    
    renvoie le nombre de bombes présentes dans les cellules voisines
    de la cellule (colonne, ligne).
    """
    compteur_bombes = 0
    for v in voisinage(colonne, ligne, len(champ)):
        if champ[v[1]][v[0]] == "💣" :
            compteur_bombes += 1
    return compteur_bombes
    
    
    
def place_indications(champ):
    """
    champ est une matrice carrée représentant un champ de mines.
    
    la fonction met à jour cette matrice en ajoutant 
    dans chaque cellule qui ne contient pas une bombe,
    le nombre de bombres présentes dans les cellules voisines.
    """
    for lig in range(n):
        for col in range(n):
            if champ[lig][col] != "💣" :
                champ[lig][col] = compte_bombes_voisines(col, lig, champ)
            
            
      
# un test avec création fichier html:  
n = 10
p = 0.3
champ = cree_champ_de_mines(n, p)
place_indications(champ)
creation_fichier_html("essai", cree_champ_de_mines(10, 0.3))   
