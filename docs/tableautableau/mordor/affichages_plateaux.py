def afficher(tab, taille_case=3):
    """
    tab - tableau 2D (liste de listes d'entiers ou de string)
    taille_case - entier
    Sortie: None 
    Affichage du tableau en console  
    où chaque case est "large" de taille_case caractères
    Les numéros des lignes s'affichent à gauche du plateau
    Les numéros des colonnes s'affichent au-dessus du plateau
    """
    
    def ligne_to_str(ligne, numero_ligne):
        """
        renvoie une chaîne de caractères représentant une ligne 
        de la grille
        """
        representation_ligne = "  |" + ((" "*(taille_case+2) + "|")*nb_colonnes) + "\n"
         
        representation_ligne += str(numero_ligne) + " |" 
        for element in ligne:
            element = str(element)
            while len(element) < taille_case:
                element = " " + element
                if len(element) < taille_case: element = element + " "
            representation_ligne += " "+element + " |" 
        representation_ligne += "\n"
        representation_ligne += "  |" + (("_"*(taille_case+2) + "|")*nb_colonnes)  + "\n"
        return representation_ligne
        
    
    # affichage des numéros de colonnes:
    nb_colonnes = len(tab[0])
    representation = "  "
    for i in range(0, nb_colonnes):
        num = str(i)
        while len(num) < taille_case+2:
            num = " " + num
            if len(num) < taille_case+2: num = num + " "
        representation += " " + num 
    representation += "\n"
    
    # représentation des lignes
    representation += "   " + "_" * (nb_colonnes*(taille_case+3)-1) + "\n"
    for numero_ligne, ligne in enumerate(tab):
        representation += ligne_to_str(ligne, numero_ligne)
        
    print(representation)
    
    
def manhattan(a,b, p,q):
    """
    (a,b) -- coordonnées (ligne, colonne) d'une cellule 
    (p,q) -- coordonnées (ligne, colonne) d'une cellule
    renvoie la distance manhattan entre ces deux cellules
    """
    return abs(a-p)+abs(b-q)
 
 
def distance_au_mal(grille, a,b):
    """
    grille -- matrice, chaque cellule contenant 'N' ou 'B'
    (a,b) -- couple (ligne, colonne) d'une cellule
    renvoie la distance manhattan minimale de la cellule (a,b) à une case noire de grille
    """
    largeur = len(grille[0]) # nombre de colonnes de la grille
    hauteur = len(grille) # nombre de lignes de la grille
    mini = manhattan(a,b, 0, 0)
    for col in range(0,largeur):
        for lig in range(0, hauteur):
            if grille[lig][col] == 'N':
                dist = manhattan(a,b, lig, col)
                if dist < mini:
                    mini = dist
    return mini
    
def grille_dist_mordor(grille):
    largeur = len(grille[0]) # nombre de colonnes de la grille
    hauteur = len(grille) # nombre de lignes de la grille
    
    D = [ [0 for k in range(largeur)] for j in range(hauteur)]
    
    for lig in range(0, hauteur):
        for col in range(0,largeur):
            D[lig][col] = distance_au_mal(grille, lig,col)
    return D
    
    
def une_isolee(grille):
    """
    renvoie les coordonnées d'une cellule parmi les plus éloignées 
    de toute cellule noire
    """
    largeur = len(grille[0]) # nombre de colonnes de la grille
    hauteur = len(grille) # nombre de lignes de la grille
    D = grille_dist_mordor(grille)
    maxi = D[0][0]
    cellule = (0,0)
    for lig in range(0, hauteur):
        for col in range(0,largeur):
            dist = D[lig][col]
            if dist > maxi:
                maxi = dist
                cellule = (lig, col)
    return cellule
    
    
    
A  = [['N','B', 'B'],
      ['B','N', 'N'],
      ['B','B','B'], 
      ['B','B','B'],
      ['B','B','B'],
      ['B','B','N'],
      ['B','B','B']]
      
print(une_isolee(A))

