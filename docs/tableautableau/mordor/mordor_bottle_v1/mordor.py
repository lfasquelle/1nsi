import bottle
import random 
import math

class Plateau:
    
    def __init__(self, nombre_colonnes, nombre_lignes, proportion_noir):
        self.largeur = nombre_colonnes
        self.hauteur = nombre_lignes 
        self.partnoire = proportion_noir if proportion_noir > 0 else 0.5
        self.table = self.remplir()
        self.table_distances = self.maj_table_distances()
        self.dist_secours = max([max(ligne) for ligne in self.table_distances])
        self.table_secours = self.maj_table_secours()
        
    def remplir(self):
        """
        Une proportion partnoire des cellules est marquée  '☠'
        """
        table = [[' ' for i in range(self.largeur)] for j in range(self.hauteur)]
        nb_cellules = self.largeur * self.hauteur
        nb_cellules_affectees = int(nb_cellules * self.partnoire)
        compteur_affectees = 0
        while compteur_affectees < nb_cellules_affectees:
            lig = random.randint(0,self.hauteur-1)
            col = random.randint(0,self.largeur-1)
            if table[lig][col] == ' ':
                table[lig][col] = '☠'
                compteur_affectees += 1
        return table
                
    def taxi_distance(self, ligne1, colonne1, ligne2, colonne2):
        """
        renvoie la distance Manhattan entre les cellules (ligne1, colonne1) et (ligne2, colonne2)
        """
        return abs(ligne2 - ligne1) + abs(colonne2 - colonne1)
        
    def distance_au_mal(self, ligne, colonne):
        """
        renvoie la distance de Manhattan minimale entre la cellule (ligne, colonne)
        et les cellules marquées  '☠'
        """
        distance = math.inf # distance max, correspondrait à une table sans  '☠'
        for lig in range(self.hauteur):
            for col in range(self.largeur):
                if self.table[lig][col] == '☠':
                    d = self.taxi_distance(ligne, colonne, lig, col)
                    if d < distance: distance = d
        return distance
        
    def  maj_table_distances(self):
        table = [[0 for i in range(self.largeur)] for j in range(self.hauteur)]
        for lig in range(self.hauteur):
            for col in range(self.largeur):
                if self.table[lig][col] != '☠':
                    table[lig][col] = self.distance_au_mal(lig, col)
        return table
                    
     
    def maj_table_secours(self):
        table = [[' ' for i in range(self.largeur)] for j in range(self.hauteur)]
        for lig in range(self.hauteur):
            for col in range(self.largeur):
                if self.table_distances[lig][col] == self.dist_secours:
                    table[lig][col] = self.dist_secours
                if self.table[lig][col] ==  '☠':
                    table[lig][col] =  '☠'
        return table
                    
                    
                    
plateau = Plateau(7, 8, 0.2)




@bottle.route('/')
@bottle.view('grille')
def index():    
    matrice = plateau.table
    return dict(nb_colonnes=plateau.largeur, nb_lignes=plateau.hauteur, tab=matrice)



@bottle.route('/secours',  method='POST')
@bottle.view('reponse')
def secours():
    matrice = plateau.table_secours
    ligne = int(bottle.request.forms.get('ligne'))
    colonne = int(bottle.request.forms.get('colonne'))
    reponse = "OK" if matrice[ligne][colonne] == plateau.dist_secours else "KO"
    distance = plateau.table_distances[ligne][colonne]
    return dict(nb_colonnes=plateau.largeur, nb_lignes=plateau.hauteur, tab=matrice, reponse=reponse, num_ligne=ligne,
    num_colonne=colonne, distance_cellule=distance)
    
    
bottle.run(host='localhost', port=8080, debug=True, reloader=True)
