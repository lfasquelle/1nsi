<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>  Test réponse   </title>
    <style>
        html{font-size: 20px;}
        body{width: 90%; margin: 20px auto;}
        table{width: {{nb_colonnes * 1.5}}rem;}
        td{text-align: center;}
        body{display: grid; grid-template-columns: 1fr 1fr; grid-column-gap: 10px;}
        form{font-size: 0.8rem;}
    </style>
</head>
<body>    
    
<table>
    <tr>
        <td style="border: none;"></td>
        % for  col in range(nb_colonnes) : 
            <td style="color: white; background-color: gray; border: none; font-size: 0.5rem;"> {{col}} </td>
        % end
    </tr>
    % for lig in range(nb_lignes) :
        <tr>
            <td style="color: white; background-color: gray;  border: none;   font-size: 0.5rem;"> {{lig}} </td>
            % for  col in range(nb_colonnes) :
                <td style="background-color: white; border: 1px solid black; "> 
                    {{tab[lig][col]}}
                </td>
            % end
        </tr>
    % end
</table> 


<section>
    <p>Vous avez proposé la cellule de ligne {{num_ligne}} et de colonne {{num_colonne}}.</p>
    <p>La distance au Mal de cette cellule est {{distance_cellule}}.</p>
    % if reponse == "OK":
        <p>Votre réponse est correcte.</p>
    % else:
        <p>Votre réponse est incorrecte.</p>
    % end
</section>


</body>
</html>
