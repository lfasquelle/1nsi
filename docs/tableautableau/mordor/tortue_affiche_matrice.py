from turtle import *


def affiche_matrice(tab, unite=10):
    delay(0)
    nb_lignes = len(tab)
    nb_colonnes = len(tab[0])
    setworldcoordinates(-1*unite,-1*unite, (nb_colonnes+1)*unite, (nb_lignes+1)*unite)
    hideturtle()
    # tracé des horizontales:
    penup()
    goto(0,0)
    pendown()
    for ligne in range(nb_lignes+1):
        forward(nb_colonnes * unite)
        penup()
        goto(0, (ligne+1)*unite)
        pendown()
    
    # tracé des verticales:
    penup()
    goto(0,0)
    pendown()
    setheading(90)
    for colonne in range(nb_colonnes+1):
        forward(nb_lignes * unite)
        penup()
        goto((colonne+1)*unite, 0)
        pendown()
        
    # numérotation des colonnes
    penup()
    goto(unite/2,-unite/3)
    pendown()
    for colonne in range(nb_colonnes):
        write(colonne,align="center", font=("Arial", 8, "normal"))
        penup()
        goto((colonne+1)*unite+unite/2, -unite/3)
        pendown()
        
        
    # numérotation des lignes
    penup()
    goto(-unite/2,unite/2)
    pendown()
    for ligne in range(nb_lignes):
        write(nb_lignes-1-ligne,align="right", font=("Arial", 8, "normal"))
        penup()
        goto(-unite/2, (ligne+1)*unite+unite/2)
        pendown()
    
    # remplissage des cellules
    for ligne in range(nb_lignes):
        for colonne in range(nb_colonnes):
            penup()
            goto(colonne*unite+unite/2, (nb_lignes-1-ligne)*unite+unite/3)
            write(tab[ligne][colonne], align="center", font=("Arial", 20, "normal"))
            
    mainloop()


if __name__ == '__main__':
    
    carte =   [['🕱','♡', '♡'],
      ['♡','🕱', '🕱'],
      ['♡','♡','♡'], 
      ['♡','♡','♡'],
      ['♡','♡','♡'],
      ['♡','♡','🕱'],
      ['♡','♡','♡']]
                    
    affiche_matrice(carte)
