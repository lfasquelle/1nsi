<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>  Test réponse   </title>
    <style>
        html{font-size: 20px;}
        body{width: 90%; margin: 20px auto;}
        table{width: {{nb_colonnes * 1.5}}rem;}
        td{text-align: center;}
        body{display: grid; grid-template-columns: 1fr 1fr; grid-column-gap: 10px;}
        form{font-size: 0.8rem;}
    </style>
</head>
<body>    
    
% include('grille.tpl')

<section>
    <p>Vous avez proposé la cellule de ligne {{num_ligne}} et de colonne {{num_colonne}}.</p>
    <p>La distance au Mal de cette cellule est {{distance_cellule}}.</p>
    % if reponse == "OK":
        <p>Votre réponse est correcte.</p>
    % else:
        <p>Votre réponse est incorrecte.</p>
    % end
</section>


</body>
</html>
