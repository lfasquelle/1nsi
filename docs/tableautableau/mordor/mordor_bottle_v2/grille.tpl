 
<table>
    <tr>
        <td style="border: none;"></td>
        % for  col in range(nb_colonnes) : 
            <td style="color: white; background-color: gray; border: none; font-size: 0.5rem;"> {{col}} </td>
        % end
    </tr>
    % for lig in range(nb_lignes) :
        <tr>
            <td style="color: white; background-color: gray;  border: none;   font-size: 0.5rem;"> {{lig}} </td>
            % for  col in range(nb_colonnes) :
                <td style="background-color: white; border: 1px solid black; "> 
                    {{tab[lig][col]}}
                </td>
            % end
        </tr>
    % end
</table> 

 
