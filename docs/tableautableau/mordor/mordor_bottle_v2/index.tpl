<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>  Grille   </title>
    <style>
        html{font-size: 20px;}
        body{width: 90%; margin: 20px auto;}
        table{width: {{nb_colonnes * 1.5}}rem;}
        td{text-align: center;}
        body{display: grid; grid-template-columns: 1fr 1fr; grid-column-gap: 10px;}
        form{font-size: 0.8rem;}
    </style>
</head>
<body>    
    
 
 
% include('grille.tpl')


<form action="/secours" method="post" style="margin: auto;">
    
    <p> Entrez le numéro de ligne et le numéro de colonne d'une cellule dont vous pensez qu'elle
    correspond à une cellule d'éloignement maximal des forces du Mal.</p>

    <div style="border:1px solid black; margin:1rem; padding: 1rem;">
        <label for="ligne">Entrez le numéro  de  ligne de la cellule:</label>
        <input name="ligne" id="ligne" type="number" min="0" max="{{nb_lignes-1}}"/>
    </div>

    <div style="border:1px solid black; margin:1rem; padding: 1rem;">
        <label for="colonne">Entrez le numéro de colonne de la cellule:</label>
        <input name="colonne" id="colonne" type="number" min="0" max="{{nb_colonnes-1}}"/>
    </div>
 
    <input value="Cliquez pour valider" type="submit" />
</form>



</body>
</html>
