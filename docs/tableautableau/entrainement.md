# De la nécessité de s'entraîner


On rappelle ici que les pages du paragraphe "entraînement" ne sont pas obligatoires comme les
pages précédentes (au sens où vous n'avez pas à les connaître parfaitement comme les 
pages qui précèdent) mais qu'elles sont tout de même nécessaires !

Rappelons que l'on n'apprend pas sans entraînement.
