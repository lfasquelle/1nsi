def affichage_simple(tab, taille_case):
    """
    tab - tableau 2D (liste de listes d'entiers ou de string)
    taille_case - entier
    Sortie: None - Affichage du tableau en console sous forme de plateau de jeu
            où chaque case est "large" de taille_case caractères
    """
    largeur = len(tab[0])
    print(" "+"_"*(largeur*(taille_case+3)-1))
    for ligne in tab:
        print("|"+(" "*(taille_case+2)+"|")*largeur)
        print("|", end="")
        for element in ligne:
            element = str(element)
            k = 1
            while len(element) < taille_case:
                if k:
                    element = " " + element
                else:
                    element = element + " "
                k = 1-k
            print(" "+element+" |", end="")
        print()
        print("|"+("_"*(taille_case+2)+"|")*largeur)



def affichage_lignes(tab, taille_case):
    """
    tab - tableau 2D (liste de listes d'entiers ou de string)
    taille_case - entier
    Sortie: None - Affichage du tableau en console sous forme de plateau de jeu
            où chaque case est "large" de taille_case caractères
            Les numéros des lignes s'affichent à gauche du plateau
    """
    largeur = len(tab[0])
    print("   "+"_"*(largeur*(taille_case+3)-1))
    n = 1
    for ligne in tab:
        print("  |"+(" "*(taille_case+2)+"|")*largeur)
        print(str(n)+" |", end="")
        for element in ligne:
            element = str(element)
            k = 1
            while len(element) < taille_case:
                if k:
                    element = " " + element
                else:
                    element = element + " "
                k = 1-k
            print(" "+element+" |", end="")
        print()
        print("  |"+("_"*(taille_case+2)+"|")*largeur)
        n = n+1



def affichage_colonnes(tab, taille_case):
    """
    tab - tableau 2D (liste de listes d'entiers ou de string)
    taille_case - entier
    Sortie: None - Affichage du tableau en console sous forme de plateau de jeu
            où chaque case est "large" de taille_case caractères
            Les numéros des colonnes s'affichent au-dessus du plateau
    """
    largeur = len(tab[0])
    for i in range(1, largeur+1):
        num = str(i)
        k = 1
        while len(num) < taille_case+2:
            if k:
                num = " " + num
            else:
                num = num + " "
            k = 1-k
        print(" "+num, end="")
    print()
    affichage_simple(tab, taille_case)



def affichage_complet(tab, taille_case):
    """
    tab - tableau 2D (liste de listes d'entiers ou de string)
    taille_case - entier
    Sortie: None - Affichage du tableau en console sous forme de plateau de jeu
            où chaque case est "large" de taille_case caractères
            Les numéros des lignes s'affichent à gauche du plateau
            Les numéros des colonnes s'affichent au-dessus du plateau
    """
    largeur = len(tab[0])
    print("  ", end="")
    for i in range(1, largeur+1):
        num = str(i)
        k = 1
        while len(num) < taille_case+2:
            if k:
                num = " " + num
            else:
                num = num + " "
            k = 1-k
        print(" "+num, end="")
    print()
    affichage_lignes(tab, taille_case)




tab=[[chr(65+i+5*j) for i in range(7)] for j in range(5)]
affichage_complet(tab, 3)
