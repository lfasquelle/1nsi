from copy import deepcopy

def cree_matrice(n):
    """
    n:  entier >= 1
    
    renvoie une matrice carrée (2n+1)*(2n+1) avec un 1 au centre 
    et des 0 partout ailleurs.
    """
    mat = [[0 for col in range(2*n+1)] for lig in range(2*n+1)]
    mat[n][n] =  1
    return mat
    
def dec_to_bin(entier):
    """
    entier: entier naturel entre 0 et 1023
    
    renvoie une chaîne de 0 et 1
    correspondant à l'écriture binaire de entier.
    """
    if entier == 0: return '0'
    chaine_bin = ''
    while entier != 0:
        chaine_bin = str(entier%2) + chaine_bin
        entier = entier//2
    return chaine_bin
    
def bin_to_code(chaine_bin):
    """
    chaine_bin: chaîne de 0 et 1 de longueur <= 10
    
    renvoie la chaîne avec d'éventuels 0 à gauche pour que cette chaîne
    ait  une longueur égale à 10.
    
    >>> bin_to_code('1101')
    '0000001101'
    """
    lg = len(chaine_bin)
    while lg != 10:
        chaine_bin = '0' + chaine_bin
        lg += 1
    return chaine_bin
    
def dec_to_code(entier):
    """
    entier: entier naturel entre 0 et 1023
    
    renvoie une chaîne de 0 et 1 de longueur 10
    correspondant à l'écriture binaire de entier 
    (avec d'éventuels 0 à gauche pour obtenir la longueur 10)
    """
    return bin_to_code(dec_to_bin(entier))
    
    
  
def nb_voisins1(matrice, xcellule, ycellule):
    """
    matrice: matrice carrée contenant uniquement des 0 et des 1
    xcellule, ycellule: coordonnées d'une cellule de la matrice
    
    renvoie le nombre de voisins de cellule qui  contiennent 1 
    (parmi les 4 voisins ayant un "côté" commun dans une représentation classique de la matrice
    sous forme d'un tableau)
    """
    compteur = 0
    if matrice[xcellule-1][ycellule] == 1: compteur += 1
    if matrice[xcellule][ycellule-1] == 1: compteur += 1
    if matrice[xcellule][ycellule+1] == 1: compteur += 1
    if matrice[xcellule+1][ycellule] == 1: compteur += 1
    return compteur  
 
def transfo_cell(matrice, xcellule, ycellule, regle):
    """
    regle: entier entre 0 et 1023
    matrice: matrice carrée
    xcellule, ycellule: coordonnées d'une cellule de la matrice
    
    renvoie 0 ou 1 suivant la règle regle.
    """
    nb1 = nb_voisins1(matrice, xcellule, ycellule)
    regle = dec_to_code(regle)
    if matrice[xcellule][ycellule] == 0: 
        return int(regle[2 * nb1])
    else: 
        return int(regle[2 * nb1 + 1])
     
    
def etape(regle, matrice):
    """
    regle: entier entre 0 et 1023.
    matrice: matrice carrée ne contenant que des 0 et des 1.
    
    renvoie la matrice dans laquelle chaque cellule a été modifiée suivant la 
    règle regle.
    Attention: les valeurs des voisins à prendre en compte pour les transformations
    sur les valeurs  initiales de la matrice, tout doit se passer comme si toutes les 
    transformations étaient faites en même temps: en d'autres termes une cellule
    ne doit pas tenir compte de la nouvelle valeur d'une de ses voisines déjà transformée
    mais de la valeur de départ dans la donnée matrice.
    """
    nv_matrice = deepcopy(matrice)
    d = len(matrice)
    for lig in range(1, d-1):
        for col in range(1, d-1):
            nv_matrice[lig][col] = transfo_cell(matrice, col,  lig, regle)
    return nv_matrice
    
    
def repet(n, regle):
    matrice = cree_matrice(n)
    for k in range(n):
        matrice = etape(regle, matrice)
    return matrice
    
 
        
def cree_image(n, regle, couleur):
    from PIL import Image
    matrice = repet(n, regle)
    d = 2*n+1
    imageBut = Image.new('RGB', (d, d))
    for y in range(d) :
        for x in range(d) :
            if matrice[y][x] == 0:
                imageBut.putpixel((x,y), (255, 255, 255))
            else:
                imageBut.putpixel((x,y), couleur)
    # sauvegarde de l'image créée :
    imageBut.save(f'repet{n}_regle{regle}.png')
    # on lance une visualisation :
    #imageBut.show()    
            
    
if __name__ == '__main__':
    
    
     
      
      
    for k in range(400,450):
        cree_image(22, k, (255,215,0))

    for k in range(400,450):
        cree_image(60, k, (255,0,0))
        
    for k in range(400,450):
        cree_image(90, k, (0,0,255))
