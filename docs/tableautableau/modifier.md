# Caractériser ligne, colonne ou diagonale. 

L'objectif des exercices ci-dessous est de savoir caractériser les éléments d'une ligne, d'une colonne, d'une diagonale.


## Diagonale "principale"

On dispose de  matrices de taille n&times;n.

On aimerait changer tous les éléments de la diagonale en 42.

Par exemple 

```
[ [0, 1, 2]
  [3, 4, 5]
  [6, 7, 8]
]
```

doit devenir

```
[ [42, 1, 2]
  [3, 42, 5]
  [6, 7, 42]
]
```

De même 

```
[ [0, 1, 2, 3]
  [4, 5, 6, 7]
  [8, 9, 10, 11]
  [12, 13, 14, 15]
]
```

doit devenir

```
[ [42, 1, 2, 3]
  [4, 42, 6, 7]
  [8, 9, 42, 11]
  [12, 13, 14, 42]
]
```

&Eacute;crire une fonction python effectuant cette transformation d'une matrice n&times;n.

!!! remarque "Rappel sur effet de bord"
    La fonction demandée ne renvoie pas de valeur (ou plutôt renvoie par défaut `None`): il s'agit 
    d'une fonction à effet de bord, elle transforme la matrice passée en argument, variable non locale
    à la fonction.
    
    Rappelons que dans la chaîne de documentation d'une telle fonction, on n'utilise pas le verbe "renvoie" 
    puisqu'une telle fonction ne renvoie rien (sauf par défaut l'objet `None`). On utilise, à la place
    de ce verbe renvoyer, un verbe correspondant à l'action faite par la fonction.

??? solution "Aide: récupérer la taille de la matrice"

    Comment récupérer n lorsqu'on dispose de la matrice A de taille n&times;n?
    
    De deux façons:
    
    + n est le nombre de listes internes, c'est à dire: `n = len(A)`.
    + n est aussi la longueur de chaque liste interne, par exemple `n = len(A[0])`.


??? solution "Aide: caractériser les éléments diagonaux"

    Les éléments de la diagonale sont les éléments A[0][0], A[1][1], A[2][2]...
    En bref, ce sont les éléments A[i][i], pour i entre 0 et len(A)-1.

??? solution

    Un code possible:
    
    ```python
    def affichage(tab):
        """
        tab -- liste de listes de nombres

        affiche chaque liste en passant à la ligne entre les listes.
        """
        for ligne in tab:
            print(ligne)



    def creeMatrice(n):
        """
        n -- entier naturel non nul

        renvoie une matrice de n lignes, n colonnes (cf exercice précédent)
        """
        return [ [i*n + j for j in range(n)] for i in range(n)]  


    def diagonale42(matrice):
        """
        matrice --  matrice n*n

        place des 42 sur la diagonale de matrice.
        """
        n = len(matrice)
        for i in range(n):
            matrice[i][i] = 42



    A = creeMatrice(4)
    diagonale42(A)

    affichage(A)
    ```











## Ligne

On dispose de  matrices de taille n&times;n.

On aimerait changer tous les éléments de la ligne numéro k en 42.

Par exemple, la modification de la ligne d'indice  0 de la matrice suivante: 

```
[ [0, 1, 2]
  [3, 4, 5]
  [6, 7, 8]
]
```

doit transformer cette matrice en:

```
[ [42, 42, 42]
  [3, 4, 5]
  [6, 7, 8]
]
```

De même, la modification de la ligne d'indice 1 de la matrice suivante: 

```
[ [0, 1, 2, 3]
  [4, 5, 6, 7]
  [8, 9, 10, 11]
  [12, 13, 14, 15]
]
```

doit transformer cette matrice en:


```
[ [0, 1, 2, 3]
  [42, 42, 42, 42]
  [8, 9, 10, 11]
  [12, 13, 14, 15]
]
```

&Eacute;crire une fonction python effectuant cette transformation d'une matrice n&times;n.


??? solution "Un code"


    ```python
    def affichage(tab):
        """
        tab -- liste de listes de nombres

        affiche chaque liste en passant à la ligne entre les listes.
        """
        for ligne in tab:
            print(ligne)



    def creeMatrice(n):
        """
        n -- entier naturel non nul

        renvoie une matrice de n lignes, n colonnes (cf exercice précédent)
        """
        return [ [i*n + j for j in range(n)] for i in range(n)]  


    def changeLigne42(matrice, numero_ligne):
        """
        matrice --  matrice n*n

        place des 42 sur la   ligne numero_ligne de la matrice
        """
        n = len(matrice)
        for i in range(n):
            matrice[numero_ligne][i] = 42



    A = creeMatrice(4)
    changeLigne42(A, 1)

    affichage(A)
    ```








## Colonne

On dispose de  matrices de taille n&times;n.

On aimerait changer tous les éléments de la **colonne** numéro k en 42.

&Eacute;crire une fonction python effectuant cette transformation d'une matrice n&times;n.


??? solution
    Un code possible:

    ```python
    def affichage(tab):
        """
        tab -- liste de listes de nombres

        affiche chaque liste en passant à la ligne entre les listes.
        """
        for ligne in tab:
            for element in ligne:
                print(element, end="\t") # \t signifie une tabulation après l'élément
            print()



    def creeMatrice(n):
        """
        n -- entier naturel non nul

        renvoie une matrice de n lignes, n colonnes (cf exercice précédent)
        """
        return [ [i*n + j for j in range(n)] for i in range(n)]  


    def changeColonne42(matrice, numero_colonne):
        """
        matrice --  matrice n*n

        place des 42 sur la colonne numero_colonne de la matrice
        """
        n = len(matrice)
        for i in range(n):
            matrice[i][numero_colonne] = 42



    A = creeMatrice(4)
    changeColonne42(A, 2)

    affichage(A)
    ```



## Seconde diagonale 

&Eacute;crire une fonction prenant en paramètre une matrice carrée
et modifiant les valeurs de la diagonale (représentée ci-dessous en pointillés)
par des 42.

![](images/diagonaler.png)


??? solution "Aide"

    On cherche à obtenir une "formule" simple pour les coordonnées des cellules de la diagonale
    qui nous intéresse ici.
    
    Il s'agit d'une équation de droite... On cherche un lien affine entre le numéro de colonne j et le
    numéro de ligne i.
    
    On trouve facilement que j = n-1-i  (testez!)

??? solution

    Un code possible:
    
    ```python
    from random import randint


    NB_LIGNES = 5
    NB_COLONNES = 5



    def affichage(matrice):
        """
        matrice -- matrice de nombres
        
        Affiche la matrice une ligne écran pour chaque ligne matrice.
        """
        for ligne in matrice:
            for valeur in ligne:
                print(valeur, end="\t") # une tabulation après chaque valeur
            print() # on passe à la ligne sur l'écran à la fin de chaque ligne matrice 

      
    def diagonale42(matrice):
        """
        matrice -- matrice carrée
        
        remplace les éléments de la diagonale principale / par des 42.
        """
        n = len(matrice) # n = nombre de lignes = nombre de colonnes
        for i in range(n):
            matrice[i][n-1-i] = 42 
        
     
    A = [[randint(0,100) for j in range(NB_COLONNES)] for i in range(NB_LIGNES)]
    affichage(A)
    print()
    diagonale42(A)
    affichage(A)
    ```
     
