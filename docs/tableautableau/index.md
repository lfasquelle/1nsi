# Les tableaux de tableaux
 
 
 


 
Un tableau de tableaux est un tableau dont les éléments sont des tableaux.

En langage Python, nous utiliserons une liste dont les éléments sont des listes.

Il n'y a donc pas de notion nouvelle dans cette capsule: nous voyons seulement une nouvelle utilisation
de notions déjà vues. Les pages sont donc ici essentiellement des pages d'exercices pour s'exercer sur cette
utilisation des tableaux.

!!! note
    Pour les tableaux à une dimension, on parle aussi de vecteurs.  
    Les vecteurs dont les éléments sont des vecteurs peuvent alors être nommés tableaux 2D, ou encore matrices.  
    C'est ce que nous manipulerons ici.
