from mako.template import Template

# création du modèle de page html:
template = Template("""
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>  Grille   </title>
    <style>
        html{font-size: 20px;}
        body{width: 80%; margin: 20px auto;}
        td{padding: 5px; text-align: center;}
    </style>
</head>
<body>    
<table>
    <tr>
        <td style="border: none;"></td>
        % for  col in range(nb_colonnes) : 
            <td style="color: green; border: none; font-size: 0.5rem;"> ${col} </td>
        % endfor 
    </tr>
    % for lig in range(nb_lignes) :
        <tr>
            <td style="color: green; border: none;   font-size: 0.5rem;"> ${lig}</td>
            % for  col in range(nb_colonnes) :
                <td style="background-color: white; border: 1px solid black; "> 
                    ${tab[lig][col]} 
                </td>
            % endfor 
        </tr>
    % endfor 
</table> 
</body>
</html>
""")


def creation_fichier_html(nom_fichier, tab):
    """
    nom -- chaîne de caractères, nom d'un fichier à créer
    tab -- matrice à afficher

    La fonction ne renvoie rien, elle crée un fichier html.
    """
    with open(nom_fichier + '.html', 'w') as f:
        texte = template.render(nb_colonnes=len(tab[0]), nb_lignes=len(tab), tab=tab)
        print(texte, file=f)


matrice = [ [ 10*i+j for j in range(7)] for i in range(5) ]
creation_fichier_html("grille_mako", matrice)
