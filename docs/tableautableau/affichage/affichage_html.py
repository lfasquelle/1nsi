


def grille_html(tab):
    """
    tab -- matrice  
    renvoie le code html d'une page html affichant le contenu
    de tab dans un élément  html table.
    """
    
    html = """<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>  Grille   </title>
    <style>
        html{font-size: 20px;}
        body{width: 80%; margin: 20px auto;}
        td{padding: 5px; text-align: center;}
    </style>
</head>
<body>
<table>
<tr>
"""

    nb_lignes = len(tab)
    nb_colonnes = len(tab[0])
    
    # on place les numéros de colonne:
    html += """<td style="border: none;"></td>\n""" 
    for col in range(nb_colonnes):
        html += f"""<td style="color: green; border: none; font-size: 0.5rem;"> {col} </td>\n"""
    html += "</tr>\n"  # fermeture de la ligne du tableau portant les numéros de colonne
    
    # on remplit les lignes:
    for lig in range(nb_lignes):
        html += "<tr>"
        # on inscrit le numéro de ligne:
        html += f"""<td style="color: green; border: none;   font-size: 0.5rem;">{lig}</td>\n"""
        # on crée les cellules de l'élément html table en y inscrivant les valeurs contenues dans tab:
        for col in range(nb_colonnes):
            html += f"""<td style="background-color: white; border: 1px solid black; "> {tab[lig][col]} </td>\n"""
        html += "</tr>\n"
    
    html += "</table>\n"
    html += "</body>\n</html>"
    
    return html
    
def creation_fichier_html(nom_fichier, tab):
    """
    nom -- chaîne de caractères, nom d'un fichier à créer
    tab -- matrice à afficher
    
    La fonction ne renvoie rien, elle crée un fichier html.
    """
    # on crée un fichier html qui sera nommé par la valeur de la variable nom_fichier:
    with open(nom_fichier + '.html', 'w') as f:
        # on écrit la chaine créée par la fonction précédente dans ce fichier:
        print(grille_html(tab), file=f)
    

