# QCM  


!!! note  
    Les QCM sont là pour vous aider à contrôler ce que vous avez retenu.
    Si vous ne répondez pas à toutes les questions sans hésitation, c'est sans doute
    qu'il faut refaire des lectures des pages précédentes.

Trouvez la ou les bonnes réponses.
 
 

## QCM 

On dispose de tee-shirts de couleurs: noir, blanc  
et de tailles S, M, L (small, medium, large).

On dispose des deux listes suivantes:

```python
couleurs = ['noir', 'blanc']
tailles = ['S', 'M', 'L']
```

Pour créer la liste des tee-shirts ('S', 'noir'), ('M', 'noir')...
triés dans l'ordre des tailles (S, M, L), les noirs venant avant les blancs à taille fixée, 
c'est à dire dans l'ordre:

```
[('S', 'noir'),
 ('S', 'blanc'),
 ('M', 'noir'),
 ('M', 'blanc'),
 ('L', 'noir'),
 ('L', 'blanc')]
```
quel code convient:

- [ ] Code 1:
```python
Tshirts = [(taille, couleur) for taille in tailles
                             for couleur in couleurs]
```
- [ ] Code 2:
```python
Tshirts = [(taille, couleur) for couleur in couleurs 
                             for taille in tailles]
```
- [ ] Code 3:
```python
Tshirts = []
for couleur in couleurs:
    for taille in tailles:
        Tshirts.append((taille, couleur))
```
- [ ] Code 4:
```python
Tshirts = []
for taille in tailles:
    for couleur in couleurs:
        Tshirts.append((taille, couleur))
```


??? solution "Réponse"



    - [X] Code 1:
    ```python
    Tshirts = [(taille, couleur) for taille in tailles
                                 for couleur in couleurs]
    ```
    - [ ] Code 2:
    ```python
    Tshirts = [(taille, couleur) for couleur in couleurs 
                                 for taille in tailles]
    ```
    - [ ] Code 3:
    ```python
    Tshirts = []
    for couleur in couleurs:
        for taille in tailles:
            Tshirts.append((taille, couleur))
    ```
    - [X] Code 4:
    ```python
    Tshirts = []
    for taille in tailles:
        for couleur in couleurs:
            Tshirts.append((taille, couleur))
    ```

     
## QCM 

Que vaut A après le script suivant:

```python
A = [ [i for i in range(5)] for j in range(5)]
n = len(A)
for i in range(0,n-1):
    A[i][i+1] = 42
```
    
- [ ] 
```
[ [0, 42, 2, 3, 4]
  [0, 1, 42, 3, 4]
  [0, 1, 2, 42, 4]
  [0, 1, 2, 3, 42]
  [0, 1, 2, 3, 4] ]
```

- [ ] 
```
[ [0, 1, 2, 3, 4]
  [42, 1, 2, 3, 4]
  [0, 42, 2, 3, 4]
  [0, 1, 42, 3, 4]
  [0, 1, 2, 42, 4] ]
```

- [ ] 
```
[42, 1, 2, 3, 4]
[0, 42, 2, 3, 4]
[0, 1, 42, 3, 4]
[0, 1, 2, 42, 4]
[0, 1, 2, 3, 42]
```


??? solution "Réponse"

    - [X] 
    ```
    [ [0, 42, 2, 3, 4]
      [0, 1, 42, 3, 4]
      [0, 1, 2, 42, 4]
      [0, 1, 2, 3, 42]
      [0, 1, 2, 3, 4] ]
    ```

    - [ ] 
    ```
    [ [0, 1, 2, 3, 4]
      [42, 1, 2, 3, 4]
      [0, 42, 2, 3, 4]
      [0, 1, 42, 3, 4]
      [0, 1, 2, 42, 4] ]
    ```

    - [ ] 
    ```
    [42, 1, 2, 3, 4]
    [0, 42, 2, 3, 4]
    [0, 1, 42, 3, 4]
    [0, 1, 2, 42, 4]
    [0, 1, 2, 3, 42]
    ```


## QCM 


Que vaut B après les lignes suivantes:

```python
A = [ [1,2,3], [4,5,6]]
B = A
A[0] = [7,8,9]
```

- [ ] `[[1, 2, 3], [4, 5, 6]]`
- [ ] `̀[[7, 8, 9], [4, 5, 6]]`


??? solution "Réponse"

    - [ ] `[[1, 2, 3], [4, 5, 6]]`
    - [X] `̀[[7, 8, 9], [4, 5, 6]]`
    
    
    
    
    
    


## QCM 


Que vaut B après les lignes suivantes:

```python
A = [ [1,2,3], [4,5,6]]
B = [ A[i] for i in range(len(A))]
A[0] = [7,8,9]
```

- [ ] `[[1, 2, 3], [4, 5, 6]]`
- [ ] `̀[[7, 8, 9], [4, 5, 6]]`


??? solution "Réponse"

    - [X] `[[1, 2, 3], [4, 5, 6]]`
    - [ ] `̀[[7, 8, 9], [4, 5, 6]]`
    
    
    
    
    
    

## QCM 


Que vaut B après les lignes suivantes:

```python
A = [ [1,2,3], [4,5,6]]
B = [ A[i] for i in range(len(A))]
A[0][1] = 42
```

- [ ] `[[1, 2, 3], [4, 5, 6]]`
- [ ] `̀[[1, 42, 3], [4, 5, 6]]`


??? solution "Réponse"

    - [ ] `[[1, 2, 3], [4, 5, 6]]`
    - [X] `̀[[1, 42, 3], [4, 5, 6]]`




## QCM 


Que vaut B après les lignes suivantes:

```python
A = [ [1,2,3], [4,5,6]]
B = [ [A[i][j] for j in range(len(A[0]))] for i in range(len(A))]
A[0][1] = 42
```

- [ ] `[[1, 2, 3], [4, 5, 6]]`
- [ ] `̀[[1, 42, 3], [4, 5, 6]]`


??? solution "Réponse"

    - [X] `[[1, 2, 3], [4, 5, 6]]`
    - [ ] `̀[[1, 42, 3], [4, 5, 6]]`
    
    
    


## QCM 


Que vaut B après les lignes suivantes:

```python
A = [ [1,2,3], [4,5,6]]
B = [ A[i][j] for j in range(len(A[0])) for i in range(len(A))]
```

- [ ] `[[1, 2, 3], [4, 5, 6]]`
- [ ] `̀[1, 2, 3, 4, 5, 6]`
- [ ] `[1, 4, 2, 5, 3, 6]`


??? solution "Réponse"

    - [ ] `[[1, 2, 3], [4, 5, 6]]`
    - [ ] `̀[1, 2, 3, 4, 5, 6]`
    - [X] `[1, 4, 2, 5, 3, 6]`
    
    
    

## QCM 


Que vaut B après les lignes suivantes:

```python
A = [ [1,2,3], [4,5,6]]
B = [ A[i][j] for i in range(len(A)) for j in range(len(A[0]))]
```

- [ ] `[[1, 2, 3], [4, 5, 6]]`
- [ ] `̀[1, 2, 3, 4, 5, 6]`
- [ ] `[1, 4, 2, 5, 3, 6]`


??? solution "Réponse"

    - [ ] `[[1, 2, 3], [4, 5, 6]]`
    - [X] `̀[1, 2, 3, 4, 5, 6]`
    - [ ] `[1, 4, 2, 5, 3, 6]`

