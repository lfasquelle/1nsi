# QCM


## QCM 1

Quelle est l'écriture binaire de l'entier 42<sub>dix</sub>:

- [ ] 101010<sub>deux</sub>
- [ ] 10101<sub>deux</sub>
- [ ] 10010<sub>deux</sub>


??? solution "Réponse"
    
    
    - [X] 101010<sub>deux</sub>
    - [ ] 10101<sub>deux</sub>
    - [ ] 10010<sub>deux</sub>



## QCM 2

Quelle est l'écriture binaire de 42<sub>seize</sub>:

- [ ] 10010<sub>deux</sub>
- [ ] 1000010<sub>deux</sub>
- [ ] 10 0100<sub>deux</sub>

??? solution "Réponse"


    - [ ] 1 0010<sub>deux</sub>
    - [X] 100 0010<sub>deux</sub>
    - [ ] 10 0100<sub>deux</sub>
    
    
    
## QCM 3

Quelle est l'écriture décimale de 42<sub>seize</sub>:


- [ ] 666<sub>dix</sub>
- [ ] 66<sub>dix</sub>
- [ ] 42<sub>dix</sub>

??? solution "Réponse"  


    - [ ] 666<sub>dix</sub>
    - [X] 66<sub>dix</sub>
    - [ ] 42<sub>dix</sub>  
    
    
## QCM 4

Quelle est l'écriture hexadécimale de 42<sub>dix</sub>:


- [ ] 42<sub>seize</sub>
- [ ] 66<sub>seize</sub>
- [ ] 2a<sub>seize</sub>


??? solution "Réponse"  

    - [ ] 42<sub>seize</sub>
    - [ ] 66<sub>seize</sub>
    - [X] 2a<sub>seize</sub>
    
    
    
## QCM 5

Sur 2 octets, combien d'entiers naturels peut-on coder en binaire?

- [ ] 16<sup>2</sup>
- [ ] 2<sup>16</sup>
- [ ] 16&times;2
- [ ] 16

??? solution "Réponse" 


    - [ ] 16<sup>2</sup>
    - [X] 2<sup>16</sup>
    - [ ] 16&times;2
    - [ ] 16


## QCM 6

On considère le code python suivant:

```python
def f(n):
    """
    n -- entier naturel non nul
    """
    ch = ''
    while n > 0:
        ch = ch + str(n%10)
        n = n//10
    return ch
```

Est ce que f renvoie une chaîne correspondant à l'écriture décimale de n?

- [ ] oui
- [ ] non

??? solution "Réponse"

    - [ ] oui
    - [X] non
    
    Il faut remplacer la ligne `ch = ch + str(n%10)` par `ch = str(n%10) + ch` 
    pour que les chiffres soient dans l'ordre de lecture usuel.
    
    
    

    
    
## QCM 7

Il se raconte que l'inventeur du jeu d'échec aurait demandé pour paiement de sa création 1 grain de blé pour
la première case, deux grains de blé pour la seconde case, 4 pour la case 3, 8 pour la case 4...et ainsi
de suite jusqu'à la case 64: on double le nombre de grains à chaque case.

Le nombre total de grains de blé est:

- [ ] 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111<sub>deux</sub> (le nombre est écrit avec 64 chiffres 1)
- [ ] 2<sup>64</sup>-1
- [ ] ffff ffff ffff ffff<sub>seize</sub>
- [ ] 2<sup>64</sup>


??? solution "Réponse"

    - [X] 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111<sub>deux</sub> 
    - [X] 2<sup>64</sup>-1
    - [X] ffff ffff ffff ffff<sub>seize</sub>
    - [ ] 2<sup>64</sup>
    
    !!! note
        2<sup>64</sup> = 18 446 744 073 709 551 616.
        
        A raison de 30 mg par grain de blé, cela représente environ 553 402 322 211 tonnes de blé.
        
        La production mondiale de blé en 2016 est d'environ 754 000 000 tonnes...
        
    
    
    
       
## QCM 8


Soit n un entier naturel.  Cet entier s'écrit en base dix avec 4 chiffres:

- [ ] si $10^4 \leqslant n < 10^5$
- [ ] si $10^3 < n \leqslant 10^4$
- [ ] si $10^3 \leqslant n < 10^4$
- [ ] si $10^4 < n \leqslant 10^5$

??? solution "Réponse"

    - [ ] si $10^4 \leqslant n < 10^5$
    - [ ] si $10^3 < n \leqslant 10^4$
    - [X] si $10^3 \leqslant n < 10^4$  (entiers entre 1000 et 9999)
    - [ ] si $10^4 < n \leqslant 10^5$
        
    
        
## QCM 9


Soit n un entier naturel. On suppose que l'entier m vérifie $2^n < m \leqslant 2^{n+1}$.

Alors: l'écriture de m en base deux comporte exactement n+1 chiffres.

- [ ] vrai.
- [ ] faux.


??? solution "Réponse"

    - [ ] vrai.
    - [X] faux.
    
    
    Le principe est le même qu'en base dix (cf qcm précédent). 
    
    Tout entier m tel que $2^n \leqslant m < 2^{n+1}$ a n+1 chiffres: il est certain qu'à partir de $2^{n+1}$
    et au-delà, on ne mettra que des 0 en écriture binaire puisque $m < 2^{n+1}$. Donc on n'a besoin de chiffres
    que sur les poids  0 à $n$.
    
    L'entier $2^{n+1}$  s'écrit avec n+2 bits (un "1" suivi de n+1 "0").
    
    Il en va de même avec toute autre base.  
    

  
