# Du décimal à l'hexadécimal

## Exercice.

A l'aide de divisions en cascade, obtenir l'écriture hexadécimale des entiers suivants:

1. $n = 43981$
2. $m = 33$
3. $k = 51$



??? solution "Solution"

    1. On pose notre division en cascade:        
    ![](img/cascadeSeize1.png){:height="400px" width="400px"}   
    L'écriture binaire est donc $43981 =  abcd_{\text{seize}}$.   
    On peut "vérifier": $13 + 12 \times 16 + 11 \times 16^2 + 10\times 16^3 = 43981$.
    2. On pose notre division en cascade:        
    ![](img/cascadeSeize2.png){:height="400px" width="400px"}   
    L'écriture binaire est donc $33 =  21_{\text{seize}}$.   
    On peut "vérifier": $1 + 2 \times 16  =  33$.
    3. On pose notre division en cascade:        
    ![](img/cascadeSeize3.png){:height="400px" width="400px"}   
    L'écriture binaire est donc $51 =  33_{\text{seize}}$.   
    On peut "vérifier": $3 + 3 \times 16  =  51$.


