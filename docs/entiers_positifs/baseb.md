# &Eacute;criture d'un entier en base b


Avant de travailler sur la base seize, on fait un peu le point sur l'aspect générique de l'écriture dans une base $b$ ($b$
désigne toujours un entier au moins égal à 2).


## Rappel sur la base 10

En base 10 usuelle, l'écriture 3452 signifie $3\times 10^3 + 4 \times 10^2 + 5 \times 10^1 + 2 \times 10^0$.

De façon plus générique, si l'on note $c_0$ le chiffre des unités de l'entier $n$, $c_1$ le chiffre des dizaines, 
$c_2$ le chiffre des centaines,... l'entier $n$ est:
$$
n = c_0 + c_1\times 10 + c_2 \times 10^2 + c_3\times 10^3 + \dots
$$
où chaque coefficient $c_i$ est l'un des chiffres 0, 1, 2, 3, 4, 5, 6,7, 8, 9.

 

!!! note
    L'écriture en base dix d'un entier est aussi appelée écriture décimale de cet entier. 
    Vous ne confondrez pas l'utilisation de l'adjectif avec l'utilisation usuelle dans "nombre décimal": nous
    parlons bien (pour le moment) uniquement d'entiers.


## En base b

Soit $b$ un entier naturel ($b \geqslant 2$). On peut écrire un entier naturel en base $b$
en utilisant le même principe que ci-dessus:

Tout entier naturel s'écrit sous la forme $n = c_0 + c_1 \times b + c_2 \times b^2 + c_3 \times b^3 + \dots$
où chaque coefficient $c_i$ est à prendre dans la liste: $0, 1, 2, \dots, b-1$.

En base $b$:

+ le coefficient $c_0$ (coefficient de $b^0$) est appelé chiffre de poids 0 (en base 10 le chiffre de poids 0 est donc le chiffre des unités),
+ le coefficient $c_1$ (coefficient de $b^1$) est appelé chiffre de poids 1 (en base 10 le chiffre de poids 1 est donc le chiffre des dizaines),
+ le coefficient $c_2$ (coefficient de $b^2$) est appelé chiffre de poids 2 (en base 10 le chiffre de poids 2 est donc le chiffre des centaines),
+ ...



## En base 2

&Eacute;crire un entier $n$ en base deux, c'est écrire cet entier $n$:

+  sous la forme $n = c_0 + c_1 \times 2 + c_2 \times 2^2 + c_3 \times 2^3 + \dots$, 
+  et dans cette écriture chacun des coefficients $c_i$ vaut 0 ou 1.

!!! note
    L'écriture d'un entier en base 2 sera appelée écriture binaire de l'entier.


En base 2:

+ le coefficient $c_0$ (coefficient de $2^0$) est appelé bit de poids 0,
+ le coefficient $c_1$ (coefficient de $2^1$) est appelé bit de poids 1,
+ le coefficient $c_2$ (coefficient de $2^2$) est appelé bit de poids 2,
+ ...

## En base 16

&Eacute;crire un entier $n$ en base seize, c'est écrire cet entier $n$:

+  sous la forme $n = c_0 + c_1 \times 16 + c_2 \times 16^2 + c_3 \times 16^3 + \dots$, 
+  et dans cette écriture chacun des coefficients $c_i$ vaut 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ou 15.

!!! note
    L'écriture d'un entier en base 16 sera appelée écriture hexadécimale de l'entier.
