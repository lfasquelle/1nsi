# Du binaire vers le décimal

??? Objectif
    Comprendre la blagounette suivante: 
    
    Il n'y a que 10 sortes de personnes:
    
    - ceux qui comprennent le binaire,
    - et les autres.
    

## Rappel sur la base 10

En base 10 usuelle, l'écriture 3452 signifie $3\times 10^3 + 4 \times 10^2 + 5 \times 10^1 + 2 \times 10^0$.

De façon plus générique, si l'on note $c_0$ le chiffre des unités de l'entier $n$, $c_1$ le chiffre des dizaines, 
$c_2$ le chiffre des centaines,... l'entier $n$ est:
$$
n = c_0 + c_1\times 10 + c_2 \times 10^2 + c_3\times 10^3 + \dots
$$
où chaque coefficient $c_i$ est l'un des chiffres 0, 1, 2, 3, 4, 5, 6,7, 8, 9.

Avec l'exemple de 3452, $c_0 = 2$, $c_1 = 5$, $c_2 = 4$ et $c_3 = 3$.

 

!!! note
    L'écriture en base dix d'un entier est aussi appelée écriture décimale de cet entier. 
    Vous ne confondrez pas l'utilisation de l'adjectif avec l'utilisation usuelle dans "nombre décimal": nous
    parlons bien (pour le moment) uniquement d'entiers.
 
 
 
 
  



## En base 2


Le principe est le même qu'en base dix, mais on utilise 2 à la place de 10!
Les seuls chiffres disponibles sont 0 et 1.

### Exemple

L'entier $n = 110_{\text{deux}}$ désigne l'entier $n = 1\times 2^2 + 1 \times 2^1 + 0\times 2^0$, 
c'est à dire l'entier $n = 6$.

!!! note
    Comme le suggèrent les écritures ci-dessus, nous noterons en indice la base utilisée afin de 
    distinguer $110_{\text{deux}} = 1\times 2^2 + 1 \times 2^1$ et $110_{\text{dix}} = 1\times 10^2 + 1 \times 10^1$ par exemple.
    
    A chaque fois que la base ne sera pas indiquée en indice, cela sous-entend que l'écriture est 
    l'écriture décimale usuelle.
    
    
    
    
   
   
??? note "Pourquoi seulement deux chiffres?"

    ### En base dix

    Le principe (appris à l'école primaire) est de faire des regroupements par paquet de dix.

    Par exemple pour 4325:

    + Je peux faire 432 paquets de dix (qu'on appellera paquet-dizaine) et un paquet de 5.
    + Je regroupe ensuite les paquets précédents par dix: je peux faire 43 paquets de dix paquets-dizaine (
    ce qui constitue des paquets-centaine) et un paquet de 2 paquets-dizaine.
    + Je regroupe par dix les paquets-centaine: je peux faire 4 paquets de dix paquets-centaine 
    et il reste 3 paquets-centaine.


    Au final: 4 paquets-millier, 3 paquets-centaine, 2 paquets-dizaine, 5 unités.

    On voit pourquoi on a besoin de dix chiffres: lorsqu'il n'y a pas assez de paquets pour en faire dix, c'est
    qu'il y en a 0, 1, 2, 3, 4, 5, 6, 7, 8, ou 9. 

    ### En base deux.

    On procède de la même façon, mais en regroupant par paquets de 2.

    Par exemple, avec 13 poires.

    + Je fais 6 paquets de deux et il m'en reste une:  🍐🍐, 🍐🍐, 🍐🍐, 🍐🍐, 🍐🍐, 🍐🍐, 🍐
    + Je regroupe les paquets de deux par deux: 🍐🍐, 🍐🍐  |  🍐🍐, 🍐🍐 | 🍐🍐, 🍐🍐 | 🍐.
    + Je regroupe les paquets de quatre par deux: 🍐🍐, 🍐🍐  |  🍐🍐, 🍐🍐  || 🍐🍐, 🍐🍐 | 🍐.


    D'où l'écriture binaire de 13: 1 paquet de 8, 1 paquet de 4, 0 paquet de 2, 1 paquet de 1, soit 1101<sub>deux</sub>.

    On voit pourquoi deux chiffres seulement: à partir de deux éléments, je peux faire un paquet d'ordre supérieur, il
    n'y a donc pas besoin de symboles spécifiques autres que 0 et 1.

       
    
    
    
    
    
## Le bit

Un chiffre en binaire vaut 0 ou 1. 

Un chiffre en binaire est parfois appelé **bit**, mot qui est un raccourci de *binary digit* (chiffre binaire).

    
    
 
    
    
    
    
    
    
## Exercice 1.

+ Donner l'écriture décimale de l'entier  $n = 101010_{\text{deux}}$.

??? solution "Résolution"
    $n = 101010_{\text{deux}}$, 
    soit $n = 1\times 2^5  + 0\times 2^4  + 1\times 2^3  + 0\times 2^2  + 1\times 2^1  + 0\times 2^0$, 
    ou encore $n = 32 + 8 + 2$. On a donc $n = 42$.


+ Donner l'écriture décimale de l'entier  $n = 1100_{\text{deux}}$.


??? solution "Résolution"
    $n = 1100_{\text{deux}}$, 
    soit $n = 1\times 2^3  + 1\times 2^2      + 0\times 2^1  + 0\times 2^0$, 
    ou encore $n = 8 + 4 + 0$. On a donc $n = 12$.
    
    
+ Donner l'écriture décimale de l'entier  $n = 100100_{\text{deux}}$.


??? solution "Résolution"
    $n = 100100_{\text{deux}}$, 
    soit $n = 1\times 2^5  + 1\times 2^2$, 
    ou encore $n = 32 + 4$. On a donc $n = 36$.
    
    
 

## Exercice 2.

Quels sont les entiers qui s'écrivent, en binaire, sous la forme d'un 1 suivi uniquement de 0 
(1<sub>deux</sub>, 10<sub>deux</sub>, 100<sub>deux</sub>, 1000<sub>deux</sub>, 10000<sub>deux</sub>...)?


??? solution "Résolution"
    
    + $1_{\text{deux}} = 1 = 2^0$
    + $10_{\text{deux}} = 2 = 2^1$
    + $100_{\text{deux}} =  2^2$
    + $1000_{\text{deux}} =  2^3$
    + ...
    + $100...0_{\text{deux}} =  2^m$ où $m$ est le nombre  de zéros.
    
    Les nombres 10...0 sont les puissances de 2. En base 10, ce sont les puissances de 10. 
    En base b, ce sont les puissances de b.
    
    
    
    
    
    
    
    
    

## Exercice 3.

On rappelle qu'un octet correspond à 8 bits.

1. Sur un octet quels entiers naturels peut-on écrire en binaire? 
1. Combien d'entiers peut-on écrire en binaire sur un octet? 
1. Quel est le plus petit?
1. Quel est le plus grand?


??? solution "sur un octet, quels entiers naturels?"

    On peut écrire les entiers de $0000\, 0000_{\text{deux}} = 0$ 
    à $1111\, 1111_{\text{deux}} = 1+2+2^2+ 2^3 +2^4+2^5+2^6 +2^{7}$, 
    c'est à dire les entiers entre 0 et $2^8-1 = 255$.
    
    
??? note "Remarque"
    On a écrit ci-dessus que  $1+2+2^2+ 2^3 +2^4+2^5+2^6 +2^{7} = 2^8-1$.
    Cela peut se montrer par un simple calcul des deux expressions. Mais on peut également
    voir cela avec un raisonnement plus générique.
    
    On pose $S = 1+2+2^2+ 2^3 +2^4+2^5+2^6 +2^{7}$.
    Et on remarque que $2S-S=S$.
    Or 
    
    \begin{align}
    2S-S &= 2(1+2+2^2+ 2^3 +2^4+2^5+2^6 +2^{7}) - (1+2+2^2+ 2^3 +2^4+2^5+2^6 +2^{7})\\
    &= (2+2^2+ 2^3 +2^4+2^5+2^6 +2^{7}+2^8) -  (1+2+2^2+ 2^3 +2^4+2^5+2^6 +2^{7})\\
    &= 2^8 - 1
    \end{align}
    
    De façon plus générale, pour un entier naturel $n$:   
    $\boxed{1+2+2^2+ 2^3 +2^4+ ... +2^{n} = 2^{n+1}-1}$
    
    
??? solution "sur un octet, combien d'entiers codés?"
    On peut écrire   les entiers entre 0 et $2^8-1$: il y en a $256$.
    
??? solution "le plus petit"    
    Le plus petit est 0.
    
??? solution "le plus grand"  
    Le plus grand est $2^8-1 = 256$.


## Exercice 4.

Soit $m$ un entier au moins égal à 1. 

1. Quels entiers naturels peut-on écrire en binaire sur $m$ bits? 
1. Combien d'entiers peut-on écrire en binaire sur  $m$ bits? 
1. Quel est le plus petit?
1. Quel est le plus grand?


??? solution "lesquels?"

    On peut écrire les entiers de $0\dots 0_{\text{deux}} = 0$ à $1\dots 1_{\text{deux}} = 1+2+2^2+\dots+2^{m-1}$,   
    c'est à dire les entiers entre 0 et $2^m-1$.
    
??? solution "combien?"    
    On peut écrire   les entiers entre 0 et $2^m-1$: il y en a $2^m$.
    
    
??? solution "le plus petit" 
    Le plus petit est 0.
    
    
??? solution "le plus grand"    
    Le plus grand est $2^m-1$.




