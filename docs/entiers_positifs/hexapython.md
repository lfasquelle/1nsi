# Entre la base 2 et la base 16, en python


!!! note
    **Les exercices de cette page sont facultatifs.**  
    Travaillez les exercices de cette page pour aller plus loin.   
    Ces exercices ne sont pas une priorité pour le moment mais nous reviendrons sur la notion
    de dictionnaire et il faudra donc savoir traiter ce genre de programme lorsque nous aurons travaillé
    les dictionnaires.
    
    
    
## Correspondance



| binaire | hexadécimal |
|:-------:|:-----------:|
|   0000  |      0      |
|   0001  |      1      |
|   0010  |      2      |
|   0011  |      3      |
|   0100  |      4      |
|   0101  |      5      |
|   0110  |      6      |
|   0111  |      7      |
|   1000  |      8      |
|   1001  |      9      |
|   1010  |      a      |
|   1011  |      b      |
|   1100  |      c      |
|   1101  |      d      |
|   1110  |      e      |
|   1111  |      f      |


&Eacute;crire un code python générant un dictionnaire `correspondance` tel que
`correspondance['0000'] = '0'`, `correspondance['0001'] = '1'`, ..., `correspondance['1111'] = 'f'`.



??? note "Qu'est ce qu'un dictionnaire?"

    Le type dictionnaire sera vu un peu plus en détail plus tard.  
    Pour cet exercice, il suffit de voir un dictionnaire comme
    un tableau dans lequel on remplace les indices par des noms explicites.
    
    Par exemple:
    
    <iframe src="fichiersHTML/traduire.html" 
     width="100%" 
     height="300" 
     style="border:2px solid orange">
    </iframe> 


??? note "Une aide: la fonction chr"
    Pour la construction de ce dictionnaire, il est possible (mais pas obligatoire) d'utiliser la fonction
    python `chr`. Le résultat du petit script ci-dessous vous permettra de comprendre son rôle:
    
    ``` python
    for k in range(97, 120):
        print(f"chr({k}) = {chr(k)}.")
    ```
    L'affichage obtenu:
    
    ```
    chr(97) = a.
    chr(98) = b.
    chr(99) = c.
    chr(100) = d.
    chr(101) = e.
    chr(102) = f.
    chr(103) = g.
    chr(104) = h.
    chr(105) = i.
    chr(106) = j.
    chr(107) = k.
    chr(108) = l.
    chr(109) = m.
    chr(110) = n.
    chr(111) = o.
    chr(112) = p.
    chr(113) = q.
    chr(114) = r.
    chr(115) = s.
    chr(116) = t.
    chr(117) = u.
    chr(118) = v.
    chr(119) = w.
    ```

??? solution "Solution: un code possible"
     
    
    ``` python
    correspondance = {}
    for c0 in ('0','1'):
        for c1 in ('0','1'):
            for c2 in ('0','1'):
                for c3 in ('0','1'):
                    mot = c3 + c2 + c1 + c0
                    hexa = int(c0) + 2 * int(c1) + 2**2 * int(c2) + 2**3 * int(c3)
                    if hexa < 10:
                        correspondance[mot] = str(hexa)
                    else:
                        correspondance[mot] = chr(hexa + 97 -10)
    ```
    
    L'instruction `print(correspondance)` donne:
    
    ```
    {'0000': '0', '1000': '8', '0100': '4', '1100': 'c', 
    '0010': '2', '1010': 'a', 
    '0110': '6', '1110': 'e', '0001': '1', '1001': '9', 
    '0101': '5', '1101': 'd', 
    '0011': '3', '1011': 'b', '0111': '7', '1111': 'f'}
    ```

   
## Exercice 2.

&Eacute;crire un corps possible pour la fonction python suivante:

``` python
def bin2hexa(n):
    """
    n -- type str, composé de '0' et de '1', 
    c'est l'écriture binaire d'un entier naturel.
    
    renvoie une chaîne correspondant 
    à l'écriture hexadécimale de n
    en procèdant comme ci-dessus.
    """
```


??? solution "Une traduction possible"
    On  utilise  le dictionnaire correspondance défini plus haut.
    
    ``` python
    bits = ('0', '1')
    binaires = [c3+c2+c1+c0 for c0 in bits for c1 in bits for c2 in bits for c3 in bits]



    correspondance = {}
    for m in binaires:
        hexa = int(m[3]) + 2 * int(m[2]) + 2**2 * int(m[1]) + 2**3 * int(m[0])
        if hexa < 10:
            correspondance[m] = str(hexa)
        else:
            correspondance[m] = chr(hexa + 97 -10)




    def complete(n):
        """
        n -- chaîne de '0' et de '1'.
        
        renvoie la chaîne avec des '0' à gauche supplémentaires 
        éventuels pour que la 
        chaîne soit de longueur multiple de 4.
        """
        lg = len(n)
        lg = lg%4
        if lg == 0:
            return n
        else:
            return '0' * (4 - lg%4) + n




    def bin2hexa(n):
        """
        n -- type str, composé de '0' et de '1', 
        c'est l'écriture binaire d'un entier naturel.

        renvoie une chaîne correspondant 
        à l'écriture hexadécimale de n
        en procèdant comme ci-dessus.
        """
        n = complete(n)
        ch = ''
        for k in range(0, len(n), 4):
            ch = ch + correspondance[n[k:k+4]]
        return ch


    # essai:
    print(bin2hexa('1000010'))
    ``` 
