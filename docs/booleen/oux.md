# Le ou exclusif


Pour distinguer le "ou exclusif" du "ou", on peut noter le "ou exclusif" par "oux".
Il est toutefois plus usuel d'utiliser le nom anglais 'xor' (eXclusif OR).


## Définition

La fonction xor est une fonction de $\mathbb{B}\times\mathbb{B}$ dans $\mathbb{B}$ définie par:

+ xor(0; 0) = 0
+ xor(0; 1) = 1
+ xor(1; 0) = 1
+ xor(1; 1) = 0

Ce que l'on présente en général sous forme d'un tableau:

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-s6z2{text-align:center}
.tg .tg-baqh{text-align:center;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-s6z2">a</th>
    <th class="tg-s6z2">b</th>
    <th class="tg-baqh">xor(a;b) <br> noté aussi "a xor b"</th>
  </tr>
  <tr>
    <td class="tg-s6z2">0</td>
    <td class="tg-s6z2">0</td>
    <td class="tg-baqh">0</td>
  </tr>
  <tr>
    <td class="tg-s6z2">0</td>
    <td class="tg-s6z2">1</td>
    <td class="tg-baqh">1</td>
  </tr>
  <tr>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">0</td>
    <td class="tg-baqh">1</td>
  </tr>
  <tr>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">0</td>
  </tr>
</table>

 
 
 

On peut résumer ce tableau   par l'équivalence:

$$(a \text{ xor } b = 0) \Longleftrightarrow (a = b)$$

Ou ce qui revient au même par l'équivalence:


$$(a \text{ xor } b = 1) \Longleftrightarrow (a \neq b)$$

!!! important
    "a xor b" s'interprète par: on a "a ou b" mais **pas les deux à la fois**
    (alors que le ou classique s'interprète par l'un ou l'autre, et éventuellement les deux).
    
    
    

## Remarque

En interprétant 0 et 1 comme des entiers naturels, on 
peut également remarquer que l'on a:

$$a\text{ or } b = \max(a~;~b)$$

$$a\text{ and } b = \min(a~;~b)$$

 

$$a \text{ xor } b = \max(a; b) - \min(a; b)$$

 

   
