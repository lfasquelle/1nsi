# Non

En informatique, la fonction non est très souvent nommée par son nom anglais: 'not'.

## La fonction 'non'.

La fonction 'ou' est une fonction de $\mathbb{B}$ dans $\mathbb{B}$ définie par:

+ non(0) = 1
+ non(1) = 0
 

Ce que l'on présente en général sous forme d'un tableau:

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-s6z2{text-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-s6z2">a</th>
    <th class="tg-s6z2">non(a)</th>
  </tr>
  <tr>
    <td class="tg-s6z2">0</td>
    <td class="tg-s6z2">1</td>
  </tr>
  <tr>
    <td class="tg-s6z2">1</td>
    <td class="tg-s6z2">0</td>
  </tr>
</table>

## L'opérateur 'non'

Au lieu d'écrire non(a), on pourra parfois écrire: non a.

Il est également fréquent de noter non(a) sous la forme $\overline{a}$.

  

 

## Une autre expression 

On peut également retenir la table du 'non' en remarquant que l'on a:

$$non(a) = 1-a$$

 

$$\overline{a} = 1-a$$
