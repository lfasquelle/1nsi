# Des tables de vérité avec python


On reprend les exercices [de cette page ](tables.md), mais cette fois votre mission est de dresser la table 
à l'aide d'un script python.


## Exercice  


 

On peut écrire la table de vérité du "et" avec python
par exemple avec une fonction telle que la suivante:


```python
def tableEt():
    table = ""
    for a in (False, True):
        for b in (False, True):
            table += f"{a} et {b} = {a and b}"
            table += "\n" # passage à la ligne
    return table
```

??? note "python version < 3.6"
    Rappelons qu'avec une version de python < 3.6, on ne dispose pas des f-strings.
    
    On utilisera:
    
    ```python
    def tableEt():
        table = ""
        for a in (False, True):
            for b in (False, True):
                table +=  "{} et {} = {}".format(a, b, a and b)
                table += "\n" # passage à la ligne
        return table
    ```
    Test avec:
    ```
    print(tableEt())
    ```
    Rappelons que l'on utilise ici print même dans un interpréteur afin que
    les sauts de ligne soient interprétés -- dans l'interpréteur, ils sont affichés (`\n`).
    
    On pourra également, si l'on préfère, utiliser des conversions avec str et des concaténations:
    
    ```python
    def tableEt():
        table = ""
        for a in (False, True):
            for b in (False, True):
                table +=  str(a) + " et " + str(b) + " = " + str(a and b) + "\n"
        return table
    ```
    
    
+ Testez cette fonction.
+ &Eacute;crire de même une fonction pour la table du ou.
+ &Eacute;crire une fonction pour la table du non.

??? note solution "Table du ou"

    Pour python version &ge; 3.6:
    
    ```python
    def tableOu():
        table = ""
        for a in (False, True):
            for b in (False, True):
                table += f"{a} ou {b} = {a or b}"
                table += "\n" # passage à la ligne
        return table
    ```
    
    pour versions &lt; 3.6:
    
    ```python
    def tableOu():
        table = ""
        for a in (False, True):
            for b in (False, True):
                table += "{} ou {} = {}\n".format(a, b, a or b)
        return table
    ```
    
    ou avec conversion et concaténation:
    
    ```python
    def tableOu():
        table = ""
        for a in (False, True):
            for b in (False, True):
                table += str(a) + " ou " + str(b) + " = " + str(a or b) + '\n'
        return table
    ```


??? note solution "Table du non"


    ```python
    def tableNon():
        table = ""
        for a in (False, True):
             table += f"non({a}) = {not a}"
             table += "\n" # passage à la ligne
        return table
    ```

    ou (&lt; 3.6):
    
    ```python
    def tableNon():
        table = ""
        for a in (False, True):
             table += "non({}) = {}\n".format(a, not a)
        return table
    ```
    
    ou encore:
    
    ```python
    def tableNon():
        table = ""
        for a in (False, True):
             table += "non(" + str(a) + ") = " + str(not(a)) + '\n'
        return table
    ```
    
    
    
    

## Exercice  


On constate dans l'exercice précédent que l'on écrit quasiment le même code 
pour dresser la table du "et" et pour dresser la table du "ou".

En programmation, il faut toujours éviter ce genre de répétition (pour la maintenance du code: toute modification de choix, 
par exemple dans le rendu final de la table, devra être corrigé  dans toutes les versions).

On cherche donc maintenant à écrire une fonction 

```python
def table(f):
    """
    f est une fonction de deux variables booléennes.
    
    renvoie une chaîne de caractères correspondant à la table de f.
    """
```
 
 
Par exemple, avec:

```python
def et(a,b):
    return a and b
 
def ou(a,b):
    return a or b
```


on pourra avoir la table du "et" par `table(et)` et la table du "ou" par `table(ou)`.

&Eacute;crire une telle fonction python `table`:

 



??? note solution "fonction table"

    ```
    def et(a,b):
        return a and b
        
    def ou(a,b):
        return a or b

    def table(f):
        laTable = ""
        for a in (False, True):
            for b in (False, True):
                laTable += f"f({a},{b}) = {f(a,b)}"
                laTable += "\n" # passage à la ligne
        laTable += "\n"
        return laTable
        
    print(table(et))
    print(table(ou))
    ```

    ou  (versions < 3.6)
    
    ```
    def et(a,b):
        return a and b

    def ou(a,b):
        return a or b

    def table(f):
        laTable = ""
        for a in (False, True):
            for b in (False, True):
                laTable += "f({},{}) = {}\n".format(a, b, f(a,b))
        laTable += "\n"
        return laTable

    print(table(et))
    print(table(ou))
    ```

??? note solution "fonction table, version 2"

    Un inconvénient dans la solution précédente est qu'on perd le nom de la fonction
    dont on affiche la table.
    
    En python, on peut récupérer le nom d'un objet fonction f avec `f.__name__`.
    
    Testez le code suivant:
    
    ```python
    def et(a,b):
        return a and b
        
    def ou(a,b):
        return a or b

    def table(f):
        laTable = ""
        for a in (False, True):
            for b in (False, True):
                laTable += f"{f.__name__}({a},{b}) = {f(a,b)}"
                laTable += "\n" # passage à la ligne
        laTable += "\n"
        return laTable
        
    print(table(et))
    print(table(ou))
    ```


    ou (versions < 3.6):
    
    ```python
    def et(a,b):
        return a and b

    def ou(a,b):
        return a or b

    def table(f):
        laTable = ""
        for a in (False, True):
            for b in (False, True):
                laTable += "{}({},{}) = {}\n".format(f.__name__, a, b, f(a,b))
        laTable += "\n"
        return laTable

    print(table(et))
    print(table(ou))
    ```


## Exercice  

Afficher les tables des fonctions nand et nor à l'aide de la fonction `table` définie dans l'exercice précédent.


??? note solution "Une solution"

    ```python
    def nand(a,b):
        return not(a and b)
        
    def nor(a,b):
        return not(a or b)

    def table(f):
        laTable = ""
        for a in (False, True):
            for b in (False, True):
                laTable += f"{f.__name__}({a},{b}) = {f(a,b)}"
                laTable += "\n" # passage à la ligne
        laTable += "\n"
        return laTable
        
    print(table(nand))
    print(table(nor))
    ```
