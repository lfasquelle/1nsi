# Et

En informatique, la fonction 'et' est très souvent nommée par son nom anglais: 'and'.

## La fonction 'et'.

La fonction 'et' est une fonction de $\mathbb{B}\times\mathbb{B}$ dans $\mathbb{B}$ définie par:

+ et(0; 0) = 0
+ et(0; 1) = 0
+ et(1; 0) = 0
+ et(1; 1) = 1

Ce que l'on présente en général sous forme d'un tableau:

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-uys7{border-color:inherit;text-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-uys7">a</th>
    <th class="tg-uys7">b</th>
    <th class="tg-uys7">et(a;b)</th>
  </tr>
  <tr>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">0</td>
  </tr>
  <tr>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">1</td>
    <td class="tg-uys7">0</td>
  </tr>
  <tr>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">0</td>
    <td class="tg-c3ow">0</td>
  </tr>
  <tr>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">1</td>
  </tr>
</table>

## L'opérateur 'et'

Au lieu de voir 'et' comme une fonction et d'écrire et(a; b), on utilise plutôt 'et' comme un opérateur
et on écrit "a et b".

La table du 'et':


<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-uys7{border-color:inherit;text-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-uys7">a</th>
    <th class="tg-uys7">b</th>
    <th class="tg-uys7">a et b</th>
  </tr>
  <tr>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">0</td>
  </tr>
  <tr>
    <td class="tg-uys7">0</td>
    <td class="tg-uys7">1</td>
    <td class="tg-uys7">0</td>
  </tr>
  <tr>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">0</td>
    <td class="tg-c3ow">0</td>
  </tr>
  <tr>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">1</td>
  </tr>
</table>





## En bref

On peut résumer l'opérateur 'et' par l'équivalence:

$$(a \text{ et } b = 1) \Longleftrightarrow (a,b)=(1,1)$$

Cette équivalence exprime que le seul cas donnant 1 est le cas "1 et 1". Tous les autres cas donnent 0.


## Une autre expression.

En interprétant 0 et 1 comme des entiers naturels, 
on peut également retenir la table du 'et' en remarquant que l'on a:

$$a \text{ et } b = a\times b$$

Ou encore:

$$a \text{ et } b = \min(a; b)$$

   
