# Les booléens
 
 
 

Un booléen peut avoir deux valeurs: **0** ou **1**.

On pourra interpréter ces deux valeurs par **faux** et **vrai**.

On notera $\mathbb{B} = \lbrace 0; 1\rbrace$ l'ensemble des booléens.

 
On utilisera aussi la notation $\mathbb{B}\times\mathbb{B}$, c'est l'ensemble des couples d'éléments de $\mathbb{B}$:

$$\mathbb{B}\times\mathbb{B} = \lbrace (0; 0), (0; 1), (1; 0), (1; 1) \rbrace$$
 

!!! note "George Boole"
    Le mot "booléen" vient du nom de George Boole (1815-1864), logicien, mathématicien
    et philosophe britannique.
    [Article wikipedia](https://fr.wikipedia.org/wiki/George_Boole)
    
