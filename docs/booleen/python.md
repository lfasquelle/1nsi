# Les booléens avec le langage Python


## Le type bool


Entrons `type(2==3)` dans un interpréteur python.
On obtient:

```
>>> type(2 == 3)
<class 'bool'>
```

Et:

```
>>> 2 == 3
False
>>> 2 < 3
True
```

Les tests utilisés usuellement dans un `if` sont des booléens. Ils peuvent avoir la valeur True ou la valeur False.


## Et, ou, non

et, ou, non sont nommés and, or, not en python:


```
>>> True and False
False
>>> (2 == 3) or (2 < 3)
True
>>> not(2 == 3)
True
```

