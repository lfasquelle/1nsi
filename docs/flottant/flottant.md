# Les flottants

## En base 10

Nous appellerons flottants (p, e<sub>min</sub>, e<sub>max</sub>) les nombres de la forme
s &times; m &times; 10<sup>e</sup>
où:

+ s est le signe (+ ou -), 
+ m la mantisse: nombre à p chiffres, avec **exactement un chiffre (non nul) avant la virgule**,
+ e est l'exposant avec e<sub>min</sub> &le; e &le; e<sub>max</sub>.


??? note 
    Il s'agit donc de nombres pour lesquels nous imposons l'écriture en notation scientifique.  
    Les nombres de chiffres pour m et pour e sont bornés: il s'agit   de tenir compte
    des contraintes des nombres en machine qui seront toujours représentés avec le même nombre, nécessairement fini,
    de bits     (sur les machines actuelles, usuellement 64 bits).
    
    
??? note
    La définition donnée ici n'est pas tout à fait la définition générique des flottants, 
    mais nous nous  tiendrons 
    à cette approche pour simplifier.
    
    
    
## Exercice 1

Quels sont les flottants (p = 2, e<sub>min</sub> = -1, e<sub>max</sub> = 1)?

??? solution "Réponse"

    Les mantisses possibles: 
    
    + 1,0; 1,1; 1,2; ...; 1,9
    + 2,0; 2,1; 2,2; ..., 2,9
    + ...
    + 9,0; 9,1; 9,2; ...; 9,9
    
    Les exposants possibles sont -1, 0 et 1.
    
    Les flottants (p = 2, e<sub>min</sub> = -1, e<sub>max</sub> = 1) sont donc:
    
    + Les nombres:
        + 0,10; 0,11; 0,12; ...; 0,19 (c'est à dire 1,0&times;10<sup>-1</sup>; 1,1&times;10<sup>-1</sup>; 1,2&times;10<sup>-1</sup>; ...; 1,9&times;10<sup>-1</sup>)
        + 0,20; 0,21; 0,22; ..., 0,29 (c'est à dire 2,0&times;10<sup>-1</sup>; 2,1&times;10<sup>-1</sup>; 2,2&times;10<sup>-1</sup>; ...; 2,9&times;10<sup>-1</sup>)
        + ...
        + 0,90; 0,91; 0,92; ...; 0,99 (c'est à dire 9,0&times;10<sup>-1</sup>; 9,1&times;10<sup>-1</sup>; 9,2&times;10<sup>-1</sup>; ...; 9,9&times;10<sup>-1</sup>)
    
    + Les nombres:
        + 1,0; 1,1; 1,2; ...; 1,9 (c'est à dire 1,0&times;10<sup>0</sup>; 1,1&times;10<sup>0</sup>; 1,2&times;10<sup>0</sup>; ...; 1,9&times;10<sup>0</sup>)
        + 2,0; 2,1; 2,2; ..., 2,9(c'est à dire 2,0&times;10<sup>0</sup>; 2,1&times;10<sup>0</sup>; 2,2&times;10<sup>0</sup>; ...; 2,9&times;10<sup>0</sup>)
        + ...
        + 9,0; 9,1; 9,2; ...; 9,9 (c'est à dire 9,0&times;10<sup>0</sup>; 9,1&times;10<sup>0</sup>; 9,2&times;10<sup>0</sup>; ...; 9,9&times;10<sup>0</sup>)
    
    + Les nombres:
        +  10;  11; 12; ...;  19 (c'est à dire 1,0&times;10<sup>1</sup>; 1,1&times;10<sup>1</sup>; 1,2&times;10<sup>1</sup>; ...; 1,9&times;10<sup>1</sup>)
        +  20;  21;  22; ...,  29 (c'est à dire 2,0&times;10<sup>1</sup>; 2,1&times;10<sup>1</sup>; 2,2&times;10<sup>1</sup>; ...; 2,9&times;10<sup>1</sup>)
        + ...
        +  90;  91; 92; ...;  99 (c'est à dire 9,0&times;10<sup>1</sup>; 9,1&times;10<sup>1</sup>; 9,2&times;10<sup>1</sup>; ...; 9,9&times;10<sup>1</sup>)
    
    + Et les opposés de ces nombres.
    
    
    
## Exercice 2


Observez les nombres flottants (p = 2, e<sub>min</sub> = -1, e<sub>max</sub> = 1).
Que pouvez-vous dire de l'écart entre un nombre et le nombre suivant?


??? note "nombre suivant"
    On peut déjà remarquer que la notion de "nombre suivant" a un sens alors qu'elle n'en a pas avec 
    les nombres réels.
    
    
??? solution "Remarque"

    Raisonnons uniquement sur les positifs.
    
    Entre deux "petits" nombres consécutifs (par exemple entre 0,10 et 0,11), l'écart est de 0,01.
    
    Entre deux "grands" nombres (par exemple 90 et 91), l'écart est de 1.
    

 

## Conséquence

A partir de la remarque de l'exercice précédent, on en déduit quelques conséquences pour les nombres en machine.

Si l'on doit représenter tous les réels par les flottants précédents, 
tous les réels de l'intervalle [0,10; 0,105[ (ouvert ou fermé à droite, choix possible) seront
représentés par le même flottant (à savoir le flottant 1,0&times;10<sup>-1</sup>).

Et tous les réels de l'intervalle [10; 10,5[ seront représentés par le flottant 1,0&times;10<sup>1</sup>.
Ainsi, suivant qu'il s'agit de nombres proches de 0 ou éloignés de 0, les nombres auront pour représentant dans les 
flottants un nombre plus ou moins éloigné de lui. Ou en d'autres termes, 
un même flottant sera représentant de chacun des réels d'un intervalle
plus ou moins grand.


!!! note
    La remarque porte sur l'écart absolu (valeur absolue de la différence entre le nombre et son représentant flottant).
    Les écarts relatifs (valeur absolue du quotient écart absolu/ nombre) sont par contre "un peu plus constants" 
    pour les ensembles de flottants usuellement utilisés en machine 
    (et c'est là une propriété importante pour le calcul scientifique en machine).
    
    
    
La conséquence la plus importante est que l'on ne peut pas attendre que les opérations sur les flottants aient les
mêmes propriétés que les opérations sur les réels.
Nous avons déjà constaté cela. Par exemple la non commutativité avec:

```
>>> 1 + 10**(-16) -1
0.0
>>> 1 - 1 + 10**(-16)
1e-16
```


+ Pouvez-vous expliquer la différence de résultat dans les deux lignes précédentes?

??? solution "Une explication"

    On a:
    
    ```
    >>> 1+10**(-16)
    1.0
    ```
    
    tandis que 
    
    ```
    >>> 0 + 10**(-16)
    1e-16
    ```

    Sans rentrer dans les détails, on peut reprendre ce qui est exposé plus haut:
    
    + `10**(-16)` est proche de 0 et aura un représentant flottant assez fidèle,
    + `1 + 10**(-16)` est plus éloigné de 0 et les réels entre `1` et `1 + 10**(-16)` auront tous 1 pour
    représentants.
    
    Dans `1 + 10**(-16) -1`, on évalue `1 + 10**(-16)` qui donne 1 puis on évalue `1 - 1`, on obtient `0`.
    
    Dans `1 - 1 + 10**(-16)`, on évalue `1 - 1` qui donne 0 puis on évalue `0 + 10**(-16)`, on obtient
    `10**(-16)`.

    


## En base 2

Nous appellerons flottants (p, e<sub>min</sub>, e<sub>max</sub>) les nombres de la forme
s &times; m &times; 2<sup>e</sup>
où:

+ s est le signe (+ ou -), 
+ m la mantisse: nombre à p bits, avec exactement un bit (non nul, donc égal à 1) avant la virgule,
+ e est l'exposant avec e<sub>min</sub> &le; e &le; e<sub>max</sub>.



    
C'est  sous cette forme (flottants en base 2) que les nombres seront en général représentés en machine.

En machine, que valent p et  e<sub>min</sub>, e<sub>max</sub>?
Le choix dépend de normes qui peuvent évoluer (pour améliorations, ou pour s'adapter aux nouvelles 
capacités des machines, etc...)  
Voir en page suivante, la norme IEEE 754.



## Un exemple

Prenons p = 6 et e<sub>min</sub> = -5, e<sub>max</sub> = 5.


Nous avons vu que 0,1<sub>dix</sub> =   0,0 0011 0011 0011 0011... <sub>2</sub>  
où la séquence 0011 se répète indéfiniment.

Si nous devons utiliser cet ensemble de flottants (p = 6 , e<sub>min</sub> = -5, e<sub>max</sub> = 5) 
pour représenter l'ensemble des réels, le  nombre 0,1<sub>dix</sub> 
sera représenté par le flottant 1,1 0011 &times; 10<sup>-100</sup> (écriture binaire).


De même  0,2<sub>dix</sub>  =  0, 0011 0011 0011 0011...<sub>2</sub> où la séquence 0011 se répète indéfiniment.  
0,2<sub>dix</sub> sera représenté par 1,1 0011 &times; 10<sup>-11</sup> (écriture binaire).


Cherchons à   additionner 0,1<sub>dix</sub> + 0,2<sub>dix</sub>.
On ne dispose, dans la machine, que des représentants flottants. 
Ce sont donc ces représentants que l'on additionne et non les nombres réels.

Sans rentrer dans les détails des algorithmes mis en oeuvre en machine, effectuons cela "à la main":


1,1 0011 &times; 10<sup>-100</sup> + 1,1 0011 &times; 10<sup>-11</sup>  <br>
= 0,110011 &times; 10<sup>-11</sup> + 1,1 0011 &times; 10<sup>-11</sup> (écriture binaire).
 
 
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-cly1{text-align:left;vertical-align:middle}
.tg .tg-0lax{text-align:left;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-0lax"></th>
    <th class="tg-cly1">0</th>
    <th class="tg-cly1">,</th>
    <th class="tg-cly1">1</th>
    <th class="tg-0lax">1</th>
    <th class="tg-0lax">0</th>
    <th class="tg-0lax">0</th>
    <th class="tg-0lax">1</th>
    <th class="tg-0lax">1</th>
  </tr>
  <tr>
    <td class="tg-0lax">+</td>
    <td class="tg-cly1">1</td>
    <td class="tg-cly1">,</td>
    <td class="tg-cly1">1</td>
    <td class="tg-0lax">0</td>
    <td class="tg-0lax">0</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax"></td>
  </tr>
  <tr style="border-top:5px solid black;">
    <td class="tg-0lax">1</td>
    <td class="tg-cly1">0</td>
    <td class="tg-cly1">,</td>
    <td class="tg-cly1">0</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">0</td>
    <td class="tg-0lax">0</td>
    <td class="tg-0lax">1</td>
  </tr>
</table>

Le résultat de cette addition est donc 10,011001 &times; 10<sup>-11</sup>.

Quel flottant correspond à cela? Le flottant 1,00110 &times; 10<sup>-10</sup>.

### A vous

+ Donner la valeur décimale de 1,00110 &times; 10<sup>-10</sup>.



??? solution "Valeur décimale de 1,00110 &times; 10<sup>-10</sup>"

    1,00110 &times; 10<sup>-10</sup> (binaire)
    s'écrit en décimal $\left(1 + \frac{1}{2^3} + \frac{1}{2^4}\right) \times 2^{-2}$, soit
    $\frac{1}{2^2} + \frac{1}{2^5} + \frac{1}{2^6}$, soit $\frac{19}{64} \approx 0,296875$.
    
+ Avec ce système de flottants a-t-on 0.1 + 0.2 = 0.3?


   
    
??? solution "0.1 + 0.2 = 0.3?"

    O,3 s'écrit en binaire 0, 01 0011 0011 0011... (répétition de 0011 indéfiniment).
    Avec le système de flottants choisi, 0.3 est donc représenté par 1,00110.10<sup>-10</sup>.
    Il semble donc que 0.1 + 0.2 soit égal à 0.3 
    (c'est à dire que le flottant associé à 0.1 + 0.2 soit bien le même que 
    le flottant associé à 0.3) avec ce système de représentants des nombres.
    

+ Et avec le système de flottants p=5 (e<sub>min</sub>=-5, e<sub>max</sub>=5), aura-t-on   0.1+0.2 = 0.3?


??? solution "Une réponse"

    0,1<sub>dix</sub> =   0,0 0011 0011 0011 0011... <sub>2</sub>  = 1, 1 0011 0011... &times; 10<sup>-100</sup>  
    0,2<sub>dix</sub>  =  0, 0011 0011 0011 0011...<sub>2</sub> = 1, 1 0011 0011...&times; 10<sup>-11</sup>  
    0,3<sub>dix</sub>  =  0, 01 0011 0011 0011...<sub>2</sub> = 1, 0011 0011 0011 &times; 10<sup>-10</sup>
    
    Si 0,1<sub>dix</sub> est représenté par 1, 1 001 &times; 10<sup>-100</sup> 
    et 0,2<sub>dix</sub> par 1, 1 001&times; 10<sup>-11</sup>,
    la somme (0, 11001 + 1,1001)&times; 10<sup>-11</sup> donne 10,01011&times; 10<sup>-11</sup>,
    représenté par le flottant 1,0010&times; 10<sup>-10</sup> alors que 0,3<sub>dix</sub> sera plutôt représenté par 1, 0011&times; 10<sup>-10</sup>.
    On n'aurait donc pas avec ce système 0.1+0.2 = 0.3.
    
    On peut aussi représenter (pour arrondir au plus proche) 0,1<sub>dix</sub> par 1, 1 010 &times; 10<sup>-100</sup>
    et  0,2<sub>dix</sub> par 1, 1 010 &times; 10<sup>-11</sup>.
    La somme (0,11010 + 1,1010)&times; 10<sup>-11</sup> donne 10,01110&times; 10<sup>-11</sup>
    soit   1,001110&times; 10<sup>-11</sup>, qui serait représenté par le flottant 1,0100&times; 10<sup>-10</sup> (en arrondissant au plus proche),
    ce qui ne correspond pas non plus au flottant représentant 0,3<sub>dix</sub> qui serait 1, 0011&times; 10<sup>-10</sup>.
    
    Avec p=5, il semble que l'on n'ait pas 0.1 + 0.2 = 0.3.
    
    Avec les systèmes de flottants en machine, il faudrait rentrer un peu dans les détails des représentations
    et des algorithmes mis en oeuvre (arrondi, addition...) mais l'essentiel du principe conduisant
    à 0.1 + 0.2 &ne; 0.3 est là: 
    
    + on représente les valeurs réelles par une petite quantité de nombres, 
    + on représente avec un nombre fini de chiffres 
    des nombres nécessitant une infinité (ou un grand nombre) de chiffres,
    + on utilise plusieurs bases.
        
    Un changement dans certains choix peut annuler certains "problèmes" mais en présentera nécessairement d'autres.
    
