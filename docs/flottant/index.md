# Les flottants


 


Nous avons déjà vu le comportement suivant des flottants:

```
>>> 0.2 + 0.1 == 0.3
False
```

L'objectif est ici d'essayer de comprendre ce phénomène en précisant un peu ce que sont les flottants.

Rappelons qu'une conséquence est qu'il ne faut pas utiliser un test '==' sur des flottants.  
On utilisera à la place  par exemple 
[isclose](https://docs.python.org/fr/3/library/cmath.html?highlight=close#cmath.isclose) 
pour tester la proximité des deux flottants. 
