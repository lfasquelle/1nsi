# La norme IEEE 754


!!! note 
    La lecture de cette page est facultative. Elle peut vous permettre de mieux saisir les problèmes
    de calcul sur flottants en précisant un peu les représentations et des étapes de calcul en machine.


Les flottants sont représentés en général en machine suivant [la norme IEEE 754](https://fr.wikipedia.org/wiki/IEEE_754).

Intéressons nous par exemple au format binary64 (ce n'est pas le seul défini par cette norme).


Les flottants dans ce format sont codés sur 64 bits.

## Quelques  codes spéciaux.



Un certain nombre de codes sont réservés pour représenter des cas *particuliers*. 

### 0 11111111111 0000000000000000000000000000000000000000000000000000

Ce code (0 suivi de onze 1, suivis de cinquante-deux 0) représente $+\infty$ (+inf).


### 1 11111111111 0000000000000000000000000000000000000000000000000000

Ce code (1 suivi de onze 1, suivis de cinquante-deux 0) représente $-\infty$ (-inf).


### 1 11111111111 1111111111111111111111111111111111111111111111111111


Ce code (soixante-quatre bits à 1) représente NaN (not a number).



### 0 00000000000 0000000000000000000000000000000000000000000000000000


Ce code (soixante-quatre bits à 0) représente 0.

### 1 00000000000 0000000000000000000000000000000000000000000000000000



Ce code (Un bit à 1 suivi de soixante-trois bits à 0) représente également 0 (-0).


## Les autres cas

Pour le cas *général*:

+ Le premier bit (à gauche) représente le signe: 0 pour un positif, 1 pour un négatif.

+ Les 11 bits suivants représentent l'exposant. Pour connaître la valeur d'exposant ainsi 
représentée, on traduit la suite de bits par un entier (écrit en binaire) puis on enlève 1023.

+ Les 52 bits restants correspondent aux bits se trouvant après la virgule du flottant. C'est la mantisse
privée du 1 se trouvant avant la virgule (ce 1 précédant la virgule
 n' a pas besoin d'être représenté puisqu'il s'agit toujours
de la valeur 1).


## Exercice 1.

Que représente le code 1 10001000110 1001 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 suivant
le format binary64 de la norme IEEE 764?

??? solution "Réponse"

    + Le bit à gauche vaut 1: le nombre représenté est négatif.
    + Les 11 bits de l'exposant sont 100 0100 0110.
    On a 10001000110<sub>deux</sub> = 2<sup>10</sup> + 2<sup>6</sup> + 2<sup>2</sup> + 2 = 1094.
    On enlève 1023: l'exposant est donc égal à 71.
    + La mantisse est m = 1,1001 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
    soit $1+\frac{1}{2}+\frac{1}{2^4}= 1,5625$.
    
    Le nombre représenté est donc le nombre $-1,5625\times 2^{71} \approx -3,689\times 10^{21}$.
    
    
## Et dans l'autre sens?

Si l'on dispose du code de soixante-quatre bits représentant un flottant sous le format binary64 de la norme IEEE 754,
il est facile d'en déduire le nombre représenté (il suffit d'appliquer la définition de ce format comme
présenté plus haut).

Ce qui est moins évident est, connaissant un nombre réel, de savoir par quel flottant il va être représenté en 
machine. Rappelons notamment qu'un même flottant représente en fait une infinité de réels distincts.   
Il y a également beaucoup de réels qui ne sont tout simplement pas représentables en machine suivant ce format 
(trop grand, trop proche de zéro, ...).


## Exercice 2.

Par quel code le nombre 7,0 est-il représenté dans le format binary64 de la norme IEEE 754?

??? solution "Réponse"

    + Le bit le plus à gauche sera un 0 puisque l'on code un positif.
    + 7 = 111<sub>deux</sub> soit (en binaire) 1.11 &times;  10<sup>10</sup>.
    L'exposant vaut 2, on lui ajoute 1023, ce qui donne 1025 et on code 1025 en binaire sur onze bits:
    1025 = 100 0000 0001.  
    La mantisse est 1.11, elle est codée par 1100...0 (deux 1 suivis de cinquante 0).
    
    
## Exercice 3.


Avec [ce convertisseur en ligne](http://weitz.de/ieee/), vous pouvez coder 0.1, 0.2 et la somme 0.1 + 0.2.
Vous pourrez ainsi constater que le code obtenu pour 0.1 + 0.2 n'est pas tout à fait le même que le code
obtenu pour 0.3.


!!! important
    Insistons encore une fois, le fait d'avoir certains tests comme `0.1 + 0.2 == 0.3`  à False est tout
    à fait inévitable quelque soit la norme choisie: nous devons représenter l'infinité des nombres réels
    sur un nombre fini de bits, donc par un nombre fini de nombres, eux-mêmes présentant
    un nombre fini de chiffres... Ceci ne peut se faire sans perte d'informations.

    
