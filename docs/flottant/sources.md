# Sources



## Court historique

+ [Les calculs sur ordinateurs de plus en plus fiables et sûrs](https://hal.inria.fr/hal-00813205/document)

## Les flottants

+ [cours sur les flottants](https://hal.inria.fr/inria-00071477/document)

+ [cours sur les flottants](https://hal.archives-ouvertes.fr/ensl-00086707)

## IEEE 754

+ [démarche de conversion](https://www.wikihow.com/Convert-a-Number-from-Decimal-to-IEEE-754-Floating-Point-Representation)

+ [démarche de conversion](http://class.ece.iastate.edu/arun/Cpre305/ieee754/ie4.html)

## Des convertisseurs IEEE 754


+ [convertisseur](http://www.binaryconvert.com/convert_float.html)

+ [convertisseur](https://www.h-schmidt.net/FloatConverter/IEEE754.html)

+ [convertisseur](https://baseconvert.com/ieee-754-floating-point)

+ [convertisseur](http://www.binaryconvert.com/convert_double.html)


+ [convertisseur avec opérations](http://weitz.de/ieee/)


