# QCM


## QCM 1


Voici une fonction python:

```python
from math import sqrt, floor
def ppee(x):
    """
    x -- float
    
    renvoie le plus petit entier n > 0 tel que n*x est entier
    """
    n = 1
    while n*x != floor(n*x):
        n += 1
    return n
```


!!! note

    On rappelle que la fonction `floor` est la fonction partie entière.

On teste cette fonction avec:

```
ppee(sqrt(2))
```

On obtient:

- [ ] rien... le programme tourne indéfiniment
- [ ] une erreur
- [ ] un entier
- [ ] aucune des réponses ci-dessus n'est valable.


??? solution "Réponse"


    - [ ] rien... le programme tourne indéfiniment
    - [ ] une erreur
    - [X] un entier
    
    D'un point de vue algorithmique, si l'on manipule des **nombres réels**, on s'attend à une boucle infinie.
    En effet $\sqrt{2}$ est un irrationnel (cf programme de mathématiques de la classe de seconde), 
    ce qui signifie que $\sqrt{2}$ ne peut pas s'écrire sous la forme $\frac{a}{b}$  où a et b sont des entiers, et donc 
    qu'il ne peut exister d'entier b tel que $b\sqrt{2}$ soit entier.
    
    Toutefois, il s'agit ici d'un programme en machine, on manipule donc des flottants et non des réels.
    Et tout flottant ne présente qu'un nombre fini de bits. Autrement dit, tout flottant représente en fait un décimal.
    Lorsqu'on écrit `sqrt(2)`, on obtient en machine une approximation décimale de $\sqrt{2}$ et non le nombre
    réel $\sqrt{2}$. Du coup, il existe effectivement un entier b tel que `b * sqrt(2)` soit entier.
    
    Un test en machine donne l'entier 93222358.
    
    
    
## QCM 2

Un réel x s'écrit 11,11 en base deux.

Ce nombre a pour écriture en base dix:

- [ ] 3,3
- [ ] 3,75
- [ ] 75,75
- [ ] 75,3

??? solution "Réponse"



    - [ ] 3,3
    - [X] 3,75
    - [ ] 75,75
    - [ ] 75,3
    
    En effet 11,11<sub>deux</sub> =  $1\times 2^1 + 1\times 2^0 + \frac{1}{2^1} + \frac{1}{2^2}$ = 3,75. 
    
    
    
    
## QCM 3

Voici un code Python:

```python
def nombre_etapes(borne):
    nombre = 0
    compteur = 0
    while nombre != borne:
        nombre = nombre + 0.1
        compteur = compteur + 1
    return compteur
```


L'exécution de 

```
>>> nombre_etapes(1.0)
```

donne: 

- [ ] 10
- [ ] 11
- [ ] une erreur
- [ ] aucune des réponses précédentes n'est correcte.

??? solution "Réponse"

    - [ ] 10
    - [ ] 11
    - [ ] une erreur
    - [X] aucune des réponses précédentes n'est correcte.
    
    Le programme va tourner indéfiniment. Les additions se font avec le représentant de 0.1 en machine,
    qui est, on l'a vu, non égal à 0.1. 
    
    
    Avec dix termes 0.1:
    
    ```
    >>> 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1
    0.9999999999999999
    ```
    
    Avec onze termes 0.1:
    
    ```
    >>> 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1
    1.0999999999999999
    ```
    
    La variable nombre n'aura jamais la valeur 1.0.
    
    !!! important
        Encore une fois, n'utilisez pas `==` ou `!=` avec les flottants.
         
         
## QCM 4



Après le code:

```
>>> a = 1 + (-1) + 10**(-40)
>>> b = 10**(-40) + 1 + (-1)
```

 


- [ ] les valeurs désignées par a et b sont égales.
- [ ] les valeurs désignées par a et b ne sont pas égales.


??? solution "Réponse"

    - [ ] les valeurs désignées par a et b sont égales.
    - [X] les valeurs désignées par a et b ne sont pas égales.
    
    Dans le cas 1, le calcul  `1-1+ 10**(-40)` est `0 + 10**(-40) = 10**(-40)`.
    
    Dans le cas 2, le calcul  `1/10**(-40) + 1 -1` est  `1 -1 = 0` (car 1/10**(-40) + 1  donne 1 avec 
    les problèmes d'arrondis liés à la représentation des flottants).
