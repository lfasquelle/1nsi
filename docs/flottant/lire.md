# De l'écriture en base b à l'écriture en base 10


!!! important
    Comme dans les chapitres précédents, une notation non indicée par une base représente une écriture
    dans la base usuelle (base 10).  
    Ainsi 101 est le nombre de [dalmatiens](https://disneyhd.tk/mov-12) 
    tandis que 101<sub>2</sub> est égal à 5.


## Décomposition d'un décimal écrit en base 10

On rappelle via un exemple  ce que signifie l'écriture décimale usuelle d'un décimal.

 

L'écriture 123,425 signifie:  
1 centaine, 2 dizaines, 3 unités, 4 dixièmes, 2 centièmes, 5 millièmes.  
C'est à dire:  
123,425 = 1 &times; 10<sup>2</sup> + 2 &times; 10<sup>1</sup> + 3 &times; 10<sup>0</sup> + 4&times; 10<sup>-1</sup> 
+ 2&times; 10<sup>-2</sup> + 5&times; 10<sup>-3</sup>, 
ce qui s'écrit aussi $123,425 = 1\times 10^2 + 2 \times  10^1 + 3\times  10^0 + \frac{4}{10} + \frac{2}{10^2} + \frac{5}{10^3}$.


## De la base 2 vers la base 10


De façon similaire, l'écriture 101,11<sub>deux</sub> s'interprète comme suit:

101,11<sub>deux</sub> = 1 &times; 2<sup>2</sup> + 0 &times; 2<sup>1</sup> + 1 &times; 2<sup>0</sup> +
1 &times; 2<sup>-1</sup> + 1 &times; 2<sup>-2</sup>, ou encore
$101,11_{\text{deux}} = 4 + 0 + 1 + \frac{1}{2} + \frac{1}{4} = 5,75$.


## De la base 16 vers la base 10

Et de façon analogue, l'écriture 2a,b<sub>seize</sub> se lit ainsi:

2a,b<sub>seize</sub> = 2 &times; 16<sup>1</sup> + 10 &times; 16<sup>0</sup> + 11 &times; 16<sup>-1</sup>.
Ou encore: $2a,b_{\text{seize}} = 2 \times 16 + 10 + \frac{11}{16}$. 
Soit 2a,b<sub>seize</sub> = 42,6875.


## Exercice 

Donner l'écriture en base dix des nombres suivants:

+ a = 111,11<sub>2</sub>
+ b = 1010,1001<sub>2</sub>
+ c = 0,011<sub>2</sub>


??? solution "a"

    $a = 4+2+1 + \frac{1}{2} + \frac{1}{4}$, soit $a = 7,75$.


??? solution "b"

    $b = 8 + 2 + \frac{1}{2} + \frac{1}{16}$, soit $b = 10,5625$.


??? solution "c"

    $c = 0 + \frac{1}{4} + \frac{1}{8}$, soit $c =  0,375$.
