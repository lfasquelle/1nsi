# De la base 10 vers la base b


Comment obtenir les chiffres binaires d'un réel positif?

## Les chiffres en base 10

Comment obtenir séquentiellement les chiffres de 8643,5712?

### Les chiffres de la partie entière

On a déjà rappelé comment on obtient de façon séquentielle les chiffres de la partie entière par une 
**succession de divisions par 10** (divisions en cascade):

![](images/cascade1.png)

### Les chiffres de la partie décimale


De façon analogue, les chiffres de la partie décimale s'obtiennent par une **succession de multiplications par 10**:
on isole la partie entière à chaque étape (c'est le chiffre suivant) et on multiplie par 10 la partie décimale (nombre
de la forme 0,...).  
Lorsque le "reste décimal" vaut 0, on stoppe.

![](images/multiplicationCascade.png)

ou encore (en ligne):

+ 0,5712 &times; 10 =  **5** + 0,712
+ 0,712 &times; 10 =  **7** + 0,12
+ 0,12 &times; 10 =  **1** + 0,2
+ 0,2 &times; 10 =  **2** + 0,0

Les chiffres sont 5, 7, 1, 2.


!!! important 
    Les chiffres de la partie décimale sont obtenus dans l'ordre d'écriture usuelle lors des multiplications
    succesives.  
    Les chiffres de la partie entière sont obtenus "à contre-sens" de l'écriture usuelle lors des divisions 
    successives.



## Les chiffres en base 2

On procède de la même façon. 

Soit x un réel positif.

+ Les chiffres binaires de la partie entière  de x s'obtiennent par des divisions successives (divisions en cascade) 
par 2.
+ Les chiffres binaires de la partie décimale s'obtiennent par des multiplications successives par 2.


### Chiffres binaires de 3,25.

+ On écrit d'abord 3 en binaire comme on a déjà appris à le faire: 3 = 11<sub>2</sub>.
+ On écrit ensuite 0,25 en binaire par des multiplications successives par 2:  
![](images/multiplicationCascade2.png)
On a donc 0,25 = 0,01<sub>2</sub>

D'où 3,25 = 11,01<sub>2</sub>.

### Chiffres binaires de 5,625

+ On écrit la partie entière en binaire: 5 = 101<sub>2</sub>
+ On écrit ensuite la partie décimale en binaire:
    + 0,625 &times; 2 = 1,25 =  **1** + 0,25
    + 0,25 &times; 2 = 0,5 = **0** + 0,5
    + 0,5 &times; 2 =  1 = **1** + 0  
On a donc 0,625 = 0,101<sub>2</sub>

Et 5,625 = 101,101<sub>2</sub>. 





## Exercice


Donner l'écriture binaire des nombres suivants:

+ a = 7,125


??? solution "a"

    + 7 = 111<sub>2</sub>
    + Partie décimale:
        + 0,125 &times; 2 = 0,25 = **0** + 0,25
        + 0,25 &times; 2 = 0,5 = **0** + 0,5
        + 0,5 &times; 2 = 1 = **1** + 0
        
    Donc  0,125 = 0,001<sub>2</sub> et 7,125 = 111,001<sub>2</sub>.
    
    
    
+ b = 13,0625



??? solution "b"

    + 13 = 1101<sub>2</sub>
    + Partie décimale:
        + 0,0625 &times; 2 = 0,125 = **0** + 0,125
        + 0,125 &times; 2 = 0,25 = **0** + 0,25
        + 0,25 &times; 2 = 0,5 = **0** + 0,5
        + 0,5 &times; 2 = 1 = **1** + 0

    Donc 0,0625 = 0,0001<sub>2</sub> et 13,0625 = 1101,0001<sub>2</sub>


+ c = 4,75


??? solution "c"

    + 4 = 100<sub>2</sub>
    + Partie décimale:
        + 0,75 &times; 2 = 1,5 = **1** + 0,5
        + 0,5 &times; 2 = 1 = **1** + 0
        
    Donc 0,75 = 0,11<sub>2</sub> et 4,75 = 100,11<sub>2</sub>.
    
    
    
??? note "Un convertisseur en ligne"

    Vous pouvez créer vous-même d'autres exercices pour vous entraîner et 
    vérifier vos calculs avec un convertisseur.  
    Par exemple, [ce convertisseur en ligne](https://www.ma-calculatrice.fr/convertir-binaire-hexadecimal.php).
