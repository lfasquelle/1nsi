# Des écritures qui ne se terminent pas




## &Eacute;criture décimale avec une infinité de chiffres 

Les nombres réels non décimaux ont une écriture décimale avec une infinité de chiffres après la virgule.  
C'est le cas de $\pi$, $\sqrt{2}$, $\frac{1}{3}$ par exemple.

Ces nombres réels auront nécessairement aussi une écriture en base deux avec une infinité de décimales.

La conséquence est que tous ces nombres **ne peuvent pas être représentés** (de manière exacte) 
par des flottants puisque par nature un nombre
en machine ne présente qu'un nombre fini de chiffres.




## &Eacute;criture décimale finie

Mais le problème ne s'arrête pas là: il existe également des nombres qui sont décimaux (donc ont une écriture décimale
présentant un nombre fini de chiffres) mais qui n'ont pas un nombre fini de chiffres en base 2.

Ces nombres ne peuvent donc pas non plus être représentés de façon exacte en machine.


### Exercice 1

Donner l'écriture en base deux du nombre a = 0,1

??? solution "Réponse"

    + 0,1 &times; 2 = 0,2 = **0** + 0,2
    + 0,2 &times; 2 = 0,4 = **0** + 0,4
    + 0,4 &times; 2 = 0,8 = **0** + 0,8
    + 0,8 &times; 2 = 1,6 = **1** + 0,6
    + 0,6 &times; 2 = 1,2 = **1** + 0,2
    + 0,2 &times; 2 = 0,4 = **0** + 0,4
    + 0,4 &times; 2 = 0,8 = **0** + 0,8
    + 0,8 &times; 2 = 1,6 = **1** + 0,6
    + 0,6 &times; 2 = 1,2 = **1** + 0,2
    
    A ce stade, on voit que l'on retombe sur 0,2: on va donc nécessairement réécrire les mêmes lignes 
    que précédemment. Et on retombera sur 0,6 puis à nouveau sur 0,2.
    
    On va ainsi boucler indéfiniment.

    Ainsi 0,1 =  0,0 0011 0011 0011 0011... <sub>2</sub>  où la séquence 0011 se répète inddéfiniment.
    

    
    
    
    
    
## Conséquence

On voit avec l'exercice précédent que même avec un seul chiffre après la virgule en décimal, il peut y avoir
une infinité de chiffres apèrs la virgule en binaire.

On a donc déjà une clef essentielle pour comprendre le code:

```
>>> 0.2 + 0.1 == 0.3
False
``` 
 
Pour additionner 0.1 et 0.2, ces deux nombres sont d'abord traduit en base 2.  
Comme 0.1 ne peut pas en fait être traduit de façon exacte en base 2 (puisqu'en machine, tout objet ne présente
qu'un nombre fini de 0 et de 1), on doit s'attendre à ce que des calculs faisant intervenir un tel nombre mène
à des erreurs dues à ce changement de base.



!!! important

    Lorsqu'on entre 0.1, il est traduit en flottant en base deux.  
    Conséquence -- la demande de l'affichage de 0.1 avec 40 décimales
    ne donne  pas que des 0 (un arrondi a nécessairement lieu puisque
    0.1 en binaire a une infinité de chiffres et qu'une machine n'en stocke
    évidemment qu'un nombre fini)
    
    ```
    >>> print("{:.40f}".format(0.1))
    0.1000000000000000055511151231257827021182
    ```
    
    On peut aussi   utiliser le 
    [module decimal](https://docs.python.org/fr/3/library/decimal.html)
    pour ce même constat:
    
    ```
    >>> from decimal import Decimal
    >>> Decimal(0.1)
    Decimal('0.1000000000000000055511151231257827021181583404541015625')
    ```
    
    
    Sans ce changement de base, le résultat de 0.1 + 0.2 pourrait être correct:
    
    ```
    >>> from decimal import Decimal
    >>> Decimal('0.1') + Decimal('0.2') == Decimal('0.3')
    True
    ```


## Exercice 2

Donner l'écriture binaire de $\frac{1}{3}$.


??? solution "Réponse"

    + $\frac{1}{3} \times 2$ =  $\frac{2}{3}$ = **0** + $\frac{2}{3}$  = **0** + 0.6666...
    + $\frac{2}{3} \times 2$ =  $\frac{4}{3}$ = **1** + $\frac{1}{3}$ = **1** + 0.3333...
    + et on repart sur 1/3...
    
    D'où l'écriture binaire de 1/3:  0, 01 01 01 01 01...<sub>2</sub> où 01 se répète indéfiniment.

