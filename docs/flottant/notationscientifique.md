# Notation scientifique


## En base 10

Un nombre réel x sera dit écrit en notation scientifique en base 10 lorsqu'il est écrit sous la forme
a&times; 10<sup>n</sup> où a est un réel de l'intervalle [1; 10[   et n un entier relatif (a et n écrits en base 10).

a est appelé la mantisse et n est appelé l'exposant.


!!! note "Remarque"
    A part 0, tout réel peut être représenté ainsi en acceptant des mantisses avec un nombre infini 
    de chiffres.  
    S'il s'agit d'écrire vraiment la mantisse, ou de stocker en machine ces chiffres, cette mantisse ne peut plus
    être infinie et
    on est, dans ce cas,  limité   aux décimaux.  
    Si on ajoute en plus comme contrainte que l'on ne  peut pas dépasser un certain nombre de chiffres
    pour écrire la mantisse (et l'exposant), on se
    retrouve avec un nombre très limité de décimaux représentés...Et c'est ce qu'il se passe en machine
    pour le codage des réels par les flottants.
    
    En bref, il nous faut   représenter l'ensemble infini des réels par un ensemble fini de nombres...
    
    
!!! note "Remarque"
    Dans l'écriture scientifique, il n'y a qu'un seul chiffre à gauche de la virgule, et ce chiffre est non nul.
    Le nombre de chiffres à droite de la virgule est par contre variable.


## Exercice 1

&Eacute;crire en notation scientifique (base 10) les nombres réels suivants.

Identifier la mantisse et l'exposant.

+ x = 3 000 000 000





??? solution "x"

    x = 3  &times; 10<sup>9</sup>
    
    mantisse = 3  
    
    exposant  = 9
    
    
+ y = 0, 006


??? solution "y"

    y = 6 &times; 10<sup>-3</sup>
     
    mantisse = 6
     
    exposant = -3
     
     
+ z = 42


??? solution "z"

    z = 4,2 &times; 10<sup>1</sup>
    
    mantisse = 4,2
    
    exposant = 1
    
    
!!! note "Un convertisseur"
    Vous pouvez vous entraîner en traitant  d'autres exercices à l'aide de 
    [ce convertisseur](http://www.learningaboutelectronics.com/LesArticles/Calculatrice-de-notation-scientifique.php#answer)
    de notation décimale vers notation scientifique.
    

## En base 2


Un nombre réel x sera dit écrit en notation scientifique en base 2 lorsqu'il est écrit sous la forme
a&times;2<sup>n</sup> où a est un réel de l'intervalle [1; 2[   et n un entier relatif (a et n écrits en base 2).


!!! note

    La mantisse a ayant excatement un bit non nul avant la virgule est nécessairement de la forme
    1,... (puisque en base 2, le seul chiffre non nul est 1).
    
!!! note
    Tout nombre, à part 0, peut s'écrire ainsi (en acceptant des mantisses avec un nombre infini 
    de bits).

## Exercice 2


&Eacute;crire en notation scientifique (base 2) les nombres réels suivants:

+ x = 1 000 000 000<sub>deux<sub>




??? solution "x"

    En décimal, on a x = 2<sup>9</sup>.

    La notation scientifique en base deux : x = 1 &times; 10<sup>1001</sup>  
    (attention, le 10 ici est en base deux, il s'agit donc du nombre 2<sub>dix</sub>).
    
    mantisse = 1
    
    exposant = 1001
    
+ y = 0,001<sub>deux<sub>



??? solution "y"

    y = 0,001<sub>deux</sub> =  1 &times; 10<sup>-11</sup>
    
    mantisse = 1
    
    exposant = -11
    
 
+ z = 11011<sub>deux<sub>


??? solution "z"

     z = 11011<sub>deux</sub> = 1,1011 &times; 10<sup>100</sup>

    mantisse = 1,1011
    
    exposant = 100


+ w = 9<sub>dix</sub>

??? solution "w"

    w = 1001<sub>deux</sub> soit  1,001 &times; 10<sup>11</sup>
    
    mantisse = 1,001
    
    exposant = 11
