from math import sqrt, floor
def ppee(x):
    """
    x -- float
    
    renvoie le plus petit entier n > 0 tel que n*x est entier
    """
    n = 1
    while n*x != floor(n*x):
        n += 1
    return n
    
    
print(ppee(sqrt(2)))
