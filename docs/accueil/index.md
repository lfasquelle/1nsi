
# Cours de première NSI


Bienvenue sur me site de ressources de la classe de première NSI du [Lycée de la plaine de l'Ain à Ambérieu en Bugey.](https://plaine-ain.ent.auvergnerhonealpes.fr/)


!!! note

    Les pages de ce site sont peuvent être vues en mode jour ou nuit, pour changer le mode cliquer sur l'icône soleil ☀ ou lune 🌜.

## Programme
[Le programme officiel](http://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf)

## Apprendre en NSI

Les pages de cours se découperont en général en deux parties:

  - Une partie à connaître parfaitement : tous les contenus de ces pages doivent être parfaitement sus, vous devez savoir par coeur les notions, les instructions python, html, css, etc... savoir refaire sans hésitation tous les programmes, tous les codes...
  - une partie entraînement : cette partie n'est pas à savoir parfaitement mais n'est pas pour autant facultative. S'entraîner sur des situations nouvelles fait partie intégrale de l'apprentissage, on ne peut pas prétendre avoir appris si l'on ne s'est pas confronté sérieusement à la résolution d'exercices divers.


## Matériel

  -  Vous devrez avoir un cahier à prendre lors de toutes les séances.
  - Lors des séances sur machine, vous devez noter ce qui vous semble important, ce sera à vous de faire le point.
  - Vous noterez au moins pour chaque séquence le plan.
  - Le travail sur les programmes python devra apparaître sur votre cahier: faire apparaître son ossature (idée principale, écrire sous forme algorithmique le coeur du programme...)

