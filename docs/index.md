# Première NSI

## Présentation de Python
+ [Exécuter du Python](https://lfasquelle.forge.aeif.fr/executerpython/)


## Représentation des données: types et valeurs de base
+ [Type Numérique](https://lfasquelle.forge.aeif.fr/typesNumeriques/)  
+ [Révision : for, if, while, def](https://lfasquelle.forge.aeif.fr/for-if-while/)
+ [Les chaînes de caractères](https://lfasquelle.forge.aeif.fr/chaines/)
+ [Entiers positifs](https://lfasquelle.forge.aeif.fr/entiers_positifs/)
+ [Entiers relatif](https://lfasquelle.forge.aeif.fr/relatifs/)
+ [Booléens](https://lfasquelle.forge.aeif.fr/booleen/)
+ [Les flottant](https://lfasquelle.forge.aeif.fr/flottant/)
+ [Encodage du texte](https://lfasquelle.forge.aeif.fr/encodagetexte)
/)

## Représentation des données: types construits
+ [tuples](https://lfasquelle.forge.aeif.fr/tuples/)
+ [tableau, liste](https://lfasquelle.forge.aeif.fr/tableautableau/)
+ [tableau de tableaux](https://lfasquelle.forge.aeif.fr/tableautableau/) 
+ [dictionnaires](https://lfasquelle.forge.aeif.fr/dictionnaires/)

## Traitement de données en tables
+ [csv, recherche, tris, fusion](https://lfasquelle.forge.aeif.fr/csv/)

## Interactions entre l'homme et la machine sur le web
+ [html, css](https://lfasquelle.forge.aeif.fr/html_css/)
+ [js, formulaires, événements](https://lfasquelle.forge.aeif.fr/jsevenements/)
+ [web et internet](https://lfasquelle.forge.aeif.fr/webinternet/)
+ client/serveur, http, https, cookies...
+ php, paramètres dans l'URL
+ formulaires et php
+ HTTP, formulaires, cookies... et python

## Architectures matérielles et systèmes d'exploitation
+ Architecture des ordinateurs
+ Modèle d'architecture séquentielle
+ Initiation a l'assembleur
+ Circuits combinatoires
+ Architecture réseau
+ Transmission de données dans un réseau
+ Tp Filius 
+ IHM
+ Système d'exploitation
+ Commandes Linux 
+ Ssh 
+ Droits et permissions 

## Langages et programmation
+ Révision : for, if, while, def
+ [Les bugs](https://lfasquelle.forge.aeif.fr/bug/) 

## Algorithmique
+  Les tris : Généralités 
+  Tri insertion 
+  Tri sélection
+  Variant de boucle 
+  Invariant de boucle
+  Complexité Cours 
+  Algorithme des k plus proches voisins 
+  Glouton
+  Dichotomie 




 
