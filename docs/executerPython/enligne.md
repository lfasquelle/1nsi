# Python en ligne



Il est possible de tester du code Python en ligne, sans installation sur la machine sur laquelle on travaille.

Le meilleur choix :

+ [Basthon](https://basthon.fr/): exécution de Python dans le navigateur.

Autre Choix:

+ [repl.it](https://repl.it/). Ce site peut être très pratique pour échanger entre vous ou pour nous poser
des questions: vous envoyez un lien sur votre script en ligne, celui qui reçoit ce lien pourra vous proposer
des corrections sans modifier votre script et vous enverra un lien sur la page contenant les corrections.


+ [trinket](https://trinket.io/). Les mêmes principes que le site précédent.


+ [PythonTutor](http://pythontutor.com/visualize.html#) dispose d'une animation permettant de suivre l'état des 
variables de votre programme au fur et à mesure de son déroulement.


