# Interpréteur dans un terminal 

Pour des programmes courts ou pour quelques tests, on lancera un interpréteur python3 dans un terminal. 


## Ouvrir un terminal

Vous pouvez ouvrir un terminal avec le raccourci Ctrl + Alt + T.

## Démarche



Ouvrir un terminal linux et taper `python3` à l'invite de commande.


Vous voyez apparaître trois chevrons `>>>` à la suite desquels vous pourrez entrer des instructions python:
 
```
$ python3
Python 3.8.2 (default, Jul 16 2020, 14:00:26) 
[GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>  
```



!!! info
    Le symbole $ ne doit pas être entré, ce symbole -- ou un autre du même genre -- apparaît 
    déjà en général, on parle d' **invite de commandes**.
    [Pour aller plus loin](https://openclassrooms.com/fr/courses/43538-reprenez-le-controle-a-laide-de-linux/38076-entrer-une-commande).


 

!!! Attention
    Ce qui est fait dans l'intepréteur python ne peut pas être sauvegardé pour être réutilisé ultérieurement.

## Premières utilisations 


!!! note

    Les instructions ci-dessous sont élémentaires et sont normalement maïtrisées par tous car vues en classe
    de mathématiques en seconde, et parfois également en classe de SNT.


Quelques opérations arithmétiques:

+ Addition : `+`, soustraction : `–`, multiplication : `*`.

+ Attention à la distinction entre division réelle `/` et division entière `//`.  
Par exemple 7 // 2 = 3  et non pas 3.5! 

```
>>> 13 / 5
2.6
>>> 7 // 2
3
```

+ Modulo : `%`  
Le modulo retourne le reste d’une division euclidienne (ou division entière). 

!!! note "Exemple"
    7 = 2 * 3 + 1. 
    Dans cet exemple:
    
    + 7 est le dividende
    + 2 est le diviseur
    + 3 est le quotient (en Python: 7//2)
    + 1 est le reste (en Python: 7%2).
    
```
>>> 7//2
3
>>> 7%2
1
>>> divmod(7,2) # couple (quotient,reste) de 7 par 2
(3, 1)
```


+ [Exponentiation](https://docs.python.org/fr/3/reference/expressions.html?highlight=bitwise#the-power-operator): `**`  
Exemple. calcul de  2<sup>3</sup>:

```
>>> 2**3
8
```

Attention: ne pas utiliser  `^` pour calculer une puissance, 
[cet opérateur a une autre signification](https://docs.python.org/fr/3/reference/expressions.html?highlight=bitwise#binary-bitwise-operations).

+ Parenthèses.
Les règles de priorité sont les règles de priorité mathématiques usuelles [PEMDAS](https://www.mathsisfun.com/operation-order-pemdas.html):
    + P pour parenthèses (priorité sur tout)
    + E pour exposant
    + M et D pour Multiplication et Division
    + A et S pour addition et soustraction

 
 

 


## Les erreurs

 
Essayez de diviser 8 par 0.

L’interpréteur Python3 vous signale cette erreur par le message: `ZeroDivisionError : division by zero`.

 

```
>>> 8 / (2-2)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ZeroDivisionError: division by zero
```


## Remonter dans l'historique

Avec la flèche haut du clavier, les entrées précédemment testées sont réécrites dans la ligne courante. 
Vous pouvez alors   modifier,   retester...

Essayez!

 
## Sortir de l’interpréteur Python 3

Pour sortir de l’interpréteur Python 3, 
il vous suffit d’entrer la commande `exit()` ou `quit()` ou `Ctrl + D`
et vous êtes de retour dans ce qu’on appelle le Shell Unix.

