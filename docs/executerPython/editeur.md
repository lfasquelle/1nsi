# Un éditeur de texte pour enregistrer et conserver


Un éditeur de texte est un logiciel qui permet de rédiger des textes (par exemple des programmes) en texte brut, 
c’est-à-dire sans aucun formatage. 

Un éditeur de texte dédié à la programmation offre également au programmeur 
une coloration syntaxique qui facilite la lecture et la compréhension du code 
ainsi que diverses options telles que l’indentation automatique, 
la numérotation des lignes ou le surlignage des parenthèses qui se correspondent.

Exemples d'éditeur de texte :

+ [geany](https://www.geany.org/) (linux, windows).    
Cet éditeur de texte inclut les fonctions élémentaires 
d'un environnement de développement intégré.
 
+ [spyder](https://www.spyder-ide.org/).    
Cet éditeur, dédié à la programmation python, est par exemple installé automatiquement lorsque vous installez 
[anaconda](https://www.anaconda.com/distribution/).

+ gedit (linux)

+ notepad++ (windows)


!!! Note
    En NSI, on utilisera les éditeurs Geany et Spyder. 
    
  
    
## Exécuter un programme 


Supposons que nous ayons écrit dans un fichier texte nommé *fonctions.py* le script suivant:

```python
def carre(x):
    """
    x -- nombre (int ou float)
    
    renvoie le carré de x
    """
    return x*x
    
    
def cube(x):
    """
    x -- nombre (int ou float)
    
    renvoie le cube de x
    """
    return x * carre(x)
    
    
def inverse(x):
    """
    x -- nombre (int ou float) non nul
    
    renvoie l'inverse de x
    """
    return 1/x
``` 
     
     
     

On commence par ouvrir un terminal  directement par clic droit **dans le dossier du fichier .py**
puis on peut utiliser notre script ainsi:

```
$ python3
Python 3.8.2 (default, Jul 16 2020, 14:00:26) 
[GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from fonctions import carre, cube, inverse
>>> carre(4)
16
>>> cube(2)
8
>>> inverse(0.25)
4.0
>>> 

```


## Changer de répertoire avec le shell 



Téléchargez [ce dossier zippé](A.zip) puis dézippez le.

Ce dossier contient une arborescence de dossiers et fichiers .py:

![](img_executer/arbo.png)

On voit cinq répertoires A, B, C, D, E et trois fichiers python.


!!! important
    On utilisera également le mot **répertoire** en lieu et place du mot *dossier*.
    
    

Si on ouvre le terminal à l'intérieur du répertoire B (le chemin indiqué 
à l'invite de commande se présente sous la forme `.../A/B`)
et que l'on veuille exécuter le fichier fonctions.py, 
on peut utiliser la commande `cd`:

 

```
.../A/B $ cd D
```

!!! important 
    `cd` sont les initiales de `change directory` (changer de répertoire).
    
    
Le répertoire de travail est alors D. On peut afficher le contenu de ce répertoire avec la commande `ls`:


 
```
.../A/B/D $ ls
coucou.py  fonctions.py
```
  
!!! important 
    `ls` est une abréviation de **l**i**s**t. On liste le contenu du répertoire.
    
  
  

Et on peut alors faire appel à l'un des scripts:

```
A/B/D $ python3
>>> from fonctions import cube
>>> cube(4)
64
```

### A vous

+ Faîtes les étapes décrites ci-dessus.
+ Ouvrir ensuite le terminal  dans le répertoire A, changer de répertoire avec `cd` pour vous placer dans le répertoire
D et exécuter le fichier coucou.py en l'important dans une session python3.


??? solution "Détails"

    On ouvre le terminal dans le répertoire A (clic droit, ouvrir dans un terminal en se plaçant dans A).
    On modifie le répertoire de travail avec la commande `cd`:
    
    ```
    .../A $ cd B/D
    .../A/B/D $ ls
    coucou.py  fonctions.py   
    ```
    
    Puis:
    
    ```
    /A/B/D $ python3
    >>> import coucou
    ```
    
    Vous devriez alors voir le résultat de l'exécution du fichier coucou.py (qui affiche son nom et
    son chemin complet depuis la racine).

    

### "Remonter" dans l'arborescence.

Si le répertoire de travail indiqué dans le terminal est B, comment le modifier en C?

On remonte d'un niveau (on sort donc de B) avant de plonger dans C de la façon suivante:

```
/A/B $ cd ../C
```

+ Testez cela et vérifiez que le répertoire de travail est bien maintenant C.
+ Faites afficher le contenu du répertoire C avec `ls`.
+ Exécuter le fichier python contenu dans C.


??? solution "Détails"

    ```
    .../A/B $ cd ../C
    .../A/C $ ls
    a.py
    .../A/C $ python3 a.py
    ```
    
    Vous devriez voir s'afficher le nom du fichier et son chemin complet depuis la racine.



## Modifier le répertoire par une commande Python


Python présente des commandes équivalentes à celles du shell linux utilisées ci-dessus.

Ouvrez un terminal dans le dossier A (clic droit dans A...).

Puis testez la séquence suivante:

```
.../A $ python3
>>> import os
>>> os.chdir("B/D")
>>> os.listdir()
>>> import fonctions
>>> fonctions.carre(3)
>>> help(fonctions)
```


