# Spyder:  environnement de développement intégré 

Un environnement de développement intégré combine un terminal interactif et un éditeur de texte. 
Très pratique, il permet de visualiser instantanément les modifications apportées à un programme.

 
 



 
##  Un ide dédié à Python

Rappelons que Spyder est, par exemple, installé lorsque vous installez [anaconda](https://www.anaconda.com/distribution/) (
c'est l'installation que nous vous
conseillons sur vos postes personnels).

Vous pouvez aussi consulter le [site de ce logiciel](https://www.spyder-ide.org/).



##  Lancer Spyder


Ouvrez Anaconda Navigator puis cliquez sur le bouton "launch" de Spyder.


Spyder lancé, vous devriez obtenir quelque chose qui ressemble à cela :

![Spyder](img_executer/spyder.png)

Spyder se divise en plusieurs fenêtres, 
deux fenêtres vont principalement nous intéresser : la fenêtre "éditeur" et la fenêtre "console".



##  Exercice

On peut utiliser l'interpréteur python (partie console) après avoir défini les fonctions
dans la partie éditeur.

Testez ce qui suit (on définit une fonction dans l'éditeur, on appelle cette fonction dans la partie console):

![](img_executer/spyderConsole.png)


##  Exercice


Tapez le script suivant dans la fenêtre d'édition:

```python
def estMultipleDeTrois(n):
    """
    n -- entier 
    
    renvoie True si n est multiple de 3, False sinon.
    """
    return n%3 == 0



for k in range(10):
    print(f"{k} est-il multiple de 3? {estMultipleDeTrois(k)}")
```



Cliquez sur le "triangle vert" afin d'exécuter le programme qui vient d'être saisi.

![Spyder](img_executer/spyder2.png)

Si votre fichier n'a pas été enregistré au préalable, Spyder vous demande de l'enregistrer.  
Enregistrez-le dans un dossier qui vous servira de dossier de travail.

Vous devez voir le résultat de l'exécution du script dans la  console:

![Spyder](img_executer/spyder3.png)

 

